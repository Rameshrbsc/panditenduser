package com.codegenstudios.purohit.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Adapter.PoojaListCardAdapter;
import com.codegenstudios.purohit.Pojo.PoojaListPojo;
import com.codegenstudios.purohit.Pojo.PoojaListResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class BookingCardListActivity extends AppCompatActivity {

    @BindView(R.id.ListView)
    ListView listView;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String[] PoojaName, PoojaID, PoojaDescription, Amount, PoojaPicthumbnail;
    String CatName, CatId, SubName, SubId, IntentPassing;
    Bundle bundle;
    @BindView(R.id.GIFLinearLayout)
    LinearLayout GIFLinearLayout;
    String activity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_card_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference(this);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();


        if (bundle != null && bundle.containsKey("IntentPassing")) {
            IntentPassing = bundle.getString("IntentPassing");
            activity = bundle.getString("Activity");
            if (IntentPassing.equals("DashboardDetails")) {
                CatName = bundle.getString("CatogeryName");
                CatId = bundle.getString("CatogeryId");
                SubName = bundle.getString("SubCatogeryName");
                SubId = bundle.getString("SubCatogeryId");
                Log.i("SubId", "" + CatId + "" + SubId);

            } else if (IntentPassing.equals("ViewAll")) {

                SubId = bundle.getString("SubCatogeryId");
                SubName = bundle.getString("SubCatogeryName");
                CatName = bundle.getString("CatogeryName");
                CatId = bundle.getString("CatogeryId");
            } else if (IntentPassing.equals("BookingCardListActivity")) {
                CatId = bundle.getString("CatogeryId");
                SubId = bundle.getString("SubCatogeryId");
            } else if (IntentPassing.equals("BookingFormActivity")) {

                CatId = bundle.getString("CatId");
                SubId = bundle.getString("SubId");
                SubName = bundle.getString("SubName");
                CatName = bundle.getString("CatName");
                activity = bundle.getString("Activity");
            }
            setTitle(SubName);
        }

        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity != null) {

                    if (activity.equals("book")) {
                        Intent intent = new Intent(BookingCardListActivity.this, UserNavigationActivity.class);
                        startActivity(intent);
                    } else if (activity.equals("more")) {

                        finish();
                        /*Intent intent = new Intent(BookingCardListActivity.this, ViewMoreActivty.class);
                        startActivity(intent);*/
                    } else {
                        Intent intent = new Intent(BookingCardListActivity.this, UserNavigationActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(BookingCardListActivity.this, UserNavigationActivity.class);
                    startActivity(intent);
                }


            }
        });


//        if (myPreference.isInternetOn()) {
//            progressBar = new ProgressDialog(BookingCardListActivity.this);
//            progressBar.setCancelable(true);
//            progressBar.setMessage("Processing ...");
//            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressBar.setProgress(0);
//            progressBar.setMax(100);
//            progressBar.show();
//            JsonObject jsonObject = new JsonObject();
//            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
//            jsonObject.addProperty("AccessToken",myPreference.getDefaultRunTimeValue()[0]);
//            jsonObject.addProperty("CatogeryId",CatId);
//            jsonObject.addProperty("SubCatogeryId",SubId);
//
//            API apiService = createServiceHeader(API.class);
//            Call<PoojaListPojo> call = apiService.POOJA_LIST_POJO_CALL(jsonObject);
//            call.enqueue(new Callback<PoojaListPojo>() {
//                @Override
//                public void onResponse(Call<PoojaListPojo> call, Response<PoojaListPojo> response) {
//
//                    progressBar.dismiss();
//
//                    switch (response.body().getCode()) {
//                        case "200":
//                            PoojaListResponsePojo[] poojaListResponsePojos = response.body().getResponse();
//                            PoojaName = new String[poojaListResponsePojos.length];
//                            PoojaID = new String[poojaListResponsePojos.length];
//                            PoojaDescription = new String[poojaListResponsePojos.length];
//                            Amount = new String[poojaListResponsePojos.length];
//                            PoojaPicthumbnail = new String[poojaListResponsePojos.length];
//
//
//                            for(int i = 0;i<poojaListResponsePojos.length;i++){
//                                PoojaName[i] = poojaListResponsePojos[i].getPoojaName();
//                                PoojaID[i] = poojaListResponsePojos[i].getPoojaId();
//                                PoojaDescription[i] = poojaListResponsePojos[i].getPoojaDescription();
//                                Amount[i] = poojaListResponsePojos[i].getAmount();
//                                PoojaPicthumbnail[i] = poojaListResponsePojos[i].getPoojaPicthumbnail();
//
//
//                            }
//                            if(PoojaName.length > 0){
//                                PoojaListCardAdapter myBookingCardAdapter = new PoojaListCardAdapter(BookingCardListActivity.this,PoojaName,PoojaID,PoojaDescription,Amount,PoojaPicthumbnail,CatName,SubName,CatId,SubId);
//                                listView.setAdapter(myBookingCardAdapter);
//                                listView.setVisibility(View.VISIBLE);
//                                GIFLinearLayout.setVisibility(View.GONE);
//                            }else{
//                                listView.setVisibility(View.GONE);
//                                GIFLinearLayout.setVisibility(View.VISIBLE);
//                            }
//
//
////                            Intent intenttransactionpage = new Intent(BookingConfirmPageActivity.this, TransactionProcessingPageActivity.class);
////                            intenttransactionpage.putExtra("BookingId",response.body().getBookingId());
////                            startActivity(intenttransactionpage);
//                            break;
//
//                        case "108":
//                            Intent intent = new Intent(BookingCardListActivity.this, LoginActivity.class);
//                            startActivity(intent);
//                            break;
//                        default:
//                            myPreference.ShowDialog(BookingCardListActivity.this,"Oops",response.body().getMessage());
//                            break;
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<PoojaListPojo> call, Throwable t) {
//
//                    progressBar.dismiss();
//                    try {
//                        Toast.makeText(BookingCardListActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
//
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//        }
//        else{
//            myPreference.ShowDialog(BookingCardListActivity.this,"Oops","There is not internet connection");
//        }


    }

    @Override
    protected void onResume() {
        super.onResume();


        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(BookingCardListActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("CatogeryId", CatId);
            jsonObject.addProperty("SubCatogeryId", SubId);

            API apiService = createServiceHeader(API.class);
            Call<PoojaListPojo> call = apiService.POOJA_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<PoojaListPojo>() {
                @Override
                public void onResponse(Call<PoojaListPojo> call, Response<PoojaListPojo> response) {

                    progressBar.dismiss();

                    switch (response.body().getCode()) {
                        case "200":
                            PoojaListResponsePojo[] poojaListResponsePojos = response.body().getResponse();
                            PoojaName = new String[poojaListResponsePojos.length];
                            PoojaID = new String[poojaListResponsePojos.length];
                            PoojaDescription = new String[poojaListResponsePojos.length];
                            Amount = new String[poojaListResponsePojos.length];
                            PoojaPicthumbnail = new String[poojaListResponsePojos.length];


                            for (int i = 0; i < poojaListResponsePojos.length; i++) {
                                PoojaName[i] = poojaListResponsePojos[i].getPoojaName();
                                PoojaID[i] = poojaListResponsePojos[i].getPoojaId();
                                PoojaDescription[i] = poojaListResponsePojos[i].getPoojaDescription();
                                Amount[i] = poojaListResponsePojos[i].getAmount();
                                PoojaPicthumbnail[i] = poojaListResponsePojos[i].getPoojaPicthumbnail();


                            }
                            if (PoojaName.length > 0) {
                                PoojaListCardAdapter myBookingCardAdapter = new PoojaListCardAdapter(BookingCardListActivity.this, PoojaName, PoojaID, PoojaDescription, Amount, PoojaPicthumbnail, CatName, SubName, CatId, SubId);
                                listView.setAdapter(myBookingCardAdapter);
                                listView.setVisibility(View.VISIBLE);
                                GIFLinearLayout.setVisibility(View.GONE);
                            } else {
                                listView.setVisibility(View.GONE);
                                GIFLinearLayout.setVisibility(View.VISIBLE);
                            }

                            break;

                        case "108":
                            Intent intent = new Intent(BookingCardListActivity.this, LoginActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            myPreference.ShowDialog(BookingCardListActivity.this, "Oops", response.body().getMessage());
                            break;

                    }
                }

                @Override
                public void onFailure(Call<PoojaListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        Toast.makeText(BookingCardListActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            myPreference.ShowDialog(BookingCardListActivity.this, "Oops", "There is not internet connection");
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        // finish();

        if (activity != null) {
            if (activity.equals("dash")) {
                finish();
            } else {

                Intent intent = new Intent(BookingCardListActivity.this, UserNavigationActivity.class);
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(BookingCardListActivity.this, UserNavigationActivity.class);
            startActivity(intent);
        }
    }
}
