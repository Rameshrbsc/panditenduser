package com.codegenstudios.purohit.Activity;

import android.app.Application;

import com.codegenstudios.purohit.SupportClass.TypefaceUtil;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(),
                "SERIF",
                "font/07558_CenturyGothic.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
    }
}
