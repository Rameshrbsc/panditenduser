package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Config.Config;
import com.codegenstudios.purohit.Pojo.SignUpPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class SignupConfirmViewActivity extends AppCompatActivity {
    @BindView(R.id.TextViewUserName)
    TextView TextViewUserName;
    @BindView(R.id.TextViewUserEmailID)
    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewUserPassword)
    TextView TextViewUserPassword;
    @BindView(R.id.TextViewUserMoblieNumber)
    TextView TextViewUserMoblieNumber;
    @BindView(R.id.TextViewUserNatchtathiram)
    TextView TextViewUserNatchtathiram;
    @BindView(R.id.TextViewUserGothram)
    TextView TextViewUserGothram;
    @BindView(R.id.TextViewUserSection)
    TextView TextViewUserSection;
    @BindView(R.id.TextViewUserAadharNumber)
    TextView TextViewUserAadharNumber;
    @BindView(R.id.TextViewUserLanguage)
    TextView TextViewUserLanguage;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String EmailIDsaved,EmailIDToken, SocialType,UserLanguage,encodedImage, UserName, UserEmailID, UserMoblieNumber,UserPassword, UserNatchtathiram,UserGothram, UserSection, UserAadhar,TempUri;
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    Uri tempUri;
    String MY_PREFS_NAME = "MyPrefsFile",UserPicURI;
    File file;
    Bundle bundle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigup_confirm_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("SignUp Details");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);

        bundle = getIntent().getExtras();
        if(bundle !=null){
            String IntentPassing = bundle.getString("IntentPassing");
            if(IntentPassing.equals("SignupPage")){
                UserName = bundle.getString("UserName");
                UserEmailID =  bundle.getString("UserEmailID");
                UserMoblieNumber =  bundle.getString("UserMoblieNumber");
                UserPassword =  bundle.getString("UserPassword");
                UserNatchtathiram =  bundle.getString("UserNatchtathiram");
                UserGothram = bundle.getString("UserGothram");
                UserSection =  bundle.getString("UserSection");
                UserAadhar =  bundle.getString("UserAadhar");
                TempUri =  bundle.getString("ImageUriPath");
                EmailIDsaved = bundle.getString("EmailID");
                EmailIDToken = bundle.getString("EmailIDUnique");
                SocialType = bundle.getString("SocialType");
                UserLanguage = bundle.getString("UserLanguage");
                try {
                    file = new File(TempUri);
                    Picasso.with(this).load(new File(TempUri)).transform(new SignUpActivity.CircleTransform()).into(ImageViewProfilePic);
                    EncodedMethodCall();
                }catch (Exception e){
                    e.printStackTrace();
                }

                TextViewUserName.setText(UserName);
                TextViewUserEmailID.setText(UserEmailID);
                TextViewUserPassword.setText(UserPassword);
                TextViewUserMoblieNumber.setText(UserMoblieNumber);
                TextViewUserNatchtathiram.setText(UserNatchtathiram);
                TextViewUserGothram.setText(UserGothram);
                TextViewUserSection.setText(UserSection);
                TextViewUserAadharNumber.setText(UserAadhar);
                TextViewUserLanguage.setText(UserLanguage);
            }
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if(EmailIDToken != null && EmailIDsaved !=null && SocialType.equals("1")){//FbSubmit
                    if (myPreference.isInternetOn()) {
                        progressBar = new ProgressDialog(SignupConfirmViewActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setCancelable(false);
                        progressBar.setMax(100);
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("UserName", UserName);
                        jsonObject.addProperty("UserEmailID",UserEmailID);
                        jsonObject.addProperty("UserPhoneNumber", UserMoblieNumber);
                        jsonObject.addProperty("UserPassword", UserPassword);
                        jsonObject.addProperty("UserSection", UserSection);//send section data
                        jsonObject.addProperty("UserNatchatram",UserNatchtathiram);
                        jsonObject.addProperty("UserGothram", UserGothram);
                        jsonObject.addProperty("UserAadharNumber", UserAadhar);
                        jsonObject.addProperty("UserLanguage", UserLanguage);
                        if (encodedImage != null) {
                            jsonObject.addProperty("UserProfilePic", encodedImage);
                        }
                        else {
                            jsonObject.addProperty("UserProfilePic", "");
                        }
                        jsonObject.addProperty("UsersocialId", EmailIDToken);
                        jsonObject.addProperty("UsersocialEmail", EmailIDsaved);
                        jsonObject.addProperty("UsersocialType", 1);
                        Log.e("json",jsonObject.toString());
                        API apiService = createServiceHeader(API.class);
                        Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<SignUpPojo>() {
                            @Override
                            public void onResponse(Call<SignUpPojo> call, Response<SignUpPojo> response) {
                                switch (response.body().getCode()) {
                                    case "200":
                                        Intent intentOTP = new Intent(SignupConfirmViewActivity.this, UserOTPageActivity.class);
                                        intentOTP.putExtra("MoblieNumber",UserMoblieNumber);
                                        intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                                        intentOTP.putExtra("NotificationCount",response.body().getNotificationCount());
                                        intentOTP.putExtra("IntentPassing","RegisterPage");

                                        startActivity(intentOTP);
                                        break;
                                    case "108":
                                        Intent intent = new Intent(SignupConfirmViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        break;
                                    default:
                                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops",response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<SignUpPojo> call, Throwable t) {

                                progressBar.dismiss();
                                try {
                                    Toast.makeText(SignupConfirmViewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                    else{
                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops","There is not internet connection");
                    }
                }else if(EmailIDToken != null && EmailIDsaved !=null && SocialType.equals("2")){//GmailSubmit

                    if (myPreference.isInternetOn()) {
                        progressBar = new ProgressDialog(SignupConfirmViewActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setCancelable(false);
                        progressBar.setMax(100);
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId",  displayFirebaseRegId());
                        jsonObject.addProperty("UserName", UserName);
                        jsonObject.addProperty("UserEmailID",UserEmailID);
                        jsonObject.addProperty("UserPhoneNumber", UserMoblieNumber);
                        jsonObject.addProperty("UserPassword", UserPassword);
                        jsonObject.addProperty("UserSection", UserSection);//send section data
                        jsonObject.addProperty("UserNatchatram",UserNatchtathiram);
                        jsonObject.addProperty("UserGothram", UserGothram);
                        jsonObject.addProperty("UserAadharNumber", UserAadhar);
                        jsonObject.addProperty("UserLanguage", UserLanguage);
                        if (encodedImage != null) {
                            jsonObject.addProperty("UserProfilePic", encodedImage);
                        }
                        else {
                            jsonObject.addProperty("UserProfilePic", "");
                        }
                        jsonObject.addProperty("UsersocialId", EmailIDToken);
                        jsonObject.addProperty("UsersocialEmail", EmailIDsaved);
                        jsonObject.addProperty("UsersocialType", 2);
                        Log.e("json",jsonObject.toString());
                        API apiService = createServiceHeader(API.class);
                        Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<SignUpPojo>() {
                            @Override
                            public void onResponse(Call<SignUpPojo> call, Response<SignUpPojo> response) {
                                switch (response.body().getCode()) {
                                    case "200":
                                        Intent intentOTP = new Intent(SignupConfirmViewActivity.this, UserOTPageActivity.class);
                                        intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                                        intentOTP.putExtra("MoblieNumber",TextViewUserMoblieNumber.getText().toString());
                                        intentOTP.putExtra("NotificationCount",response.body().getNotificationCount());
                                        intentOTP.putExtra("IntentPassing","RegisterPage");
                                        startActivity(intentOTP);
                                        break;
                                    case "108":
                                        Intent intent = new Intent(SignupConfirmViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        break;
                                    default:
                                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops",response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<SignUpPojo> call, Throwable t) {

                                progressBar.dismiss();
                                try {
                                    Toast.makeText(SignupConfirmViewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }else{
                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops","There is not internet connection");
                    }
                }else {
                    if (myPreference.isInternetOn()){//normal Submition
                        progressBar = new ProgressDialog(SignupConfirmViewActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.setCancelable(false);
                        progressBar.show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("UserName", UserName);
                        jsonObject.addProperty("UserEmailID",UserEmailID);
                        jsonObject.addProperty("UserPhoneNumber", UserMoblieNumber);
                        jsonObject.addProperty("UserPassword", UserPassword);
                        jsonObject.addProperty("UserSection", UserSection);//send section data
                        jsonObject.addProperty("UserNatchatram",UserNatchtathiram);
                        jsonObject.addProperty("UserGothram", UserGothram);
                        jsonObject.addProperty("UserAadharNumber", UserAadhar);
                        jsonObject.addProperty("UserLanguage", UserLanguage);
                        if (encodedImage != null) {
                            jsonObject.addProperty("UserProfilePic", encodedImage);
                        }
                        else {
                            jsonObject.addProperty("UserProfilePic", "");
                        }
                        jsonObject.addProperty("UsersocialId", "");
                        jsonObject.addProperty("UsersocialEmail", "");
                        jsonObject.addProperty("UsersocialType", "");


                        API apiService = createServiceHeader(API.class);
                        Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL(jsonObject);
                        Log.e("JsonRequestSignup",jsonObject.toString());
                        call.enqueue(new Callback<SignUpPojo>() {
                            @Override
                            public void onResponse(Call<SignUpPojo> call, Response<SignUpPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        Intent intentOTP = new Intent(SignupConfirmViewActivity.this, UserOTPageActivity.class);
                                        intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                                        intentOTP.putExtra("MoblieNumber",TextViewUserMoblieNumber.getText().toString());
                                        intentOTP.putExtra("NotificationCount",response.body().getNotificationCount());
                                        intentOTP.putExtra("IntentPassing","RegisterPage");
                                        startActivity(intentOTP);
                                        break;

                                    case "108":
                                        Intent intent = new Intent(SignupConfirmViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        break;
                                    default:
                                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops",response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<SignUpPojo> call, Throwable t) {
                                progressBar.dismiss();
                                try {
                                    Toast.makeText(SignupConfirmViewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                    } else{
                        myPreference.ShowDialog(SignupConfirmViewActivity.this,"Oops","There is not internet connection");
                    }

                }
            }
        });

    }

    private void EncodedMethodCall() {
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 60, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.i("encodedImage",""+encodedImage);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String displayFirebaseRegId() {
        String regId;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("DeviceId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e("TAG", "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e("TAG", "Firebase Reg Id is not received yet!");
        return regId;
    }

}
