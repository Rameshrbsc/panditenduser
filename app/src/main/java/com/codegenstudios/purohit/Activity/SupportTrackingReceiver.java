package com.codegenstudios.purohit.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.TrackingPurohitLatLongPojo;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class SupportTrackingReceiver extends BroadcastReceiver{
    public static String  PUROHIT_MAP_UPDATE = "purohittrackingupdate";
    public static String  PUROHIT_STOP_TRACKING = "purohitstoptracking";
    Bundle bundle;
    MyPreference myPreference;
    String ErrorCode = "200";
    @Override
    public void onReceive(final Context context, Intent intent) {
        bundle = intent.getExtras();
        Log.e("Message", "Inside Receiver");
        Log.e("BookingId", bundle.getString("BookingId"));

        myPreference = new MyPreference(context);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
        jsonObject.addProperty("BookingId",bundle.getString("BookingId"));

        API apiService = createServiceHeader(API.class);
        Call<TrackingPurohitLatLongPojo> call = apiService.TRACKING_PUROHIT_LAT_LONG_POJO_CALL(jsonObject);

        call.enqueue(new Callback<TrackingPurohitLatLongPojo>() {
            @Override
            public void onResponse(Call<TrackingPurohitLatLongPojo> call, Response<TrackingPurohitLatLongPojo> response) {
                switch (response.body().getCode()) {
                    case "200":
                        String  PurohitLat = response.body().getLatitude();
                        String  PurohitLong = response.body().getLongitude();
                        Intent pushNotification = new Intent(PUROHIT_MAP_UPDATE);
                        pushNotification.putExtra("purohitlat",PurohitLat  );
                        pushNotification.putExtra("purohitlong", PurohitLong);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(pushNotification);

                        break;
                    case "204":
                        ErrorCode ="204";
                        Intent intenthome = new Intent(context, UserNavigationActivity.class);
                        context.startActivity(intenthome);
                        Intent StopTracking = new Intent(PUROHIT_STOP_TRACKING);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(StopTracking);
                        break;
                    case "108":
                        ErrorCode ="204";
                        Intent intent1 = new Intent(context, LoginActivity.class);
                        context.startActivity(intent1);
                        break;
                    default:
                        ErrorCode ="204";
                        Intent intenthome1 = new Intent(context, UserNavigationActivity.class);
                        context.startActivity(intenthome1);
                        Intent StopTracking1 = new Intent(PUROHIT_STOP_TRACKING);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(StopTracking1);
//                        myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        break;
                }
            }

            @Override
            public void onFailure(Call<TrackingPurohitLatLongPojo> call, Throwable t) {

            }
        });


        Log.e("Errorcode",ErrorCode);
            if(!ErrorCode.equals("204")){

                Intent myIntent = new Intent(context, SupportTrackingReceiver.class);
                myIntent.putExtra("BookingId",bundle.getString("BookingId"));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 100, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                assert manager != null;
                long time = System.currentTimeMillis();
                manager.set(AlarmManager.RTC_WAKEUP,  time + 15000, pendingIntent);
                Log.e("Message", "Call Triggered");
            }





//        JsonObject jsonObject = new JsonObject();
//            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
//            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
//            jsonObject.addProperty("BookingId",bundle.getString("BookingId"));
//
//            API apiService = createServiceHeader(API.class);
//            Call<TrackingPurohitLatLongPojo> call = apiService.TRACKING_PUROHIT_LAT_LONG_POJO_CALL(jsonObject);
//            call.enqueue(new Callback<TrackingPurohitLatLongPojo>() {
//                @Override
//                public void onResponse(Call<TrackingPurohitLatLongPojo> call, Response<TrackingPurohitLatLongPojo> response) {
//
//                    switch (response.body().getCode()) {
//                        case "200":
//
//                            Log.i("purohitlat ", response.body().getLatitude());
//                            Log.i("purohitlong ", response.body().getLongitude());
//
//                            Intent pushNotification = new Intent(PUSH_NOTIFICATION);
//                            pushNotification.putExtra("purohitlat", response.body().getLatitude());
//                            pushNotification.putExtra("purohitlong", response.body().getLongitude());
//                            LocalBroadcastManager.getInstance(context).sendBroadcast(pushNotification);
//
//                            Intent myIntent = new Intent(context, SupportTrackingReceiver.class);
//                            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 100, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
//                            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//                            assert manager != null;
//                            long time = System.currentTimeMillis();
//                            manager.set(AlarmManager.RTC_WAKEUP,  time + 15000, pendingIntent);
//                            Log.e("Message", "Call Triggered");
//
//                            break;
//                        case "108":
//                            Intent intent = new Intent(context, LoginActivity.class);
//                            context.startActivity(intent);
//                            break;
//
//
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<TrackingPurohitLatLongPojo> call, Throwable t) {
//                    Log.i("eceptiom",""+t.getMessage());
//
//                }
//            });








    }
}
