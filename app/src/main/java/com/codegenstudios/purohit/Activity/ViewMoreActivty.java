package com.codegenstudios.purohit.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.codegenstudios.purohit.Adapter.MoreViewDashboardSubCardViewAdapter;
import com.codegenstudios.purohit.Pojo.DashboardPojo;
import com.codegenstudios.purohit.Pojo.DashboardResponsePojo;
import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
import com.codegenstudios.purohit.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewMoreActivty extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.TextViewTitle)
    TextView TextViewTitle;

    List<DashboardResponsePojo> dashboardResponsePojosList;
    DashboardResponsePojo dashboardResponsePojo;
    DashboardSubCatgeryListResponsePojo[] dashboardSubCatgeryListResponsePojos;
    String Code = "", titleName;
    MoreViewDashboardSubCardViewAdapter moreViewDashboardSubCardViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_more_activty);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dashboardResponsePojosList = new ArrayList<>();
        dashboardResponsePojo = new DashboardResponsePojo();
        Intent intent = getIntent();
        dashboardSubCatgeryListResponsePojos = new DashboardSubCatgeryListResponsePojo[1000];

        ButterKnife.bind(this);


        toolbar.setNavigationIcon(R.drawable.ic_back_while);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (intent != null) {

            Bundle bundle = intent.getExtras();

            Code = bundle.getString("code");
            titleName = bundle.getString("Name");
            TextViewTitle.setText(titleName);

            toolbar.setTitle(titleName);
            List<DashboardResponsePojo> thumbs = (List<DashboardResponsePojo>) bundle.getSerializable("Valuue");
            int size = thumbs.size();
            for (int i = 0; i < size; i++) {
                String aa = thumbs.get(i).getCatogeryId();
                String NAme = thumbs.get(i).getCatogeryName();

                if (aa.equals(Code)) {

                    int LstSize = thumbs.get(i).getSubCatgeryListResponse().length;
                    dashboardSubCatgeryListResponsePojos = thumbs.get(i).getSubCatgeryListResponse();
                    for (int j = 0; j < LstSize; j++) {

                        dashboardSubCatgeryListResponsePojos[j].getSubCatogeryId();
                        dashboardSubCatgeryListResponsePojos[j].getSubCatogeryName();
                        dashboardSubCatgeryListResponsePojos[j].getSubCatogeryPicthumbnail();
                        moreViewDashboardSubCardViewAdapter = new MoreViewDashboardSubCardViewAdapter(getApplicationContext(),
                                NAme, Code, thumbs.get(i).getSubCatgeryListResponse());
                        GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 3, GridLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(moreViewDashboardSubCardViewAdapter);


                    }

                }


            }


        }


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

