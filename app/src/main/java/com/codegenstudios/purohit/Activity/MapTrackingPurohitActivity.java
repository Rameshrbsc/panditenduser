package com.codegenstudios.purohit.Activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.TrackingPurohitLatLongPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.GpsTracker;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.maps.model.JointType.ROUND;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

public class MapTrackingPurohitActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    double longitude, latitude;
    private GpsTracker gpsTracker;
    Bundle bundle;
    String IntentPassing, BookingId, UserLat, UserLong, PurohitLat, PurohitLong, PurohitName;
    MyPreference myPreference;
    private ProgressDialog progressBar;
    private PendingIntent pendingIntent;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean MapClick = false;
    Polyline polylineFinal;
    boolean draw = true;
    LatLng SomePos;

    Bitmap mMarkerIcon;
    MarkerOptions markerOptionsPurohit, markerOptionsPurohitMove;
    Marker mCarMarker;
    private List<LatLng> polyLineList;
    int mIndexCurrentPoint = 0;
    private PolylineOptions polylineOptions, blackPolylineOptions;
    private Polyline blackPolyline, greyPolyLine;
    private Marker movingmarker;
    private LatLng sydney;
    private Handler handler;
    private int index, next;
    private LatLng startPosition, endPosition;
    boolean Purohitinitialmarker = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_tracking_purohit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        myPreference = new MyPreference(this);
        bundle = getIntent().getExtras();
        polyLineList = new ArrayList<>();

        mMarkerIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_house);

        if (bundle.getString("IntentPassing") != null) {
            IntentPassing = bundle.getString("IntentPassing");
            if (IntentPassing.equals("NotificationFullViewActivity")) {
                BookingId = bundle.getString("BookingId");
                UserLat = bundle.getString("UserLat");
                UserLong = bundle.getString("UserLong");
                PurohitName = bundle.getString("PurohitName");
                PurohitLat = bundle.getString("PurohitLat");
                PurohitLong = bundle.getString("PurohitLong");
                //fullview display
            } else if (IntentPassing.equals("BookingCardAdapter")) {

                BookingId = bundle.getString("BookingId");
                UserLat = bundle.getString("UserLat");
                UserLong = bundle.getString("UserLong");
                PurohitName = bundle.getString("PurohitName");
                PurohitLat = bundle.getString("PurohitLat");
                PurohitLong = bundle.getString("PurohitLong");
            }

        }


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(SupportTrackingReceiver.PUROHIT_MAP_UPDATE)) {
                    // new push notification is received
                    PurohitLat = intent.getStringExtra("purohitlat");
                    PurohitLong = intent.getStringExtra("purohitlong");
                    Log.e("recevierpurohitlat",PurohitLat);
                    Log.e("recevierpurohitlong",PurohitLong);
                    MarkingandDrawMethod(PurohitLat,PurohitLong);
                    Animationmove(PurohitLat,PurohitLong);



                }else if(intent.getAction().equals(SupportTrackingReceiver.PUROHIT_STOP_TRACKING)){

//                    MapTrackingPurohitActivity.this.finishAffinity();
                    onBackPressed();

                }
            }
        };

        getLocation();


    }

    private void Animationmove(String purohitLat, String purohitLong) {
        // pass purohit's api location in start postion ********* not done yet ********
        final LatLng startPosition = new LatLng(Double.parseDouble(purohitLat), Double.parseDouble(purohitLong));
        final LatLng finalPosition = new LatLng(Double.parseDouble(purohitLat), Double.parseDouble(purohitLong));
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        final boolean hideMarker = false;

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);

                LatLng currentPosition = new LatLng(
                        startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);

                movingmarker.setPosition(currentPosition);

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        movingmarker.setVisible(false);
                    } else {
                        movingmarker.setVisible(true);
                    }
                }
            }
        });



        Intent myIntent = new Intent(MapTrackingPurohitActivity.this, SupportTrackingReceiver.class);
        myIntent.putExtra("BookingId",BookingId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MapTrackingPurohitActivity.this, 100, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        assert manager != null;
        long time = System.currentTimeMillis();
        manager.set(AlarmManager.RTC_WAKEUP,  time+20000, pendingIntent);
        Log.e("Message", "Call Triggered");
    }

//


    public void getLocation() {

        gpsTracker = new GpsTracker(MapTrackingPurohitActivity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            Log.i("Lat and Log", "" + String.valueOf(latitude) + " " + String.valueOf(longitude));
        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap =googleMap;


        SomePos = new LatLng(Double.parseDouble(PurohitLat),Double.parseDouble( PurohitLong));

        //Create the URL to get request from first marker to second marker

        try {
//            if (googleMap == null) {
//                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
//            }
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.setTrafficEnabled(false);
            mMap.setIndoorEnabled(false);
            mMap.setBuildingsEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(SomePos,12.0f));

            movingmarker = mMap.addMarker(new MarkerOptions()
                    .position(SomePos)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
                    .title("Purohit"));

            MarkingandDrawMethod(PurohitLat, PurohitLong);
            Animationmove(PurohitLat,PurohitLong);


        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    private void MarkingandDrawMethod(String purohitLat, String purohitLong) {

        String url = getRequestUrl(Double.parseDouble(UserLat),Double.parseDouble(UserLong),Double.parseDouble(purohitLat), Double.parseDouble(purohitLong));//start user lat and long here
//
//        if (MapClick) {
//            mMap.clear();
//        }
//        MapClick = true;
        LatLng latLngUser = new LatLng(Double.parseDouble(UserLat),Double.parseDouble(UserLong));
        // adding a marker on map with image from  drawable
        mMap.addMarker(new MarkerOptions()
                .position(latLngUser)
                .title("Customer Location")
//                .snippet("Population: 4,627,300")
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.ic_house))));

            if(Purohitinitialmarker == true){

                LatLng latLngPurohitMove = new LatLng(Double.parseDouble(purohitLat) ,Double.parseDouble(purohitLong));
                mMap.addMarker(new MarkerOptions()
                        .position(latLngPurohitMove)
                        .title("Purohit Location")
//                .snippet("Population: 4,627,300")
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.ic_pin))));
                 Purohitinitialmarker = false;
            }


        TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
        taskRequestDirections.execute(url);





    }



//https://maps.googleapis.com/maps/api/directions/json?origin=13.040763,80.265116&destination=13.044494,80.270760&sensor=false&mode=driving&key=AIzaSyChzRKOIuTzIGf2vSHqSkURNUM_Tv9vNiE
    private String getRequestUrl(double latitude, double longitude, double purohitLat, double purohitLong) {
        //Value of origin
        String str_org = "origin=" + latitude +","+longitude;
        //Value of destination
        String str_dest = "destination=" + purohitLat+","+purohitLong;//purohit lat and long here
        //Set value enable the sensor
        String sensor = "sensor=false";
        //Mode for find direction
        String mode = "mode=driving";
        //Build the full param
        String param = str_org +"&" + str_dest + "&" +sensor+"&" +mode;
        //Output format
            String output = "json";
        //Create url to request
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param +"&key=AIzaSyChzRKOIuTzIGf2vSHqSkURNUM_Tv9vNiE";
        return url;
    }




    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String responseString = "";
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            TaskParser taskParser = new TaskParser();//1
            taskParser.execute(s);
        }
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try{
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>> > {//2

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            //Get list route and display it into the map

            ArrayList points = null;

            PolylineOptions polylineOptions = null;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat,lon));
                }

                polylineOptions.addAll(points);
                polylineOptions.width(15);
                polylineOptions.color(R.color.colorPrimary);
                polylineOptions.geodesic(true);
            }

            if (polylineOptions!=null) {

                if(draw == true){
                    polylineFinal = mMap.addPolyline(polylineOptions);
                     draw = false;
                }

            } else {
                Toast.makeText(getApplicationContext(), "Direction not found!", Toast.LENGTH_SHORT).show();
            }


        }

    }


    public class DirectionsParser {//3
        /**
         * Returns a list of lists containing latitude and longitude from a JSONObject
         */
        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try {

                jRoutes = jObject.getJSONArray("routes");

                // Loop for all routes
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
//                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("duration");

                    List path = new ArrayList<HashMap<String, String>>();

                    //Loop for all legs
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        //Loop for all steps
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");

                            polyLineList = decodePolyline(polyline);
//                            Loop for all points
                            for (int l = 0; l < polyLineList.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();

                                hm.put("lat", Double.toString(((LatLng) polyLineList.get(l)).latitude));
                                hm.put("lon", Double.toString(((LatLng) polyLineList.get(l)).longitude));




                                path.add(hm);




                            }

                        }
                        routes.add(path);


                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }

            return routes;
        }

        /**
         * Method to decode polyline
         * Source : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
         */
        private List decodePolyline(String encoded) {

            List poly = new ArrayList();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }

    }




    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SupportTrackingReceiver.PUROHIT_MAP_UPDATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SupportTrackingReceiver.PUROHIT_STOP_TRACKING));

    }

    @Override
    protected void onPause() {
        super.onPause();
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(MapTrackingPurohitActivity.this, SupportTrackingReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MapTrackingPurohitActivity.this, 100, myIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        assert alarmManager != null;
        alarmManager.cancel(pendingIntent);


        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }




    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }











}
