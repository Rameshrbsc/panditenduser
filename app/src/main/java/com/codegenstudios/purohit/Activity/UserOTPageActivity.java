package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Config.Config;
import com.codegenstudios.purohit.Pojo.ResendOTPPojo;
import com.codegenstudios.purohit.Pojo.SendOTPPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class UserOTPageActivity extends AppCompatActivity {
    @BindView(R.id.TextViewTitle)
    TextView TextViewTitle;
    @BindView(R.id.TextViewLoginClickTitle)
    TextView TextViewLoginClickTitle;
    @BindView(R.id.EditTextUserOTP)
    EditText EditTextUserOTP;
    @BindView(R.id.ButtonVeriftyOTP)
    Button ButtonVeriftyOTP;
    @BindView(R.id.ButtonResendOTP)
    Button ButtonResendOTP;
    String first,next,first1,next1,AccessToken,TempAccessToken,MoblieNumber,NotificationCount,IntentPassing;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";
    public static String  NOTIFICATION_COUNT = "NotificationCount";
    Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_otpage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // bind the view using butterknife
        ButterKnife.bind(this);
        myPreference = new MyPreference(UserOTPageActivity.this);
        bundle = getIntent().getExtras();
        try{
            if(bundle != null){
                TempAccessToken = bundle.getString("TempAccessToken");
                MoblieNumber =  bundle.getString("MoblieNumber");
                NotificationCount =  bundle.getString("NotificationCount");
                IntentPassing = bundle.getString("IntentPassing");
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        first = "We have sent an OTP via SMS to";
        next = "<font color='#F08400'>"+MoblieNumber+"</font>";
        TextViewTitle.setText(Html.fromHtml(first + next));
        first1 = "\u2190 Back to login Please ";
        next1 = "<font color='#F08400'> click here?</font>";
        TextViewLoginClickTitle.setText(Html.fromHtml(first1 + next1));
        TextViewLoginClickTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        ButtonVeriftyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(UserOTPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId",displayFirebaseRegId());
                    jsonObject.addProperty("Otp", EditTextUserOTP.getText().toString());
                    jsonObject.addProperty("AccessToken",TempAccessToken);
                    if(IntentPassing.equals("ForgotPassword")){
                        jsonObject.addProperty("ForgetPassword","1");
                    }else if(IntentPassing.equals("RegisterPage")){
                        jsonObject.addProperty("ForgetPassword","0");
                    }
                    API apiService = createServiceHeader(API.class);
                    Call<SendOTPPojo> call = apiService.SEND_OTP_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<SendOTPPojo>() {
                        @Override
                        public void onResponse(Call<SendOTPPojo> call, Response<SendOTPPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
//                                    Intent pushNotification = new Intent(NOTIFICATION_COUNT);
//                                    pushNotification.putExtra("NotificationCount",NotificationCount );
//                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("AccessToken", TempAccessToken);
                                    editor.putString("DeviceId", displayFirebaseRegId());
//                                    Log.i("NotificationCount",NotificationCount);
//                                    editor.putInt("NotificationCount", Integer.parseInt(NotificationCount));
                                    editor.apply();
                                    Intent intent1 = new Intent(UserOTPageActivity.this, UserNavigationActivity.class);
                                    startActivity(intent1);
                                    break;
                                case "108":
                                    Intent intent = new Intent(UserOTPageActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(UserOTPageActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<SendOTPPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(UserOTPageActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }else {
                    myPreference.ShowDialog(UserOTPageActivity.this,"Oops","There is not internet connection");
                }
            }
        });
        ButtonResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (myPreference.isInternetOn()) {

                    progressBar = new ProgressDialog(UserOTPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId",displayFirebaseRegId());
                    jsonObject.addProperty("AccessToken", TempAccessToken);

                    API apiService = createServiceHeader(API.class);
                    Call<ResendOTPPojo> call = apiService.RESEND_OTP_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<ResendOTPPojo>() {
                        @Override
                        public void onResponse(Call<ResendOTPPojo> call, Response<ResendOTPPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {

                                case "200":
                                    Log.i("newOTP",""+response.body().getOtp());
//                                    newOTP = response.body().getOtp();
                                    break;
                                case "108":
                                    Intent intent = new Intent(UserOTPageActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(UserOTPageActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResendOTPPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(UserOTPageActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }else {
                    myPreference.ShowDialog(UserOTPageActivity.this,"Oops","There is not internet connection");
                }
            }
        });


    }
    private String displayFirebaseRegId() {
        String regId;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("DeviceId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e("TAG", "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e("TAG", "Firebase Reg Id is not received yet!");
        return regId;
    }
}
