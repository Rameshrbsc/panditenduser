package com.codegenstudios.purohit.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.Pojo.UserProfileUpdatePojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;
public class UserProfileEditActivity extends AppCompatActivity {

    private ProgressDialog progressBar;
    MyPreference myPreference;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextUserEmail)
    EditText EditTextUserEmail;
    @BindView(R.id.EditTextNatchtathiram)
    EditText EditTextNatchtathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;

    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.FloatingActionButtonEditProfile)
    FloatingActionButton FloatingActionButtonEditProfile;
    String userChoosenTask;
    private Uri currentImageUri;
    Uri tempUri;
    private Target mTarget;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog;
    Bitmap thumbnail = null;
    File dir = new File(Environment.getExternalStorageDirectory() + "/PurohitUserApp/.Images");
    //keep track of cropping intent
    final int PIC_CROP = 2;
    String encodedImage;
    String FilePath,ImageURL;
    File camimage;
    UserProfileViewResponsePojo userProfileViewResponsePojo;
    private  int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private  int REQUEST_PERMISSION_SETTING = 111;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bundle = getIntent().getExtras();
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);


        EditTextAadhar3.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if(bundle!=null){
            EditTextUserName.setText(bundle.getString("UserName"));
            EditTextMoblieNumber.setText(bundle.getString("UserMoblieNumber"));
            EditTextUserEmail.setText(bundle.getString("UserEmailID"));
            EditTextSection.setText(bundle.getString("UserSection"));
            EditTextNatchtathiram.setText(bundle.getString("UserNatchtathiram"));
            EditTextGothram.setText(bundle.getString("UserGothram"));
            EditTextLanguage.setText(bundle.getString("UserLanguage"));
            String Aadharnumber = bundle.getString("UserAadhar");

            String[] separate = Arrays.toString(splitToNChar(Aadharnumber,4)).split(", ");
            EditTextAadhar1.setText(Aadharnumber.substring(0,4));
            EditTextAadhar2.setText(separate[1]);
            EditTextAadhar3.setText(separate[2]);

            EditTextAadhar1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(EditTextAadhar1.getText().toString().length()==4){
                        EditTextAadhar2.requestFocus();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            EditTextAadhar2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(EditTextAadhar2.getText().toString().length()==4){
                        EditTextAadhar3.requestFocus();
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length() == 0){
                        EditTextAadhar1.requestFocus();
                    }
                }
            });
            EditTextAadhar3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length() == 0){
                        EditTextAadhar2.requestFocus();
                    }

                }
            });
            if(bundle.getString("UserPicURI").equals("")){

                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.panditlogo);
                RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), getclip(largeIcon));
                roundDrawable.setCircular(true);
                ImageViewProfilePic.setImageDrawable(roundDrawable);
            }else{

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Picasso.with(UserProfileEditActivity.this).load(bundle.getString("UserPicURI")).transform(new CircleTransform()).into(ImageViewProfilePic);
                }
            }



        }

            FloatingActionButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(UserProfileEditActivity.this,  new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        });
        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String AadhaNumber = EditTextAadhar1.getText().toString()  + EditTextAadhar2.getText().toString()  + EditTextAadhar3.getText().toString();

                if (myPreference.isInternetOn()) {
                    if(AadhaNumber.length()==12){
                        {

                            ButtonSubmit.setVisibility(View.GONE);
                            progressBar = new ProgressDialog(UserProfileEditActivity.this);
                            progressBar.setCancelable(true);
                            progressBar.setMessage("Processing ...");
                            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressBar.setProgress(0);
                            progressBar.setMax(100);
                            progressBar.show();
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                            jsonObject.addProperty("UserName", EditTextUserName.getText().toString());
                            jsonObject.addProperty("UserEmailID", EditTextUserEmail.getText().toString());
                            jsonObject.addProperty("UserPhoneNumber", EditTextMoblieNumber.getText().toString());
                            jsonObject.addProperty("UserSection", EditTextSection.getText().toString());//send section data
                            jsonObject.addProperty("UserNatchatram", EditTextNatchtathiram.getText().toString());
                            jsonObject.addProperty("UserGothram", EditTextGothram.getText().toString());
                            jsonObject.addProperty("UserLanguage", EditTextLanguage.getText().toString());
                            jsonObject.addProperty("UserAadharNumber", AadhaNumber);
                            if (encodedImage != null) {
                                jsonObject.addProperty("UserProfilePic", encodedImage);
                            } else {
                                jsonObject.addProperty("UserProfilePic", "");
                            }
                            API apiService = createServiceHeader(API.class);
                            Call<UserProfileUpdatePojo> call = apiService.USER_PROFILE_UPDATE_POJO_CALL(jsonObject);
                            call.enqueue(new Callback<UserProfileUpdatePojo>() {
                                @Override
                                public void onResponse(Call<UserProfileUpdatePojo> call, Response<UserProfileUpdatePojo> response) {
                                    progressBar.dismiss();
                                    ButtonSubmit.setVisibility(View.VISIBLE);
                                    switch (response.body().getCode()) {
                                        case "200":
                                            onBackPressed();
                                            break;
                                        case "108":
                                            Intent intent = new Intent(UserProfileEditActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            break;
                                        default:
                                            myPreference.ShowDialog(UserProfileEditActivity.this,"Oops",response.body().getMessage());
                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Call<UserProfileUpdatePojo> call, Throwable t) {
                                    progressBar.dismiss();
                                    try {
                                        Toast.makeText(UserProfileEditActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });
                        }
                    }else{
                        myPreference.ShowDialog(UserProfileEditActivity.this,"Oops","Invalid Aadhar Number");
                    }
                }
                else {
                    myPreference.ShowDialog(UserProfileEditActivity.this,"Oops","There is not internet connection");
                }


            }
        });




    }


    private void selectImage() {
        final String [] value= getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UserProfileEditActivity.this);
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    userChoosenTask="Take Photo";
                        alertDialog.dismiss();
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        dir.mkdirs();

                        if (dir.isDirectory()){
                            Log.i("FileCreated","FileCreated");

                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            camimage = new File(dir,"PurhoitAPP"+ timeStamp + ".jpg");
                            currentImageUri = Uri.fromFile( camimage );//file
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                            startActivityForResult(cameraIntent, REQUEST_CAMERA);
                        }else {
                            Log.i("FileNotCreated","FileNotCreated");
                        }


                } else if (position == 1) {

                    userChoosenTask="Choose from Library";


                        alertDialog.dismiss();
                        startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                        Log.i("onsave","going to gallery ");

                }else  if(position == 2){
                    alertDialog.dismiss();
                    userChoosenTask="Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {//camera
                camimage= new File(currentImageUri.getPath());
                Log.i("Imagetest",""+camimage);

                if(currentImageUri!=null){
                    try {
                        FilePath = currentImageUri.getPath();
                        LoadImage(camimage);
                        EncodedMethodCall();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(UserProfileEditActivity.this.getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave","data got from gallery ");

                    FilePath = getRealPathFromURI(UserProfileEditActivity.this,currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if(imageFileFilter.accept(camimage)){
                        Log.i("GalleryImageLoaded","GalleryImageLoaded");
                        LoadImage(camimage);
                        EncodedMethodCall();

                    }else{
                        Toast.makeText(this, "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }else  if (requestCode == REQUEST_PERMISSION_SETTING){
                if (checkPermissions()){
                    selectImage();
                }
            }
        }
    }

    private void LoadImage(File Path) {
        Picasso.with(this).load(Path).transform(new SignUpActivity.CircleTransform()).into(ImageViewProfilePic);
        alertDialog.dismiss();
    }

    private void CamLoadImage(Bitmap thumbnail) {

        progressBar = new ProgressDialog(UserProfileEditActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Processing ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }

        ImageViewProfilePic.setImageBitmap(thumbnail);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        progressBar.dismiss();
//        RelativelayoutImageView.setVisibility(View.VISIBLE);
//        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    //Some device image get rotated
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }



    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    private void EncodedMethodCall() {
        Bitmap bm = BitmapFactory.decodeFile(camimage.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.i("encodedImage",""+encodedImage);
    }

     class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static Bitmap getclip(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 4, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
    private Uri getImageFileUri() {
        if (! dir.exists()){
            if (! dir.mkdirs()){
                Log.d("CameraTestIntent", "failed to create directory");
                return null;
            }else{
                Log.d("Imagetest","create new Purohit folder");
            }
        }
        // Create an image file name
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File image = new File(dir,"PurhoitAPP"+ timeStamp + ".jpg");
        if(!image.exists()){
            try {
                image.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        // Create an File Uri
        return Uri.fromFile(image);
    }




    public class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    private static String[] splitToNChar(String text, int size) {
        List<String> parts;
        parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(UserProfileEditActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE){
            if (checkPermissions()){
                selectImage();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

}


