package com.codegenstudios.purohit.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.text.format.DateFormat;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.Pojo.BookingFormSubmitPojo;
import com.codegenstudios.purohit.Pojo.SignUpPojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.DateDialog;
import com.codegenstudios.purohit.SupportClass.MyPreference;
import com.codegenstudios.purohit.SupportClass.TimePickerFragment;

//import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Activity.SignUpActivity.isMobValid;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class BookingFormActivity extends AppCompatActivity {
    @BindView(R.id.EditTextLocation)
    EditText EditTextLocation;
    @BindView(R.id.EditTextDate)
    EditText EditTextDate;
    @BindView(R.id.EditTextTime)
    EditText EditTextTime;
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.EditTextEmailId)
    EditText EditTextEmailId;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextPoojaName)
    EditText EditTextPoojaName;
    @BindView(R.id.EditTextCatogery)
    EditText EditTextCatogery;
    @BindView(R.id.EditTextSubCatogery)
    EditText EditTextSubCatogery;
    @BindView(R.id.EditTextAmount)
    EditText EditTextAmount;
    @BindView(R.id.name_linear)
    LinearLayout name_linear;
    @BindView(R.id.email_linear)
    LinearLayout email_linear;

    @BindView(R.id.phone_linear)
    LinearLayout phone_linear;



    String MY_PREFS_NAME = "MyPrefsFile", LocationAddress = "", Longitude = "", Latitude = "", dates, times;
    String CatId, SubId, Name, Email, MoblieNumber, Address, Language, PoojaName, CategoryName, SubCategoryName, Amount, PoojaID, IntentPassing, IntentPassingType, am_pm, imageIcon;
    UserProfileViewResponsePojo userProfileViewResponsePojo;
    Bundle bundle;
    int hour_x;
    int minute_x;

    private ProgressDialog progressBar;
    MyPreference myPreference;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Booking Details");
        bundle = getIntent().getExtras();

        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                Intent intent = new Intent(BookingFormActivity.this, BookingCardListActivity.class);
                intent.putExtra("IntentPassing", "BookingFormActivity");
                intent.putExtra("CatId", bundle.getString("CatId"));
                intent.putExtra("CatName", bundle.getString("CatName"));
                intent.putExtra("SubId", bundle.getString("SubId"));
                intent.putExtra("SubName", bundle.getString("SubName"));
                intent.putExtra("Activity", bundle.getString("book"));
                startActivity(intent);
            }
        });
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);

        if (bundle != null && bundle.containsKey("IntentPassing")) {
            IntentPassing = bundle.getString("IntentPassing");
            IntentPassingType = bundle.getString("IntentPassingType");

            if (IntentPassing.equals("MyBookingCardActivity")) {

                PoojaID = bundle.getString("PoojaID");
                PoojaName = bundle.getString("PoojaName");
                Amount = bundle.getString("Amount");
                CatId = bundle.getString("CatId");
                CategoryName = bundle.getString("CatName");
                SubId = bundle.getString("SubId");
                SubCategoryName = bundle.getString("SubName");
                imageIcon = bundle.getString("imageIcon");

                EditTextPoojaName.setText(PoojaName);
                EditTextCatogery.setText(CategoryName);
                EditTextSubCatogery.setText(SubCategoryName);
                EditTextAmount.setText(Amount);
                myPreference.setImage(getApplicationContext(), imageIcon);

            } else if (IntentPassing.equals("LocationSearchingActivity") && IntentPassingType.equals("CurrentLocation")) {
                PoojaID = bundle.getString("PoojaID");
                PoojaName = bundle.getString("PoojaName");
                Amount = bundle.getString("Amount");
                CatId = bundle.getString("CatId");
                CategoryName = bundle.getString("CatName");
                SubId = bundle.getString("SubId");
                SubCategoryName = bundle.getString("SubName");

                LocationAddress = bundle.getString("LocationResult");
                Latitude = bundle.getString("Lat");
                Longitude = bundle.getString("Long");

                Name = bundle.getString("Name");
                Email = bundle.getString("Email");
                MoblieNumber = bundle.getString("MoblieNumber");
                Language = bundle.getString("Language");


                EditTextUserName.setText(Name);
                EditTextEmailId.setText(Email);
                EditTextMoblieNumber.setText(MoblieNumber);
                EditTextLanguage.setText(Language);
                EditTextLocation.setText(LocationAddress);
                EditTextPoojaName.setText(PoojaName);
                EditTextCatogery.setText(CategoryName);
                EditTextSubCatogery.setText(SubCategoryName);
                EditTextAmount.setText(Amount);

            } else if (IntentPassing.equals("LocationSearchingActivity")) {
                PoojaID = bundle.getString("PoojaID");
                PoojaName = bundle.getString("PoojaName");
                Amount = bundle.getString("Amount");
                CatId = bundle.getString("CatId");
                CategoryName = bundle.getString("CatName");
                SubId = bundle.getString("SubId");
                SubCategoryName = bundle.getString("SubName");
                LocationAddress = bundle.getString("LocationResult");
                Latitude = bundle.getString("Lat");
                Longitude = bundle.getString("Long");
                Name = bundle.getString("Name");
                Email = bundle.getString("Email");
                MoblieNumber = bundle.getString("MoblieNumber");
                Language = bundle.getString("Language");


                EditTextUserName.setText(Name);
                EditTextEmailId.setText(Email);
                EditTextMoblieNumber.setText(MoblieNumber);
                EditTextLanguage.setText(Language);
                EditTextLocation.setText(LocationAddress);
                EditTextPoojaName.setText(PoojaName);
                EditTextCatogery.setText(CategoryName);
                EditTextSubCatogery.setText(SubCategoryName);
                EditTextAmount.setText(Amount);

            }


        }


        EditTextLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingFormActivity.this, LocationSearchingActivity.class);
                intent.putExtra("IntentPassing", "BookingFormActivity");
                intent.putExtra("Name", EditTextUserName.getText().toString());
                intent.putExtra("Email", EditTextEmailId.getText().toString());
                intent.putExtra("MoblieNumber", EditTextMoblieNumber.getText().toString());
                intent.putExtra("LocationResult", EditTextLocation.getText().toString());
                intent.putExtra("Language", EditTextLanguage.getText().toString());
                intent.putExtra("PoojaName", EditTextPoojaName.getText().toString());
                intent.putExtra("PoojaID", PoojaID);
                intent.putExtra("CatName", EditTextCatogery.getText().toString());
                intent.putExtra("CatId", CatId);
                intent.putExtra("SubName", EditTextSubCatogery.getText().toString());
                intent.putExtra("SubId", SubId);
                intent.putExtra("Amount", EditTextAmount.getText().toString());
                intent.putExtra("LocationResult", LocationAddress);
                intent.putExtra("Lat", Latitude);
                intent.putExtra("Long", Longitude);
                startActivity(intent);

            }
        });

        //getting default value form database username,number,email

        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(BookingFormActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            API apiService = createServiceHeader(API.class);
            Call<UserProfileViewPojo> call = apiService.USER_PROFILE_VIEW_POJO_CALL(jsonObject);
            call.enqueue(new Callback<UserProfileViewPojo>() {
                @Override
                public void onResponse(Call<UserProfileViewPojo> call, Response<UserProfileViewPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            userProfileViewResponsePojo = response.body().getResponse();
                            EditTextUserName.setText(userProfileViewResponsePojo.getUserName());
                            EditTextMoblieNumber.setText(userProfileViewResponsePojo.getUserPhone());
                            EditTextEmailId.setText(userProfileViewResponsePojo.getUserEmail());
                            EditTextLanguage.setText(userProfileViewResponsePojo.getLanguage());

                            break;
                        case "108":
                            Intent intent = new Intent(BookingFormActivity.this, LoginActivity.class);
                            startActivity(intent);
                        default:
                            myPreference.ShowDialog(BookingFormActivity.this, "Oops", response.body().getMessage());
                            break;
                    }

                }

                @Override
                public void onFailure(Call<UserProfileViewPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        Toast.makeText(BookingFormActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            myPreference.ShowDialog(BookingFormActivity.this, "Oops", "There is not internet connection");
        }


        EditTextLocation.setText(LocationAddress);
        //fetching date
        EditTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDialog dialog = new DateDialog(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "DatePicker");

            }
        });
        if (dates == null) {
            EditTextDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
        }
//fetching time
        EditTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    DialogFragment newFragment = new TimePickerFragment(v);
//                    newFragment.show(BookingFormActivity.this.getSupportFragmentManager(), "DialogTime");

                DialogFragment newFragment = new TimePickerFragment(v);
                newFragment.show(getSupportFragmentManager(), "timePicker");
//                    showDialog(0);

            }
        });

//submit the form

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                if(!Check(EditTextLocation)){
                    EditTextLocation.setError("Enter Your Location");
                    EditTextLocation.setFocusable(true);

                }else  if(!Check(EditTextTime)){
                    EditTextTime.setError("select date");
                    EditTextTime.setFocusable(true);

                }else  if(!Check(EditTextDate)){
                    EditTextDate.setError("select time");
                    EditTextDate.setFocusable(true);

                }else {

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = new Date();
                    //Time Processing
                    Date ConvertedTime;
                    int DifferenceInHours = 0;
                    try {
                        //Calculating Time difference
                        ConvertedTime = new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).parse(EditTextTime.getText().toString() + " " + EditTextDate.getText().toString());
//                            Date CurrentTime = new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).parse(new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).format(new Date()));
                        long DifferenceTime = ConvertedTime.getTime() - new Date().getTime();
                        DifferenceInHours = (int) ((DifferenceTime - (1000 * 60 * 60 * 24 * (int) (DifferenceTime / (1000 * 60 * 60 * 24)))) / (1000 * 60 * 60));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if ((DifferenceInHours > 1)) {
                        Intent intentConfirm = new Intent(BookingFormActivity.this, BookingConfirmPageActivity.class);
                        intentConfirm.putExtra("BookingName", EditTextUserName.getText().toString());
                        intentConfirm.putExtra("BookingEmailID", EditTextEmailId.getText().toString());
                        intentConfirm.putExtra("BookingMoblieNumber", EditTextMoblieNumber.getText().toString());
                        intentConfirm.putExtra("BookingAmount", EditTextAmount.getText().toString());
                        intentConfirm.putExtra("BookingLanguage", EditTextLanguage.getText().toString());
                        intentConfirm.putExtra("BookingAddress", EditTextLocation.getText().toString());
                        intentConfirm.putExtra("Bookinglat", Latitude);
                        intentConfirm.putExtra("Bookinglong", Longitude);
                        intentConfirm.putExtra("BookingPoojaId", PoojaID);
                        intentConfirm.putExtra("BookingPoojaName", EditTextPoojaName.getText().toString());
                        intentConfirm.putExtra("BookingCatogeryId", CatId);
                        intentConfirm.putExtra("BookingCatogeryName", EditTextCatogery.getText().toString());
                        intentConfirm.putExtra("BookingSubCatogeryId", SubId);
                        intentConfirm.putExtra("BookingSubCatogeryName", EditTextSubCatogery.getText().toString());
                        intentConfirm.putExtra("BookingPoojaName", EditTextPoojaName.getText().toString());
                        intentConfirm.putExtra("BookingDate", EditTextDate.getText().toString());
                        intentConfirm.putExtra("BookingTime", EditTextTime.getText().toString());
                        intentConfirm.putExtra("IntentPassing", "BookingFormActivity");

                        startActivity(intentConfirm);

                    } else {
                        Toast.makeText(BookingFormActivity.this, EditTextDate.getText().toString() + EditTextTime.getText().toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(BookingFormActivity.this, "Time must be in future!!", Toast.LENGTH_SHORT).show();
                    }


                }



                /*if (Check(EditTextUserName) && Check(EditTextMoblieNumber) && Check(EditTextAmount) && Check(EditTextLanguage) && Check(EditTextLocation) && Check(EditTextTime) && Check(EditTextDate)) {
                    if (isMobValid(EditTextMoblieNumber.getText().toString())) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = new Date();
                        //Time Processing
                        Date ConvertedTime;
                        int DifferenceInHours = 0;
                        try {
                            //Calculating Time difference
                            ConvertedTime = new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).parse(EditTextTime.getText().toString() + " " + EditTextDate.getText().toString());
//                            Date CurrentTime = new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).parse(new SimpleDateFormat("hh:mm a dd-MM-yyyy", Locale.getDefault()).format(new Date()));
                            long DifferenceTime = ConvertedTime.getTime() - new Date().getTime();
                            DifferenceInHours = (int) ((DifferenceTime - (1000 * 60 * 60 * 24 * (int) (DifferenceTime / (1000 * 60 * 60 * 24)))) / (1000 * 60 * 60));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if ((DifferenceInHours > 1)) {
                            Intent intentConfirm = new Intent(BookingFormActivity.this, BookingConfirmPageActivity.class);
                            intentConfirm.putExtra("BookingName", EditTextUserName.getText().toString());
                            intentConfirm.putExtra("BookingEmailID", EditTextEmailId.getText().toString());
                            intentConfirm.putExtra("BookingMoblieNumber", EditTextMoblieNumber.getText().toString());
                            intentConfirm.putExtra("BookingAmount", EditTextAmount.getText().toString());
                            intentConfirm.putExtra("BookingLanguage", EditTextLanguage.getText().toString());
                            intentConfirm.putExtra("BookingAddress", EditTextLocation.getText().toString());
                            intentConfirm.putExtra("Bookinglat", Latitude);
                            intentConfirm.putExtra("Bookinglong", Longitude);
                            intentConfirm.putExtra("BookingPoojaId", PoojaID);
                            intentConfirm.putExtra("BookingPoojaName", EditTextPoojaName.getText().toString());
                            intentConfirm.putExtra("BookingCatogeryId", CatId);
                            intentConfirm.putExtra("BookingCatogeryName", EditTextCatogery.getText().toString());
                            intentConfirm.putExtra("BookingSubCatogeryId", SubId);
                            intentConfirm.putExtra("BookingSubCatogeryName", EditTextSubCatogery.getText().toString());
                            intentConfirm.putExtra("BookingPoojaName", EditTextPoojaName.getText().toString());
                            intentConfirm.putExtra("BookingDate", EditTextDate.getText().toString());
                            intentConfirm.putExtra("BookingTime", EditTextTime.getText().toString());
                            intentConfirm.putExtra("IntentPassing", "BookingFormActivity");

                            startActivity(intentConfirm);

                        } else {
                            Toast.makeText(BookingFormActivity.this, EditTextDate.getText().toString() + EditTextTime.getText().toString(), Toast.LENGTH_SHORT).show();
                            Toast.makeText(BookingFormActivity.this, "Time must be in future!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BookingFormActivity.this, "Email and mobile enter correct", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    EditTextLocation.setError("Enter Your Location");
                    // Toast.makeText(BookingFormActivity.this, "Missing fields", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BookingFormActivity.this, BookingCardListActivity.class);
        intent.putExtra("IntentPassing", "BookingFormActivity");
        intent.putExtra("CatId", bundle.getString("CatId"));
        intent.putExtra("CatName", bundle.getString("CatName"));
        intent.putExtra("SubId", bundle.getString("SubId"));
        intent.putExtra("SubName", bundle.getString("SubName"));
        intent.putExtra("Activity", bundle.getString("book"));
        startActivity(intent);

    }


  /*  @Override
    protected Dialog onCreateDialog(int id) {
        if(id ==0)
            return new TimePickerDialog(BookingFormActivity.this,R.style.MyTimePickerDialogStyle,KTimePickerListner,hour_x,minute_x,false);
        return null;
    }


    protected  TimePickerDialog.OnTimeSetListener KTimePickerListner = new TimePickerDialog.OnTimeSetListener() {

        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            hour_x = hourOfDay % 12;
            if (hour_x == 0)
                hour_x = 12;


            String hour = String.valueOf(((hourOfDay > 12) ? hourOfDay % 12 : hourOfDay));
            String min = String.valueOf((minute < 10 ? ("0" + minute) : minute));
            // TODO: need to change the AM and PM format as per the time format on different devices
            am_pm = ((hourOfDay >= 12) ? "PM" : "AM");

            EditTextTime.setText(hour+":"+min+" "+am_pm);

            hour_x = Integer.parseInt(hour);
            minute_x= Integer.parseInt(min);


        }
    };*/


/*
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    android.text.format.DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            int a= hourOfDay;
            int b= minute;
            String startTime= String.valueOf(a)+":"+String.valueOf(b)+":00";
            StringTokenizer tk = new StringTokenizer(startTime);
            String time = tk.nextToken();

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
            Date dt;

            try {
                dt = sdf.parse(time);
//                System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here

            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }
*/
}
