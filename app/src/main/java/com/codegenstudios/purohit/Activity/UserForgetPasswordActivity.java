package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Config.Config;
import com.codegenstudios.purohit.Pojo.ForgetPasswordPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class UserForgetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.ButtonVeriftyOTP)
    Button ButtonVeriftyOTP;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String regId,NotificationCount;
    public static String  NOTIFICATION_COUNT = "NotificationCount";
    String MY_PREFS_NAME = "MyPrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_forget_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        myPreference = new MyPreference(UserForgetPasswordActivity.this);

        ButtonVeriftyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (myPreference.isInternetOn()){
                    if(Check(EditTextMoblieNumber)){
                        progressBar = new ProgressDialog(UserForgetPasswordActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("UserPhoneNumber",EditTextMoblieNumber.getText().toString());
                        API apiService = createServiceHeader(API.class);
                        Call<ForgetPasswordPojo> call = apiService.FORGET_PASSWORD_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<ForgetPasswordPojo>() {
                            @Override
                            public void onResponse(Call<ForgetPasswordPojo> call, Response<ForgetPasswordPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                        pushNotification.putExtra("NotificationCount",response.body().getNotificationCount() );
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                        SharedPreferences.Editor editor1 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                        editor1.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                        Log.i("NotificationCount",response.body().getNotificationCount());
                                        editor1.apply();


                                        Intent intent = new Intent(UserForgetPasswordActivity.this,UserOTPageActivity.class);
//                                        intent.putExtra("OTPgot",""+response.body().getOtp());
                                        intent.putExtra("MoblieNumber",EditTextMoblieNumber.getText().toString());
                                        intent.putExtra("TempAccessToken",""+response.body().getAccessToken());
                                        intent.putExtra("IntentPassing","ForgotPassword");
                                        startActivity(intent);
                                        break;

                                    case "108":
                                        Intent intentemg = new Intent(UserForgetPasswordActivity.this, LoginActivity.class);
                                        startActivity(intentemg);
                                        break;
                                    default:
                                        myPreference.ShowDialog(UserForgetPasswordActivity.this,"Oops",response.body().getMessage());
                                        break;

                                }

                            }

                            @Override
                            public void onFailure(Call<ForgetPasswordPojo> call, Throwable t) {
                                progressBar.dismiss();
                                try {
                                    Toast.makeText(UserForgetPasswordActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }else{

                        Toast.makeText(UserForgetPasswordActivity.this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    final Dialog dialog = new Dialog(UserForgetPasswordActivity.this);
                    dialog.setContentView(R.layout.custom_failor_dialog);
                    Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                    TextView TextviewSmallTitle1 = (TextView)dialog.findViewById(R.id.TextviewSmallTitle1);
                    TextviewSmallTitle1.setText("There is no internet connection");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }

            }
        });




    }
    //validations
    boolean Check(EditText testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty();
    }
    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("DeviceId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }
}
