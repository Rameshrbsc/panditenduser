package com.codegenstudios.purohit.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.codegenstudios.purohit.Adapter.Feedback_adapter;
import com.codegenstudios.purohit.Pojo.FeedbackPojo;
import com.codegenstudios.purohit.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;

/**
 * Created by dawnshine1t on 19/10/17.
 */

public class Feedback_Activity extends AppCompatActivity {


    private RecyclerView recyclerViewinbox;
//    Session_Manager sm;
    //adapter object
    private Feedback_adapter adapterGifts;
    private LinearLayout /*linear_layout_android1,*/linear_layout_android;

    private  String noevents;
    //progress dialog
    private ProgressDialog progressDialog;

    //list to hold all the uploaded images
    private List<FeedbackPojo> historyModelList;


    public Feedback_Activity() {
    }

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_feedback);

        recyclerViewinbox = (RecyclerView) findViewById(R.id.recyclerView_feedback);
//        linear_layout_android1 = (LinearLayout) findViewById(R.id.linear_layout_android1);
        linear_layout_android = (LinearLayout) findViewById(R.id.linear_layout_android);
        recyclerViewinbox.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewinbox.setLayoutManager(llm);

        progressDialog = new ProgressDialog(this);
//        sm=new Session_Manager(getContext());

        historyModelList = new ArrayList<FeedbackPojo>();
        getData();
        noevents = "1";
        if (noevents.equals("0")) {
//            linear_layout_android1.setVisibility(View.VISIBLE);
            linear_layout_android.setVisibility(View.GONE);
        }

    }

    public void getData()
    {
        try
        {
            URL url=new URL("");
            HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream os=httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
//            String session_id= sm.getSessionId();
//            String data="session_id="+ URLEncoder.encode("","UTF-8");
            bufferedWriter.write("");
            bufferedWriter.flush();
            bufferedWriter.close();
            os.close();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String line="";
            String response="";

            while ((line=bufferedReader.readLine())!=null)
            {
                response+=line;
            }


            historyModelList.add(new FeedbackPojo( "5 Jan 2019", "Purohit", "He is really nice and he does the pooja very well.","",  "1","4.5"));



        }
        catch (Exception e)
        {
            e.printStackTrace();

            if (e.toString().contains("java.net.MalformedURLException: Protocol not found: ")){

                historyModelList.add(new FeedbackPojo( "5 Jan 2019", "Purohit", "He is really nice and he does the pooja very well.","",  "1","4.5"));

            }
        }
        //creating adapter
        adapterGifts = new Feedback_adapter(this, historyModelList);

        //adding adapter to recyclerview
        recyclerViewinbox.setAdapter(adapterGifts);
        adapterGifts.notifyDataSetChanged();
    }


}
