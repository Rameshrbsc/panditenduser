package com.codegenstudios.purohit.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.GpsTracker;
import com.codegenstudios.purohit.SupportClass.LocationAddress;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.os.Build.ID;

public class LocationSearchingActivity extends AppCompatActivity {
    @BindView(R.id.TextViewLocationLatLag)
    TextView TextViewLocationLatLag;
   @BindView(R.id.TextViewAddress)
   EditText TextViewAddress;
   @BindView(R.id.TextViewSearching)
   TextView TextViewSearching;
   @BindView(R.id.ButtonSubmit)
   Button ButtonSubmit;
       private GpsTracker gpsTracker;
    Geocoder geocoder;
    String latitude,longitude;
    String locationAddress,MY_PREFS_NAME = "MyPrefsFile",Name,Email, MoblieNumber,PoojaID, CatId,Language,SubId,PoojaName,CategoryName,SubCategoryName, Amount,ShareOpertion,IntentPassing,latitudeMap,longitudeMap,CurrentLocationTap;
    private ProgressDialog progressBar;
    Bundle bundle;
    SharedPreferences myperf;
    MyPreference myPreference;





    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_searching);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference( this);
        setTitle("Choose your locaiton");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
//        ActivityCompat.requestPermissions(SignUpActivity.this,  new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            IntentPassing =  bundle.getString("IntentPassing");

            if(IntentPassing.equals("MapSearchingActivity")){

                PoojaID = bundle.getString("PoojaId");
                PoojaName =bundle.getString("PoojaName");
                Amount = bundle.getString("Amount");
                CatId = bundle.getString("CatId");
                CategoryName = bundle.getString("CatName");
                SubId = bundle.getString("SubId");
                SubCategoryName = bundle.getString("SubName");

                locationAddress = bundle.getString("LocationResult");
                latitude = bundle.getString("Lat");
                longitude = bundle.getString("Long");

                Name = bundle.getString("Name");
                Email = bundle.getString("Email");
                MoblieNumber = bundle.getString("MoblieNumber");
                Language = bundle.getString("Language");

                TextViewAddress.setText(locationAddress);

            }else if(IntentPassing.equals("BookingFormActivity")){
               PoojaID = bundle.getString("PoojaID");
                PoojaName =bundle.getString("PoojaName");
                Amount = bundle.getString("Amount");
                CatId = bundle.getString("CatId");
                CategoryName = bundle.getString("CatName");
                SubId = bundle.getString("SubId");
                SubCategoryName = bundle.getString("SubName");

                locationAddress = bundle.getString("LocationResult");
                latitude =bundle.getString("Lat");
                longitude =bundle.getString("Long");

                Name = bundle.getString("Name");
                Email = bundle.getString("Email");
                MoblieNumber = bundle.getString("MoblieNumber");
                Language = bundle.getString("Language");

                TextViewAddress.setText(locationAddress);

            }



            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }


        geocoder = new Geocoder(this, Locale.getDefault());
        TextViewLocationLatLag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation(v);//get the lat and log
                if(!latitude.equals("")||!longitude.equals("")){
                    LocationAddress locationAddress = new LocationAddress();//passing the lat and log
                    locationAddress.getAddressFromLocation( Double.parseDouble(latitude),  Double.parseDouble(longitude), getApplicationContext(), new GeocoderHandler());//getting full addresss
                }else{
                    myPreference.ShowDialog(LocationSearchingActivity.this,"Oops","Please turn on the GPS first");
                }

            }
        });

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!Check(TextViewAddress)){
                    TextViewAddress.setError("Location is empty");
                    TextViewAddress.setFocusable(true);
                }else {
                    Intent intent = new Intent(LocationSearchingActivity.this, BookingFormActivity.class);
                    getLocationFromAddress(LocationSearchingActivity.this, TextViewAddress.getText().toString());
                    intent.putExtra("IntentPassing", "LocationSearchingActivity");
                    intent.putExtra("IntentPassingType", "CurrentLocation");
                    intent.putExtra("LocationResult", TextViewAddress.getText().toString());
                    intent.putExtra("Lat", latitude);
                    intent.putExtra("Long", longitude);
                    intent.putExtra("Name", Name);
                    intent.putExtra("Email", Email);
                    intent.putExtra("MoblieNumber", MoblieNumber);
                    intent.putExtra("Language", Language);
                    intent.putExtra("PoojaName", PoojaName);
                    intent.putExtra("PoojaID", PoojaID);
                    intent.putExtra("CatName", CategoryName);
                    intent.putExtra("CatId", CatId);
                    intent.putExtra("SubName", SubCategoryName);
                    intent.putExtra("SubId", SubId);
                    intent.putExtra("Amount", Amount);
                    startActivity(intent);
                    onBackPressed();
                }
            }
        });
        TextViewSearching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(LocationSearchingActivity.this,MapSeacrchingActivity.class);
                intent.putExtra("IntentPassing","LocationSearchingActivity");
                intent.putExtra("Name",Name);
                intent.putExtra("Email",Email);
                intent.putExtra("MoblieNumber",MoblieNumber);
                intent.putExtra("Language",Language);
                intent.putExtra("PoojaName",PoojaName);
                intent.putExtra("PoojaID",PoojaID);
                intent.putExtra("CatName",CategoryName);
                intent.putExtra("CatId",CatId);
                intent.putExtra("SubName",SubCategoryName);
                intent.putExtra("SubId",SubId);
                intent.putExtra("Amount",Amount);
                startActivity(intent);
            }
        });


    }



    public void getLocation(View view) {
        gpsTracker = new GpsTracker(LocationSearchingActivity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            Log.i("Lat and Log", "" + String.valueOf(latitude) + " " + String.valueOf(longitude));
        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
//            Toast.makeText(LocationSearchingActivity.this, "Address--->"+locationAddress, Toast.LENGTH_SHORT).show();
            Log.i("Address ",""+locationAddress);//result of full address
            //location  has been loading to the list
            TextViewAddress.setText(locationAddress);


        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            Log.i("",""+p1);


        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

}
