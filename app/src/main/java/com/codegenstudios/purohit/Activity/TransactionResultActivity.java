package com.codegenstudios.purohit.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.LoginPojo;
import com.codegenstudios.purohit.Pojo.TransactionSuccessSubmitPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class TransactionResultActivity extends AppCompatActivity {

    MyPreference myPreference;
    String TransactionResult;
    @BindView(R.id.ButtonSuccess)
    Button ButtonSuccess;
    @BindView(R.id.ButtonFailed)
    Button ButtonFailed;
    @BindView(R.id.ButtonCancelled)
    Button ButtonCancelled;
    private ProgressDialog progressBar;
    String BookingId;
    private AlertDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
        final Intent intent = getIntent();
        TransactionResult = Objects.requireNonNull(intent.getExtras().get("TransactionResult")).toString();
        BookingId = Objects.requireNonNull(intent.getExtras().get("BookingId")).toString();

        if(TransactionResult.equals("Success")){
            ButtonSuccess.setVisibility(View.VISIBLE);
        }else{
            ButtonSuccess.setVisibility(View.GONE);
        }
        ButtonSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myPreference.isInternetOn()){
//                        progressBar = new ProgressDialog(TransactionResultActivity.this);
//                        progressBar.setCancelable(true);
////                        progressBar.setMessage("Processing ...");
//                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                        progressBar.setProgress(0);
//                        progressBar.setMax(100);
//                        progressBar.setIcon(getResources().getDrawable(R.drawable.user));
//                        progressBar.show();
                    final AlertDialog.Builder imageDialog = new AlertDialog.Builder(TransactionResultActivity.this, R.style.RajCustomDialog);
                    final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                    final View dialogView = inflater.inflate(R.layout.content_show_gif, null);
                    imageDialog.setView(dialogView);
                    final AlertDialog alertdialog = imageDialog.create();
                    alertdialog.show();
                        JsonObject jsonObject = new JsonObject();
                         jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                         jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                         jsonObject.addProperty("BookingId",BookingId);
                         jsonObject.addProperty("TransactionId","sdfsddf");
                         jsonObject.addProperty("Status","1");
                        API apiService = createServiceHeader(API.class);
                        Call<TransactionSuccessSubmitPojo> call = apiService.TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<TransactionSuccessSubmitPojo>() {
                            @Override
                            public void onResponse(Call<TransactionSuccessSubmitPojo> call, Response<TransactionSuccessSubmitPojo> response) {
//                                progressBar.dismiss();
                                alertdialog.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        final AlertDialog.Builder imageDialog1 = new AlertDialog.Builder(TransactionResultActivity.this, R.style.RajCustomDialog);
                                        final LayoutInflater inflater1 = (LayoutInflater) TransactionResultActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                        final View dialogView1 = inflater1.inflate(R.layout.custom_dialog_success, null);
                                        imageDialog1.setView(dialogView1);
                                        TextView TextviewSmallTitle1 = dialogView1.findViewById(R.id.TextviewSmallTitle1);
                                        TextviewSmallTitle1.setText(response.body().getMessage());
                                        TextView TextviewBigTitle1 = dialogView1.findViewById(R.id.TextviewBigTitle);
                                        TextviewBigTitle1.setText("Awesome");
                                        Button ButtonOk1 = dialogView1.findViewById(R.id.ButtonOk);
                                        ButtonOk1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                Intent intent1 = new Intent(TransactionResultActivity.this,UserNavigationActivity.class);
                                                startActivity(intent1);
                                            }
                                        });
                                        dialog = imageDialog1.create();
                                        dialog.show();


                                        break;
                                    case "201":

                                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(TransactionResultActivity.this, R.style.RajCustomDialog);
                                        final LayoutInflater inflater = (LayoutInflater) TransactionResultActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                        imageDialog.setView(dialogView);
                                        TextView TextviewSmallTitle = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                        TextviewSmallTitle.setText(response.body().getMessage());
                                        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                        TextviewBigTitle.setText("Success");
                                        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                        ButtonOk.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                Intent intent1 = new Intent(TransactionResultActivity.this,UserNavigationActivity.class);
                                                startActivity(intent1);
                                            }
                                        });
                                        dialog = imageDialog.create();
                                        dialog.show();

                                        break;

                                    case "108":
                                        Intent intent = new Intent(TransactionResultActivity.this, UserNavigationActivity.class);
                                        startActivity(intent);
                                        break;
                                    default:
                                        myPreference.ShowDialog(TransactionResultActivity.this,"Oops",response.body().getMessage());
                                        break;

                                }


                            }

                            @Override
                            public void onFailure(Call<TransactionSuccessSubmitPojo> call, Throwable t) {
//                                progressBar.dismiss();
                                alertdialog.dismiss();

                                try {
                                    Toast.makeText(TransactionResultActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                }else{
                    myPreference.ShowDialog(TransactionResultActivity.this,"Oops","There is not internet connection");
                }


            }
        });


        ButtonCancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myPreference.isInternetOn()){
                    progressBar = new ProgressDialog(TransactionResultActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("BookingId",BookingId);
                    jsonObject.addProperty("TransactionId","sdfsdf");
                    jsonObject.addProperty("Status","3");
                    API apiService = createServiceHeader(API.class);
                    Call<TransactionSuccessSubmitPojo> call = apiService.TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<TransactionSuccessSubmitPojo>() {
                        @Override
                        public void onResponse(Call<TransactionSuccessSubmitPojo> call, Response<TransactionSuccessSubmitPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":

                                    Intent intent1 = new Intent(TransactionResultActivity.this,UserNavigationActivity.class);
                                    startActivity(intent1);

                                    break;

                                case "108":
                                    Intent intent = new Intent(TransactionResultActivity.this, UserNavigationActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(TransactionResultActivity.this,"Oops",response.body().getMessage());
                                    break;

                            }

                        }

                        @Override
                        public void onFailure(Call<TransactionSuccessSubmitPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(TransactionResultActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else{
                    myPreference.ShowDialog(TransactionResultActivity.this,"Oops","There is not internet connection");
                }

            }
        });

        ButtonFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()){
                    progressBar = new ProgressDialog(TransactionResultActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("BookingId",BookingId);
                    jsonObject.addProperty("TransactionId","sdfsdf");
                    jsonObject.addProperty("Status","2");
                    API apiService = createServiceHeader(API.class);
                    Call<TransactionSuccessSubmitPojo> call = apiService.TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<TransactionSuccessSubmitPojo>() {
                        @Override
                        public void onResponse(Call<TransactionSuccessSubmitPojo> call, Response<TransactionSuccessSubmitPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    Intent intent1 = new Intent(TransactionResultActivity.this,UserNavigationActivity.class);
                                    startActivity(intent1);
                                    break;

                                case "108":
                                    Intent intent = new Intent(TransactionResultActivity.this, UserNavigationActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(TransactionResultActivity.this,"Oops",response.body().getMessage());
                                    break;

                            }


                        }

                        @Override
                        public void onFailure(Call<TransactionSuccessSubmitPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(TransactionResultActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else{
                    myPreference.ShowDialog(TransactionResultActivity.this,"Oops","There is not internet connection");
                }

            }
        });

    }

}
