package com.codegenstudios.purohit.Activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Adapter.NotificationReadListAdapter;
import com.codegenstudios.purohit.NotificationService.MyFirebaseMessagingService;
import com.codegenstudios.purohit.Pojo.NotificaitonReadSubmitPojo;
import com.codegenstudios.purohit.Pojo.NotificationListPojo;
import com.codegenstudios.purohit.Pojo.NotificationListResponsePojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class NotificaitonListActivity extends AppCompatActivity {
    @BindView(R.id.ListView)
    RecyclerView recyclerView;
    @BindView(R.id.MarkallRead)
    Button MarkallRead;
    @BindView(R.id.GIFLinearLayout)
    LinearLayout GIFLinearLayout;
    MyPreference myPreference;
    private List<NotificationListResponsePojo> notificationListResponsePojos;
    NotificationReadListAdapter notificationReadListAdapter;
    private ProgressDialog progressBar;
    LinearLayoutManager linearLayoutManager;
    String MY_PREFS_NAME = "MyPrefsFile";
    int Page_Number =1;
    int Item_Count =10;
    private boolean isLoading = true;
    private  int  PastVisable,VisiableItemCount,TotalItemCount,PreviousTotal = 0;
    int ViewTherholds = 0;
    int ResponseTotalPage ;
    JsonArray array;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaiton_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Notificaitons");
        myPreference = new MyPreference(this);
        ButterKnife.bind(this);
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        MarkallRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationListResponsePojos = new ArrayList<>();

                if (myPreference.isInternetOn()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("SenderType","2");
                    jsonObject.addProperty("ReadNotification","1");
                    array = new JsonArray();
                    for(int i =0 ; i<notificationListResponsePojos.size(); i++){
                        if(notificationListResponsePojos.get(i).getReadNotification().equals("0")){
                            array.add( notificationListResponsePojos.get(i).getNotificationID());
                        }
                    }
                    jsonObject.add("NotificaitonIdArray", array);
                    API apiService = createServiceHeader(API.class);
                    Call<NotificaitonReadSubmitPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<NotificaitonReadSubmitPojo>() {
                        @Override
                        public void onResponse(Call<NotificaitonReadSubmitPojo> call, Response<NotificaitonReadSubmitPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    ServerCallAPI();
                                    SharedPreferences myPrefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);  //Activity1.class
                                    Integer NotificationCount = myPrefs.getInt("NotificationCount",0);

                                    if(NotificationCount > 0){
                                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                        editor.putInt("NotificationCount", NotificationCount - array.size());
                                        editor.apply();
                                        Intent pushNotification = new Intent(MyFirebaseMessagingService.NOTIFICATION_COUNT);
                                        pushNotification.putExtra("NotificationCount",NotificationCount );
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                    }

                                    break;
                                case "108":
                                    Intent intent = new Intent(NotificaitonListActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                default:
                                    myPreference.ShowDialog(NotificaitonListActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<NotificaitonReadSubmitPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(NotificaitonListActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }else {
                    myPreference.ShowDialog(NotificaitonListActivity.this,"Oops","There is no internet connection");
                }

            }
        });


    }




    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

     ServerCallAPI();
    }

    private void ServerCallAPI() {
        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(NotificaitonListActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("SenderType","2");
            jsonObject.addProperty("per_page",Item_Count);
            jsonObject.addProperty("page",Page_Number);

            API apiService = createServiceHeader(API.class);
            Call<NotificationListPojo> call = apiService.NOTIFICATION_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<NotificationListPojo>() {
                @SuppressLint("NewApi")
                @Override
                public void onResponse(Call<NotificationListPojo> call, Response<NotificationListPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            notificationListResponsePojos = new ArrayList<>();

                            Collections.addAll(notificationListResponsePojos,response.body().getResponse());
//
                            if(notificationListResponsePojos.size() > 0){
                                ResponseTotalPage = Integer.parseInt(response.body().getTotalPage());
                                notificationReadListAdapter= new NotificationReadListAdapter(NotificaitonListActivity.this, notificationListResponsePojos, new NotificationReadListAdapter.RefreshData() {
                                    @Override
                                    public void refreshData() {
                                        ServerCallAPI();
                                        Log.e("Refeshcall","cal");
                                    }
                                });
                                linearLayoutManager = new LinearLayoutManager(NotificaitonListActivity.this, LinearLayoutManager.VERTICAL, false);
                                recyclerView.setLayoutManager(linearLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter( notificationReadListAdapter);

                                recyclerView.setVisibility(View.VISIBLE);
                                GIFLinearLayout.setVisibility(View.GONE);
                            }else {
                                recyclerView.setVisibility(View.GONE);
                                GIFLinearLayout.setVisibility(View.VISIBLE);
                            }

                            for(int i = 0 ; i <notificationListResponsePojos.size(); i++){
                                if(notificationListResponsePojos.get(i).getReadNotification().equals("0") ){
                                    MarkallRead.setVisibility(View.VISIBLE);
                                    break;
                                    }else{
                                        MarkallRead.setVisibility(View.GONE);
                                    }

                                }
                            break;
                        case "108":
                            Intent intent = new Intent(NotificaitonListActivity.this, LoginActivity.class);
                            startActivity(intent);
                        default:
                            myPreference.ShowDialog(NotificaitonListActivity.this,"Oops",response.body().getMessage());
                            break;
                   }
                }

                @Override
                public void onFailure(Call<NotificationListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        Toast.makeText(NotificaitonListActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    VisiableItemCount = layoutManager.getChildCount();
                    Log.i("VisiableItemCount", String.valueOf(VisiableItemCount));
                    TotalItemCount = layoutManager.getItemCount();
                    Log.i("TotalItemCount", String.valueOf(TotalItemCount));
                    PastVisable = layoutManager.findFirstVisibleItemPosition();
                    if(dy > 0 ){
                        if(isLoading){
                            if(TotalItemCount >= PreviousTotal){
                                isLoading =false;
                                PreviousTotal = TotalItemCount;
                            }
                        }else
                        if(!isLoading && (TotalItemCount - VisiableItemCount) <= (PastVisable + ViewTherholds) ){
                            Page_Number++;
                            Log.i("Page_Number ", String.valueOf(Page_Number));
                            PerformingPagination();
                            isLoading = true;
                        }
                    }
                }
            });
        }else {

            myPreference.ShowDialog(NotificaitonListActivity.this,"Oops","There is no internet connection");

        }
    }

    private void PerformingPagination() {
        if(Page_Number <= ResponseTotalPage){
            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(NotificaitonListActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("SenderType","2");
                jsonObject.addProperty("per_page",Item_Count);
                jsonObject.addProperty("page",Page_Number);

                API apiService = createServiceHeader(API.class);
                Call<NotificationListPojo> call = apiService.NOTIFICATION_LIST_POJO_CALL(jsonObject);
                call.enqueue(new Callback<NotificationListPojo>() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(Call<NotificationListPojo> call, Response<NotificationListPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":

                                if(response.body().getResponse().length>0){
                                    List<NotificationListResponsePojo> listResponsePojos = Arrays.asList(response.body().getResponse());
                                        notificationReadListAdapter.adddate(listResponsePojos);
                                        for(int i = 0 ; i <notificationListResponsePojos.size(); i++){
                                            if(notificationListResponsePojos.get(i).getReadNotification().equals("0") ){
                                                MarkallRead.setVisibility(View.VISIBLE);
                                                break;
                                            }else{
                                                recyclerView.setVisibility(View.VISIBLE);
                                                GIFLinearLayout.setVisibility(View.GONE);
                                                MarkallRead.setVisibility(View.GONE);
                                            }
                                        }
                                }else{
                                    myPreference.ShowDialog(NotificaitonListActivity.this,"Message","No more data");
                                }
//                                if(response.body().getMessage().equals("Notification List")){



//                                }else{
//                                    Toast.makeText(NotificaitonListActivity.this, "no data found", Toast.LENGTH_SHORT).show();
//                                }
                                break;
                            case "108":
                                Intent intent = new Intent(NotificaitonListActivity.this, LoginActivity.class);
                                startActivity(intent);
                            default:
                                myPreference.ShowDialog(NotificaitonListActivity.this,"Oops",response.body().getMessage());
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationListPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            Toast.makeText(NotificaitonListActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }else {
                myPreference.ShowDialog(NotificaitonListActivity.this,"Oops","There is no internet connection");
            }
        }else{
            myPreference.ShowDialog(NotificaitonListActivity.this,"Message","No more data");
        }
    }

}
