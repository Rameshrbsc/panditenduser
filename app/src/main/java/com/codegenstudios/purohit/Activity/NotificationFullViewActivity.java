package com.codegenstudios.purohit.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.NotificationService.MyFirebaseMessagingService;
import com.codegenstudios.purohit.Pojo.NotificaitonReadSubmitPojo;
import com.codegenstudios.purohit.Pojo.NotificationSingleBookingIdPojo;
import com.codegenstudios.purohit.Pojo.NotificationSingleBookingIdResponsePojo;
import com.codegenstudios.purohit.Pojo.TrackingPurohitLatLongPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Activity.UserProfileEditActivity.getclip;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class NotificationFullViewActivity extends AppCompatActivity {

    @BindView(R.id.ImageViewPoojaImage)
    ImageView ImageViewPoojaImage;
    @BindView(R.id.TextViewPoojaName)
    TextView TextViewPoojaName;
    //    @BindView(R.id.TextViewUserName)
//    TextView TextViewUserName;
//    @BindView(R.id.TextViewUserEmailID)
//    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewMoblieNumber)
    TextView TextViewMoblieNumber;
    @BindView(R.id.track)
    FloatingActionButton tracks;
    @BindView(R.id.TextViewLanguage)
    TextView TextViewLanguage;
    @BindView(R.id.TextViewCatogeryName)
    TextView TextViewCatogeryName;
    @BindView(R.id.TextViewSubCatogeryName)
    TextView TextViewSubCatogeryName;
    @BindView(R.id.TextViewBookingDate)
    TextView TextViewBookingDate;
    @BindView(R.id.TextViewTime)
    TextView TextViewTime;
    @BindView(R.id.TextViewAmount)
    TextView TextViewAmount;
    @BindView(R.id.TextViewStatus)
    TextView TextViewStatus;
    @BindView(R.id.TextViewMoblieLabel)
    TextView TextViewMoblieLabel;
    @BindView(R.id.fabTrackmeicon)
    FloatingActionButton fabTrackmeicon;
    MyPreference myPreference;
    private ProgressDialog progressBar;
    String BookingId, NotificaitonBookingId, JsonRegardingIdBookingId, NotificaitonId, JsonNotificationId, PurohitLat, PurohitLong;
    NotificationSingleBookingIdResponsePojo[] notificationListResponsePojos;
    Bundle bundle;
    String[] stringarray = {"1", "2"};
    String MY_PREFS_NAME = "MyPrefsFile";
    List<String> stringList = new ArrayList<String>(Arrays.asList(stringarray));
    String status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_full_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Booked Details");
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
        bundle = getIntent().getExtras();
        assert bundle != null;
        status = bundle.getString("status");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bundle.getString("JsonRegardingId") != null) {
                    Intent intent = new Intent(NotificationFullViewActivity.this, UserNavigationActivity.class);
                    startActivity(intent);
                    if (bundle != null) {
                        bundle.clear();
                        // i also use the below statement
                        // bundle.remove("KEY");
                    }
                } else if (bundle.getString("IntentPassing").equals("BookedCardAdapter")) {
                    Intent intentnav = new Intent(NotificationFullViewActivity.this, UserNavigationActivity.class);
                    intentnav.putExtra("IntentPassing", "MybookingFragment");
                    startActivity(intentnav);
                } else {
                    onBackPressed();
                    if (bundle != null) {
                        bundle.clear();
                    }
                }
            }
        });
        if (bundle.getString("IntentPassing").equals("FcmIntentPassing")) {

            JsonNotificationId = bundle.getString("JsonNotificationId");


        }
        if (status != null)
            if (status.equals("cancel")) {
                tracks.setVisibility(View.GONE);

            }
        tracks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NotificationFullViewActivity.this, TrackStatusActivity.class);
                intent.putExtra("status", status);
                startActivity(intent);


                // startActivity(new Intent(NotificationFullViewActivity.this, TrackStatusActivity.class));
            }
        });
        TextViewPoojaName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NotificationFullViewActivity.this, Feedback_Activity.class));
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        if (bundle.getString("JsonRegardingId") != null) {
            Intent intent = new Intent(NotificationFullViewActivity.this, UserNavigationActivity.class);
            startActivity(intent);
            if (bundle != null) {
                bundle.clear();
                // i also use the below statement
                // bundle.remove("KEY");
            }

        } else {
            onBackPressed();
            if (bundle != null) {
                bundle.clear();
                // i also use the below statement
                // bundle.remove("KEY");
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //fullview display

        if (myPreference.isInternetOn()) {

            progressBar = new ProgressDialog(NotificationFullViewActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            if (bundle.getString("IntentPassing").equals("NotificaitonReadListAdapter")) {
                NotificaitonBookingId = bundle.getString("NotificaitonBookingId");
                jsonObject.addProperty("BookingId", NotificaitonBookingId);
            } else if (bundle.getString("JsonRegardingId") != null) {
                JsonRegardingIdBookingId = bundle.getString("JsonRegardingId");
                jsonObject.addProperty("BookingId", JsonRegardingIdBookingId);

            } else if (bundle.getString("IntentPassing").equals("BookedCardAdapter")) {
                BookingId = bundle.getString("BookingId");
                jsonObject.addProperty("BookingId", BookingId);

            } else if (bundle.getString("IntentPassing").equals("MapTracking")) {
                BookingId = bundle.getString("BookingId");
                jsonObject.addProperty("BookingId", BookingId);
            }
            jsonObject.addProperty("SenderType", "2");
            API apiService = createServiceHeader(API.class);
            Call<NotificationSingleBookingIdPojo> call = apiService.NOTIFICATION_SINGLE_BOOKING_ID_POJO_CALL(jsonObject);
            call.enqueue(new Callback<NotificationSingleBookingIdPojo>() {
                @Override
                public void onResponse(Call<NotificationSingleBookingIdPojo> call, Response<NotificationSingleBookingIdPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":

                            notificationListResponsePojos = response.body().getBookingDetails();
                            if (notificationListResponsePojos[0].getPurohitImage().equals("")) {
                                Drawable myDrawable = getResources().getDrawable(R.mipmap.ic_launcher);
                                ImageViewPoojaImage.setImageDrawable(myDrawable);
                            } else {
                                Picasso.with(NotificationFullViewActivity.this).load(notificationListResponsePojos[0].getPurohitImage()).transform(new UserProfileViewFragment.CircleTransform()).into(ImageViewPoojaImage);
                            }

                            TextViewPoojaName.setText(notificationListResponsePojos[0].getPurohitName());
                            if (notificationListResponsePojos[0].getPurohitPhoneNumber().equals("")) {
                                TextViewMoblieLabel.setVisibility(View.GONE);
                                TextViewPoojaName.setVisibility(View.GONE);
                            } else {
                                TextViewMoblieNumber.setText(notificationListResponsePojos[0].getPurohitPhoneNumber());
                                TextViewMoblieLabel.setVisibility(View.VISIBLE);
                                TextViewPoojaName.setVisibility(View.VISIBLE);
                            }

                            TextViewLanguage.setText(notificationListResponsePojos[0].getLanguage());
                            TextViewCatogeryName.setText(notificationListResponsePojos[0].getCatogeryName());
                            TextViewSubCatogeryName.setText(notificationListResponsePojos[0].getSubCatogeryName());
                            TextViewBookingDate.setText(notificationListResponsePojos[0].getBookingDate());
                            TextViewTime.setText(notificationListResponsePojos[0].getTime());


                            TextViewAmount.setText("Rs." + notificationListResponsePojos[0].getAmount());
                            //        0 - Pending   1 - Confirmed   2 - Rejected     3 - Cancelled    4 - Completed
                            if (notificationListResponsePojos[0].getStatus().equals("0")) {
                                TextViewStatus.setText("Transaction Confirmed. Waiting for Purohit To accept ");
                                TextViewStatus.setTextColor((Color.parseColor("#E65100")));
                            } else if (notificationListResponsePojos[0].getStatus().equals("1")) {
                                if (notificationListResponsePojos[0].getTrackMe().equals("1")) {
                                    fabTrackmeicon.setVisibility(View.VISIBLE);
                                    fabTrackmeicon.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (myPreference.isInternetOn()) {
                                                progressBar = new ProgressDialog(NotificationFullViewActivity.this);
                                                progressBar.setCancelable(true);
                                                progressBar.setMessage("Processing...");
                                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                progressBar.setProgress(0);
                                                progressBar.setMax(100);
                                                progressBar.show();
                                                JsonObject jsonObject = new JsonObject();
                                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                                jsonObject.addProperty("BookingId", notificationListResponsePojos[0].getBookingId());

                                                API apiService = createServiceHeader(API.class);
                                                Call<TrackingPurohitLatLongPojo> call = apiService.TRACKING_PUROHIT_LAT_LONG_POJO_CALL(jsonObject);
                                                call.enqueue(new Callback<TrackingPurohitLatLongPojo>() {
                                                    @Override
                                                    public void onResponse(Call<TrackingPurohitLatLongPojo> call, Response<TrackingPurohitLatLongPojo> response) {
                                                        progressBar.dismiss();
                                                        switch (response.body().getCode()) {
                                                            case "200":
                                                                PurohitLat = response.body().getLatitude();
                                                                PurohitLong = response.body().getLongitude();
                                                                Intent intent = new Intent(NotificationFullViewActivity.this, MapTrackingPurohitActivity.class);
                                                                intent.putExtra("IntentPassing", "NotificationFullViewActivity");
                                                                intent.putExtra("BookingId", notificationListResponsePojos[0].getBookingId());
                                                                intent.putExtra("UserLat", notificationListResponsePojos[0].getLatitude());
                                                                intent.putExtra("UserLong", notificationListResponsePojos[0].getLongitude());
                                                                intent.putExtra("PurohitLat", PurohitLat);
                                                                intent.putExtra("PurohitLong", PurohitLong);
                                                                intent.putExtra("PurohitName", notificationListResponsePojos[0].getPurohitName());
                                                                startActivity(intent);
                                                                break;
                                                            case "108":
                                                                Intent intent1 = new Intent(NotificationFullViewActivity.this, LoginActivity.class);
                                                                startActivity(intent1);
                                                                break;
                                                            default:
                                                                myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", response.body().getMessage());
                                                                break;
                                                        }

                                                    }

                                                    @Override
                                                    public void onFailure(Call<TrackingPurohitLatLongPojo> call, Throwable t) {
                                                        progressBar.dismiss();
                                                        try {
                                                            Toast.makeText(NotificationFullViewActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                                                        } catch (NullPointerException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });

                                            } else {
                                                myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", "There is no internet connection");
                                            }
                                        }
                                    });
                                }
                                TextViewStatus.setText("Confirmed");
                                TextViewStatus.setTextColor((Color.parseColor("#558B2F")));
                            } else if (notificationListResponsePojos[0].getStatus().equals("2")) {
                                TextViewStatus.setText("Rejected");
                                TextViewStatus.setTextColor((Color.parseColor("#D84315")));
                            } else if (notificationListResponsePojos[0].getStatus().equals("3")) {
                                TextViewStatus.setText("Cancelled");
                                TextViewStatus.setTextColor(Color.parseColor("#D84315"));
                            } else if (notificationListResponsePojos[0].getStatus().equals("4")) {
                                TextViewStatus.setText("Completed");
                                TextViewStatus.setTextColor((Color.parseColor("#558B2F")));
                            }
                            break;
                        case "108":
                            Intent intent = new Intent(NotificationFullViewActivity.this, LoginActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", response.body().getMessage());
                            break;
                    }
                }

                @Override
                public void onFailure(Call<NotificationSingleBookingIdPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        Toast.makeText(NotificationFullViewActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", "There is no internet connection");
        }

        //this is for read notification
        if (bundle.getString("IntentPassing").equals("NotificaitonReadListAdapter") || bundle.getString("IntentPassing").equals("FcmIntentPassing")) {

            if (myPreference.isInternetOn()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("SenderType", "2");
                jsonObject.addProperty("ReadNotification", "1");
                if (bundle.getString("IntentPassing").equals("NotificaitonReadListAdapter")) {
                    NotificaitonId = bundle.getString("NotificaitonId");
                    JsonArray array = new JsonArray();
                    array.add(NotificaitonId);
                    jsonObject.add("NotificaitonIdArray", array);

                } else if (JsonNotificationId != null) {

                    JsonArray array = new JsonArray();
                    array.add(JsonNotificationId);
                    jsonObject.add("NotificaitonIdArray", array);
                }


                API apiService = createServiceHeader(API.class);
                Call<NotificaitonReadSubmitPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                call.enqueue(new Callback<NotificaitonReadSubmitPojo>() {
                    @Override
                    public void onResponse(Call<NotificaitonReadSubmitPojo> call, Response<NotificaitonReadSubmitPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":

                                SharedPreferences myPrefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);  //Activity1.class
                                Integer NotificationCount = myPrefs.getInt("NotificationCount", 0);
                                if (NotificationCount > 0) {
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putInt("NotificationCount", NotificationCount - 1);
                                    editor.apply();
                                    Intent pushNotification = new Intent(MyFirebaseMessagingService.NOTIFICATION_COUNT);
                                    pushNotification.putExtra("NotificationCount", NotificationCount);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                }

                                break;
                            case "108":
                                Intent intent = new Intent(NotificationFullViewActivity.this, LoginActivity.class);
                                startActivity(intent);
                            default:
                                myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", response.body().getMessage());
                                break;
                        }

                    }

                    @Override
                    public void onFailure(Call<NotificaitonReadSubmitPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            Toast.makeText(NotificationFullViewActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

            } else {
                myPreference.ShowDialog(NotificationFullViewActivity.this, "Oops", "There is no internet connection");
            }
        } else {
            Log.i("no come", "no come");
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if(bundle.getString("JsonRegardingId")!= null){

        if (bundle.getString("IntentPassing").equals("BookedCardAdapter")) {
            Intent intentnav = new Intent(NotificationFullViewActivity.this, UserNavigationActivity.class);
            intentnav.putExtra("IntentPassing", "MybookingFragment");
            startActivity(intentnav);
        } else {
            Intent intent = new Intent(NotificationFullViewActivity.this, UserNavigationActivity.class);
            startActivity(intent);
        }

//            if(bundle!=null) {
//                bundle.clear();
//            }
//        }else{
//            onBackPressed();
//
//
//    }
    }
}
