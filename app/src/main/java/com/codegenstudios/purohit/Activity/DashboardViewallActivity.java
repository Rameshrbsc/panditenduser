package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Adapter.DashboardSubGridViewAdapter;
import com.codegenstudios.purohit.Adapter.DashboardViewAllAdapter;
import com.codegenstudios.purohit.Pojo.DashboardPojo;
import com.codegenstudios.purohit.Pojo.SubCatogeryViewAllPojo;
import com.codegenstudios.purohit.Pojo.SubCatogeryViewAllResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class DashboardViewallActivity extends AppCompatActivity {
    @BindView(R.id.GridViewLayout)
    GridView GridViewLayout;
    DashboardViewAllAdapter dashboardViewAllAdapter;
    String[] testing = {"sdfsd","sdf","sdfsdf","sdf","sdfa"};
    String CatogeryId, CatogeryName;
    MyPreference myPreference;
    private ProgressDialog progressBar;
    private List<SubCatogeryViewAllResponsePojo>  subCatogeryViewAllResponsePojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_viewall);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        CatogeryId = intent.getExtras().get("CatogeryId").toString();
        CatogeryName = intent.getExtras().get("CatogeryName").toString();
        setTitle(CatogeryName);
        myPreference = new MyPreference(this);

        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("CatogeryId", CatogeryId);
            API apiService = createServiceHeader(API.class);
            Call<SubCatogeryViewAllPojo> call = apiService.SUB_CATOGERY_VIEW_ALL_POJO_CALL(jsonObject);
            call.enqueue(new Callback<SubCatogeryViewAllPojo>() {
                @Override
                public void onResponse(Call<SubCatogeryViewAllPojo> call, Response<SubCatogeryViewAllPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            Log.i("", "" + response.body().getMessage());
                            subCatogeryViewAllResponsePojos = new ArrayList<>();
                            Collections.addAll(subCatogeryViewAllResponsePojos, response.body().getResponse());
                            dashboardViewAllAdapter = new DashboardViewAllAdapter(DashboardViewallActivity.this,subCatogeryViewAllResponsePojos,CatogeryId,CatogeryName);
                            GridViewLayout.setSmoothScrollbarEnabled(true);

                            GridViewLayout.setAdapter(dashboardViewAllAdapter);
                            break;
                        case "300":
                            break;
                        case "108":
                            Intent intent = new Intent(DashboardViewallActivity.this, LoginActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            myPreference.ShowDialog(DashboardViewallActivity.this,"Oops",response.body().getMessage());
                            break;
                    }
                }

                @Override
                public void onFailure(Call<SubCatogeryViewAllPojo> call, Throwable t) {
                    progressBar.dismiss();
                    Toast.makeText(DashboardViewallActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            myPreference.ShowDialog(DashboardViewallActivity.this,"Oops","There is not internet connection");
        }




    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
