package com.codegenstudios.purohit.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.GeocodingLocationFromAddress;
import com.codegenstudios.purohit.SupportClass.GpsTracker;
import com.codegenstudios.purohit.SupportClass.LocationAddress;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapSeacrchingActivity extends AppCompatActivity {
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.TextSalesPersonName)
    TextView TextViewAddressName;
    @BindView(R.id.EditTextAddress)
    TextView EditViewAddressName;
    @BindView(R.id.LocationLayout)
    LinearLayout LocationLayout;
    BottomSheetBehavior sheetBehavior;

    double longitude, latitude;
    private GpsTracker gpsTracker;
    String MY_PREFS_NAME = "MyPrefsFile",locationAddress;
    Marker marker;
    StringBuilder result;
    String Name,Email, MoblieNumber, Address,Language,PoojaName,CategoryName,SubCategoryName, Amount,PoojaID,CatId,SubId,IntentPassing;
    Bundle bundle;
    PlaceAutocompleteFragment placeAutoComplete;
    private MapView mapView;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private boolean MapClick = false;
    LatLng latlong;
    GoogleMap mapbox;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_seacrching);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        toolbar.setSubtitle("MapView");
        getLocation();

        Toast.makeText(this, "Pin your location", Toast.LENGTH_SHORT).show();
        bundle = getIntent().getExtras();
        IntentPassing =  bundle.getString("IntentPassing");
        if(IntentPassing.equals("LocationSearchingActivity")){
            PoojaID = bundle.getString("PoojaID");
            PoojaName =bundle.getString("PoojaName");
            Amount = bundle.getString("Amount");
            CatId = bundle.getString("CatId");
            CategoryName = bundle.getString("CatName");
            SubId = bundle.getString("SubId");
            SubCategoryName = bundle.getString("SubName");
            Name = bundle.getString("Name");
            Email = bundle.getString("Email");
            MoblieNumber = bundle.getString("MoblieNumber");
            Language = bundle.getString("Language");

        }

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mapView = findViewById(R.id.map_view);
        mapView.onCreate(mapViewBundle);
        sheetBehavior = BottomSheetBehavior.from(LocationLayout);

        placeAutoComplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);
        /*placeAutoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.e("LatLong", "Place selected: " + place.getLatLng());
                Log.e("Name", "Place selected: " + place.getName());
                Log.e("URI", "Place selected: " + place.getWebsiteUri());
                Log.e("Address", "Place selected: " + place.getAddress());
                latlong = place.getLatLng();

                mapmarker(latlong);
            }

            @Override
            public void onError(Status status) {
                Log.i("MapError",status.getStatus().toString());

            }
        });*/

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();
        placeAutoComplete.setFilter(typeFilter);

        placeAutoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                Log.e("LatLong", "Place selected: " + place.getLatLng());
                Log.e("Name", "Place selected: " + place.getName());
                Log.e("URI", "Place selected: " + place.getWebsiteUri());
                Log.e("Address", "Place selected: " + place.getAddress());
                latlong = place.getLatLng();

                mapmarker(latlong);
                //Log.i(TAG, "Place: " + place.getName());//get place details here
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                //Log.i(TAG, "An error occurred: " + status);
            }
        });

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapSeacrchingActivity.this,LocationSearchingActivity.class);
                intent.putExtra("IntentPassing","MapSearchingActivity");
                intent.putExtra("LocationResult",EditViewAddressName.getText().toString());
                intent.putExtra("Lat",String.valueOf(latitude));
                intent.putExtra("Long",String.valueOf(longitude));
                intent.putExtra("Name",Name);
                intent.putExtra("Email",Email);
                intent.putExtra("MoblieNumber",MoblieNumber);
                intent.putExtra("Language",Language);
                intent.putExtra("PoojaName",PoojaName);
                intent.putExtra("PoojaId",PoojaID);
                intent.putExtra("CatName",CategoryName);
                intent.putExtra("CatId",CatId);
                intent.putExtra("SubName",SubCategoryName);
                intent.putExtra("SubId",SubId);
                intent.putExtra("Amount",Amount);
                startActivity(intent);
                MapSeacrchingActivity.this.finish();
            }
        });

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap mapboxMap) {
                mapbox = mapboxMap;
               LatLng SomePos = new LatLng(latitude,longitude);
                try {
                    mapbox.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    if (ActivityCompat.checkSelfPermission(MapSeacrchingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapSeacrchingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mapbox.setMyLocationEnabled(true);
                    mapbox.setTrafficEnabled(false);
                    mapbox.setIndoorEnabled(false);
                    mapbox.setBuildingsEnabled(true);
                    mapbox.getUiSettings().setZoomControlsEnabled(true);
                    mapbox.animateCamera(CameraUpdateFactory.newLatLngZoom(SomePos,12.0f));

                }catch (Exception e){

                }

                mapbox.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                   @Override
                   public void onMapClick(LatLng point) {
                       sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                       if (MapClick) {
                           mapbox.clear();
                       }
                       MapClick = true;
                       computeCentroid(Collections.singletonList(point));
                       mapbox.addMarker(new MarkerOptions().position(point));
                       Log.e("lat", "" + point);
                       LocationLayout.setVisibility(View.VISIBLE);
                       EditViewAddressName.setText(locationAddress);

//                       sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//                           @Override
//                           public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                               switch (newState) {
//                                   case BottomSheetBehavior.STATE_HIDDEN:
//                                       break;
//                                   case BottomSheetBehavior.STATE_EXPANDED: {
//
//
//                                   }
//                                   break;
//                                   case BottomSheetBehavior.STATE_COLLAPSED: {
//                                       LocationLayout.setVisibility(View.VISIBLE);
//                                       EditViewAddressName.setText(locationAddress);
//                                   }
//                                   break;
//                                   case BottomSheetBehavior.STATE_DRAGGING:
//                                       break;
//                                   case BottomSheetBehavior.STATE_SETTLING:
//                                       break;
//                               }
//                           }
//
//                           @Override
//                           public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//                           }
//                       });





                   }
               });

            }
        });




}

    private void mapmarker(LatLng latlong) {
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        if (MapClick) {
            mapbox.clear();
        }
        MapClick = true;
        mapbox.getUiSettings().setZoomControlsEnabled(true);
        mapbox.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong,12.0f));

        computeCentroid(Collections.singletonList(latlong));
        mapbox.addMarker(new MarkerOptions().position(latlong));

        Log.e("lat", "" + latlong);
//
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {

                        LocationLayout.setVisibility(View.VISIBLE);
                        EditViewAddressName.setText(locationAddress);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        LocationLayout.setVisibility(View.VISIBLE);
                        EditViewAddressName.setText(locationAddress);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                bottomSheet.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        int action = MotionEventCompat.getActionMasked(event);
                        switch (action) {
                            case MotionEvent.ACTION_DOWN:
                                return false;
                            default:
                                return true;
                        }
                    }
                });
            }
        });

    }

    private LatLng computeCentroid(List<LatLng> points) {
        latitude = 0;
         longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.latitude;
            longitude += point.longitude;
        }
        Log.i("centerPointlat",""+latitude+""+longitude);
        getAddress(latitude,longitude);

        return new LatLng(latitude/n, longitude/n);
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }





    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    public String getAddress(double latitude1, double longitude1) {
         result = new StringBuilder();

        try {

            System.out.println("get address");
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude1, longitude1, 1);
            if (addresses.size() > 0) {
                System.out.println("size====" + addresses.size());
                Address address = addresses.get(0);

                for (int i = 0; i <= addresses.get(0).getMaxAddressLineIndex(); i++) {
                    if (i == addresses.get(0).getMaxAddressLineIndex()) {
                        result.append(addresses.get(0).getAddressLine(i));
                    } else {
                        result.append(addresses.get(0).getAddressLine(i) + ",");
                    }
                }
                System.out.println("ad==" + address);
                System.out.println("result---" + result.toString());

                locationAddress = result.toString();
                latitude = latitude1;
                longitude = longitude1;
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    public void getLocation() {
        gpsTracker = new GpsTracker(MapSeacrchingActivity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            Log.i("Lat and Log", "" + String.valueOf(latitude) + " " + String.valueOf(longitude));
//            Toast.makeText(this, "LocationPoint "+String.valueOf(latitude)+" "+ String.valueOf(longitude), Toast.LENGTH_SHORT).show();

        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
