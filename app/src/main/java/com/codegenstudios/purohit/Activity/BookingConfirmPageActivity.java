package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Fragment.BookingConfirmedFragment;
import com.codegenstudios.purohit.Pojo.BookDetails;
import com.codegenstudios.purohit.Pojo.BookingFormSubmitPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class BookingConfirmPageActivity extends AppCompatActivity {
    String MY_PREFS_NAME = "MyPrefsFile", UserPicURI;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String BookingName, BookingEmailID, BookingMoblieNumber, BookingAmount, BookingLanguage, BookingAddress, Bookinglat, Bookinglong, BookingPoojaId, BookingPoojaName, BookingCatogeryId, BookingCatogeryName, BookingSubCatogeryId, BookingSubCatogeryName, BookingTime, BookingDate, IntentPassing;

    @BindView(R.id.TextViewBookingName)
    TextView TextViewBookingName;
    @BindView(R.id.TextViewBookingEmailId)
    TextView TextViewBookingEmailId;
    @BindView(R.id.TextViewBookingMoblieNumber)
    TextView TextViewBookingMoblieNumber;
    @BindView(R.id.TextViewBookingAmount)
    TextView TextViewBookingAmount;
    @BindView(R.id.TextViewBookingLanguage)
    TextView TextViewBookingLanguage;
    @BindView(R.id.TextViewBookingAddress)
    TextView TextViewBookingAddress;
    @BindView(R.id.TextViewBookingPoojaName)
    TextView TextViewBookingPoojaName;
    @BindView(R.id.TextViewBookingCatogeryName)
    TextView TextViewBookingCatogeryName;
    @BindView(R.id.TextViewBookingSubCatogeryName)
    TextView TextViewBookingSubCatogeryName;
    @BindView(R.id.TextViewBookingDate)
    TextView TextViewBookingDate;
    @BindView(R.id.TextViewBookingTime)
    TextView TextViewBookingTime;
    @BindView(R.id.booking_others)
    TextView booking_others;

    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirm_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);

        setTitle("Booking Confirmation");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        final SharedPreferences myperf=this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("IntentPassing")) {
            IntentPassing = bundle.getString("IntentPassing");
            if (IntentPassing.equals("BookingFormActivity")) {
                BookingName = bundle.getString("BookingName");
                BookingEmailID = bundle.getString("BookingEmailID");


                BookingMoblieNumber = bundle.getString("BookingMoblieNumber");
                BookingAmount = bundle.getString("BookingAmount");
                BookingLanguage = bundle.getString("BookingLanguage");
                BookingAddress = bundle.getString("BookingAddress");
                Bookinglat = bundle.getString("Bookinglat");
                Bookinglong = bundle.getString("Bookinglong");
                BookingPoojaId = bundle.getString("BookingPoojaId");
                BookingPoojaName = bundle.getString("BookingPoojaName");
                BookingCatogeryId = bundle.getString("BookingCatogeryId");
                BookingCatogeryName = bundle.getString("BookingCatogeryName");
                BookingSubCatogeryId = bundle.getString("BookingSubCatogeryId");
                BookingSubCatogeryName = bundle.getString("BookingSubCatogeryName");
                BookingDate = bundle.getString("BookingDate");
                BookingTime = bundle.getString("BookingTime");
                BookDetails bookDetails = new BookDetails();

                bookDetails.setBookingName(BookingName);
                bookDetails.setBookingEmailID(BookingName);
                bookDetails.setBookingMoblieNumber(BookingMoblieNumber);
                bookDetails.setBookingAmount(BookingAmount);
                bookDetails.setBookingLanguage(BookingLanguage);
                bookDetails.setBookingAddress(BookingAddress);
                bookDetails.setBookinglat(Bookinglat);
                bookDetails.setBookinglong(Bookinglong);
                bookDetails.setBookingPoojaId(BookingPoojaId);
                bookDetails.setBookingPoojaName(BookingPoojaName);
                bookDetails.setBookingCatogeryId(BookingCatogeryId);
                bookDetails.setBookingCatogeryName(BookingSubCatogeryName);
                bookDetails.setBookingSubCatogeryId(BookingSubCatogeryId);
                bookDetails.setBookingSubCatogeryName(BookingSubCatogeryName);
                bookDetails.setBookingDate(BookingDate);
                bookDetails.setBookingTime(BookingTime);

                TextViewBookingName.setText(BookingName);
                TextViewBookingEmailId.setText(BookingEmailID);
                TextViewBookingMoblieNumber.setText(BookingMoblieNumber);
                TextViewBookingAmount.setText(BookingAmount);
                TextViewBookingLanguage.setText(BookingLanguage);
                TextViewBookingAddress.setText(BookingAddress);
                TextViewBookingPoojaName.setText(BookingPoojaName);
                TextViewBookingCatogeryName.setText(BookingCatogeryName);
                TextViewBookingSubCatogeryName.setText(BookingSubCatogeryName);
                TextViewBookingDate.setText(BookingDate);
                TextViewBookingTime.setText(BookingTime);

                Gson gson = new Gson();
                String json = gson.toJson(bookDetails);
                myPreference.setAllBookingDetails(getApplicationContext(), json);

            }

        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        booking_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intenttransactionpage = new Intent(BookingConfirmPageActivity.this, BookingFormOthersActivity.class);

                startActivity(intenttransactionpage);

            }
        });
        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (myPreference.isInternetOn()) {
                    ButtonSubmit.setVisibility(View.GONE);
                    progressBar = new ProgressDialog(BookingConfirmPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("UserName", TextViewBookingName.getText().toString());
                    jsonObject.addProperty("UserEmailID", TextViewBookingEmailId.getText().toString());
                    jsonObject.addProperty("UserPhoneNumber", TextViewBookingMoblieNumber.getText().toString());
                    jsonObject.addProperty("Latitude", Bookinglat);
                    jsonObject.addProperty("Longitude", Bookinglong);
                    jsonObject.addProperty("Language", TextViewBookingLanguage.getText().toString());
                    jsonObject.addProperty("PoojaId", BookingPoojaId);
                    Log.i("jsonObjectPass", BookingPoojaId);
                    jsonObject.addProperty("CatogeryId", BookingCatogeryId);
                    jsonObject.addProperty("SubCatogeryId", BookingSubCatogeryId);
                    jsonObject.addProperty("Address", TextViewBookingAddress.getText().toString());

                    String conventdate = TextViewBookingDate.getText().toString();
                    SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
                    Date newDate = null;
                    try {
                        newDate = spf.parse(conventdate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    spf = new SimpleDateFormat("yyyy-MM-dd");
                    conventdate = spf.format(newDate);
                    Log.i("", "" + conventdate);


                    jsonObject.addProperty("Date", conventdate);

                    SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                    SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                    Date date = null;
                    try {
                        date = parseFormat.parse(TextViewBookingTime.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.i("time", parseFormat.format(date) + " = " + displayFormat.format(date));


                    jsonObject.addProperty("Time", displayFormat.format(date));
                    jsonObject.addProperty("Amount", TextViewBookingAmount.getText().toString());
//                    jsonObject.addProperty("Rating","");
                    jsonObject.addProperty("Status", "0");
                    API apiService = createServiceHeader(API.class);
                    Call<BookingFormSubmitPojo> call = apiService.BOOKING_FORM_SUBMIT_POJO_CALL(jsonObject);

                    Log.e("Json", jsonObject.toString());


                    call.enqueue(new Callback<BookingFormSubmitPojo>() {
                        @Override
                        public void onResponse(Call<BookingFormSubmitPojo> call, Response<BookingFormSubmitPojo> response) {
                            progressBar.dismiss();
                            ButtonSubmit.setVisibility(View.VISIBLE);
                            switch (response.body().getCode()) {
                                case "200":
                                    Intent intenttransactionpage = new Intent(BookingConfirmPageActivity.this, TransactionProcessingPageActivity.class);
                                    intenttransactionpage.putExtra("BookingId", response.body().getBookingId());
                                    intenttransactionpage.putExtra("BookingAmount", BookingAmount);
                                    Log.e("", "");
                                    startActivity(intenttransactionpage);
                                    break;
                                case "108":
                                    Intent intent = new Intent(BookingConfirmPageActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(BookingConfirmPageActivity.this, "Oops", response.body().getMessage());
                                    break;

                            }
                        }

                        @Override
                        public void onFailure(Call<BookingFormSubmitPojo> call, Throwable t) {

                            progressBar.dismiss();
                            try {
                                Toast.makeText(BookingConfirmPageActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else {
                    myPreference.ShowDialog(BookingConfirmPageActivity.this, "Oops", "There is not internet connection");
                }
            }
        });

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void alertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BookingConfirmPageActivity.this);

        // set title
        String name = getResources().getString(R.string.app_name);
        alertDialogBuilder.setTitle(name);

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want to Book for other ?")
                .setCancelable(false)
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}
