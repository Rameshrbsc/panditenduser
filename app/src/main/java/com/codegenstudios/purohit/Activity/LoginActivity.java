package com.codegenstudios.purohit.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Config.Config;
import com.codegenstudios.purohit.Fragment.BookingConfirmedFragment;
import com.codegenstudios.purohit.Fragment.MyBookingFragment;
import com.codegenstudios.purohit.Pojo.ForgetPasswordPojo;
import com.codegenstudios.purohit.Pojo.LoginPojo;
import com.codegenstudios.purohit.Pojo.SoicalLoginPojo;
import com.codegenstudios.purohit.Pojo.UserSessionAuthPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.Services.Service;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.TextViewSignup)
    TextView TextViewSignup;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.ButtonLogin)
    Button ButtonLogin;
    @BindView(R.id.checkboxRememberPassword)
    CheckBox checkboxRememberPassword;
    @BindView(R.id.FrameLayout1)
    FrameLayout FrameLayout1;
    @BindView(R.id.TextViewForgetpassword)
    TextView TextViewForgetpassword;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";
    String MoblieNumber, Password, Checkbox, FacebookID, Facebookemail, FacebookName, GmailpersonName, GmailEmail, GmailId, IntentPassing, JsonRegarding, JsonRegardingId, JsonNotificationId;
    @BindView(R.id.ImageViewGoogleLoginButton)
    ImageView ImageViewGoogleLoginButton;
    private ProgressDialog progressBar;
    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    //fb
    LoginButton loginButton;
    @BindView(R.id.ImageViewFacebookButton)
    ImageView ImageViewFacebookButton;
    CallbackManager callbackManager;
    Bundle bundle;
    private AlertDialog dialog;
    String regId;
    public static String NOTIFICATION_COUNT = "NotificationCount";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setTheme(R.style.AppTheme);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);*/
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // bind the view using butterknife
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
        // Obtain the FirebaseAnalytics instance.

//
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.codegenstudios.purohit",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("com.example.fishe", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

//
//        if (bundle != null && bundle.containsKey("IntentPassing")) {
//            IntentPassing = bundle.getString("IntentPassing");
//            if(IntentPassing.equals("FcmIntentPassing")){
//                JsonRegarding = bundle.getString("JsonRegarding");
//                if(JsonRegarding.equals("1")){
//                  Intent intent = new Intent(LoginActivity.this,UserNavigationActivity.class);
//                  intent.putExtra("FragmentToLoad","MyBookingFragment");
//                  startActivity(intent);
//
//                }
//
//            }
//        }

        bundle = getIntent().getExtras();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        ImageViewFacebookButton.setOnClickListener(this);


        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.i(LoginActivity.this.getPackageName(), "On Success");
                Log.i(LoginActivity.this.getPackageName(), loginResult.getAccessToken().getToken());
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Facebookemail = object.getString("email");
                            FacebookName = object.getString("name");
                            FacebookID = object.getString("id");
                            Log.i("FacebookID", "" + FacebookID);
                            if (myPreference.isInternetOn()) {
                                progressBar = new ProgressDialog(LoginActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                                jsonObject.addProperty("socialEmail", Facebookemail);
                                jsonObject.addProperty("socialId", FacebookID);
                                jsonObject.addProperty("socialType", 1);
                                API apiService = createServiceHeader(API.class);
                                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                                call.enqueue(new Callback<SoicalLoginPojo>() {
                                    @Override
                                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
//                                                bundle = getIntent().getExtras();

                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                                Log.i("NotificationCount", response.body().getNotificationCount());
                                                editor.apply();
                                                Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                                pushNotification.putExtra("NotificationCount", response.body().getNotificationCount());
                                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                                if (bundle != null && bundle.containsKey("IntentPassing")) {
                                                    IntentPassing = bundle.getString("IntentPassing");
                                                    if (IntentPassing.equals("FcmIntentPassing")) {
                                                        JsonRegarding = bundle.getString("JsonRegarding");
                                                        if (JsonRegarding.equals("1")) {
                                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                                            startActivity(intent);
                                                            SharedPreferences.Editor editorfcm1 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                            editorfcm1.putString("AccessToken", response.body().getAccessToken());
                                                            Log.i("AccessToken", response.body().getAccessToken());
                                                            editorfcm1.putString("DeviceId", displayFirebaseRegId());
                                                            editorfcm1.apply();

                                                        } else if (JsonRegarding.equals("2")) {
                                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                                            startActivity(intent);
                                                            SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                            editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                                            Log.i("AccessToken", response.body().getAccessToken());
                                                            editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                                            editorfcm2.apply();

                                                        } else if (JsonRegarding.equals("")) {
                                                            Intent intent = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                                            SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                            editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                                            Log.i("AccessToken", response.body().getAccessToken());
                                                            editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                                            editorfcm2.apply();
                                                            startActivity(intent);
                                                        }

                                                    }
                                                } else {
                                                    SharedPreferences.Editor editorfcm3 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                    editorfcm3.putString("AccessToken", response.body().getAccessToken());
                                                    Log.i("AccessToken", response.body().getAccessToken());
                                                    editorfcm3.putString("DeviceId", displayFirebaseRegId());
                                                    editorfcm3.apply();
                                                    Intent intentOTP = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                                    startActivity(intentOTP);
                                                    ImageViewFacebookButton.setVisibility(View.GONE);
                                                    ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                }

                                                break;
                                            case "300":
                                                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LoginActivity.this, R.style.RajCustomDialog);
                                                final LayoutInflater inflater = (LayoutInflater) LoginActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                                final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                                imageDialog.setView(dialogView);
                                                TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                TextviewSmallTitle1.setText(response.body().getMessage());
                                                TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                TextviewBigTitle.setText("Success");
                                                Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                        Intent intentSignupPage = new Intent(LoginActivity.this, SignUpActivity.class);
                                                        intentSignupPage.putExtra("EmailID", Facebookemail);
                                                        intentSignupPage.putExtra("EmailIDUnique", FacebookID);
                                                        intentSignupPage.putExtra("SocialType", "1");
                                                        intentSignupPage.putExtra("username", FacebookName);

                                                        startActivity(intentSignupPage);
                                                    }
                                                });
                                                dialog = imageDialog.create();
                                                dialog.show();

                                                ImageViewFacebookButton.setVisibility(View.GONE);
                                                ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                break;

                                            case "108":
//
                                                break;
                                            default:
                                                myPreference.ShowDialog(LoginActivity.this, "Oops", response.body().getMessage());
                                                break;
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        Toast.makeText(LoginActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                                    }
                                });


                            } else {
                                myPreference.ShowDialog(LoginActivity.this, "Oops", "There is not internet connection");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.i(LoginActivity.this.getPackageName(), "On Cancel");

            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                FacebookException facebookException = exception;
                Log.i(LoginActivity.this.getPackageName(), "On Error");
                Log.i(LoginActivity.this.getPackageName(), facebookException.getMessage());
                Log.i(LoginActivity.this.getPackageName(), facebookException.getLocalizedMessage());

            }
        });


        /* String first = "Don't Have an Account? ";*/
        String next = "<font color='#F08400'>SignUp</font>";
        TextViewSignup.setText(Html.fromHtml(/*first + */next));
        TextViewForgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(LoginActivity.this, UserForgetPasswordActivity.class);
                startActivity(intent);

            }
        });
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        MoblieNumber = prefs.getString("MoblieNumber", "");
        Password = prefs.getString("Password", "");
        Checkbox = prefs.getString("CheckBoxTrue", "1");
        if (Checkbox.equals("0")) {
            checkboxRememberPassword.setChecked(true);
            EditTextMoblieNumber.setText(MoblieNumber);
            EditTextPassword.setText(Password);
        } else {
            checkboxRememberPassword.setChecked(false);
            EditTextMoblieNumber.setText("");
            EditTextPassword.setText("");
        }
        TextViewSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, TabFragmentActivity.class);
                startActivity(intent);
            }
        });


        if (myPreference.getDefaultRunTimeValue()[0] != null) {
            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(LoginActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                API apiService = createServiceHeader(API.class);
                Call<UserSessionAuthPojo> call = apiService.USER_SESSION_AUTH_POJO_CALL(jsonObject);
                call.enqueue(new Callback<UserSessionAuthPojo>() {
                    @Override
                    public void onResponse(Call<UserSessionAuthPojo> call, Response<UserSessionAuthPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
//                                bundle = getIntent().getExtras();
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                Log.i("NotificationCount", response.body().getNotificationCount());
                                editor.apply();
                                Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                pushNotification.putExtra("NotificationCount", response.body().getNotificationCount());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                if (bundle != null && bundle.containsKey("IntentPassing")) {
                                    IntentPassing = bundle.getString("IntentPassing");
                                    if (IntentPassing.equals("FcmIntentPassing")) {
                                        JsonRegarding = bundle.getString("JsonRegarding");
                                        if (JsonRegarding.equals("1")) {
                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                            intent.putExtra("IntentPassing", IntentPassing);
                                            startActivity(intent);

                                        } else if (JsonRegarding.equals("2")) {
                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                            intent.putExtra("IntentPassing", IntentPassing);
                                            startActivity(intent);
                                        } else if (JsonRegarding.equals("")) {
                                            Intent intent = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                } else {
                                    Intent intentHome = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                    startActivity(intentHome);
                                }

                                break;
                            case "108":
                                break;
                            default:
                                myPreference.ShowDialog(LoginActivity.this, "Oops", response.body().getMessage());
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<UserSessionAuthPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            Toast.makeText(LoginActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else {
                myPreference.ShowDialog(LoginActivity.this, "Oops", "There is not internet connection");
            }

        } else {
            Log.i("LoginRequired", "LoginRequired");

        }


        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                Checkbox = prefs.getString("CheckBoxTrue", "1");
                if (Checkbox.equals("0")) {
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("MoblieNumber", EditTextMoblieNumber.getText().toString());
                    editor.putString("Password", EditTextPassword.getText().toString());
                    editor.apply();
                }

                if (myPreference.isInternetOn()) {
                    if (Check(EditTextMoblieNumber) && Check(EditTextPassword)) {
                        progressBar = new ProgressDialog(LoginActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("Email", EditTextMoblieNumber.getText().toString());
                        jsonObject.addProperty("Password", EditTextPassword.getText().toString());
                        jsonObject.addProperty("socialEmail", "");
                        jsonObject.addProperty("socialId", "");
                        jsonObject.addProperty("socialType", "");
                        Log.i("LoginJsin", jsonObject.toString());
                        API apiService = createServiceHeader(API.class);
                        Call<LoginPojo> call = apiService.LOGIN_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<LoginPojo>() {
                            @Override
                            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":

                                        Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                        pushNotification.putExtra("NotificationCount", response.body().getNotificationCount());
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);


                                        bundle = getIntent().getExtras();
                                        if (bundle != null && bundle.containsKey("IntentPassing")) {
                                            IntentPassing = bundle.getString("IntentPassing");
                                            if (IntentPassing.equals("FcmIntentPassing")) {
                                                JsonRegarding = bundle.getString("JsonRegarding");
                                                if (JsonRegarding.equals("1")) {
                                                    JsonNotificationId = bundle.getString("JsonNotificationId");
                                                    JsonRegardingId = bundle.getString("JsonRegardingId");
                                                    Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                                    intent.putExtra("JsonRegardingId", JsonRegardingId);
                                                    intent.putExtra("JsonNotificationId", JsonNotificationId);
                                                    intent.putExtra("IntentPassing", IntentPassing);

                                                    startActivity(intent);
                                                    SharedPreferences.Editor editorfcm1 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                    editorfcm1.putString("AccessToken", response.body().getAccessToken());

                                                    editorfcm1.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                                    Log.i("NotificationCount", response.body().getNotificationCount());
                                                    Log.i("AccessToken", response.body().getAccessToken());
                                                    editorfcm1.putString("DeviceId", displayFirebaseRegId());
                                                    editorfcm1.apply();

                                                } else if (JsonRegarding.equals("2")) {
                                                    JsonRegardingId = bundle.getString("JsonRegardingId");
                                                    JsonNotificationId = bundle.getString("JsonNotificationId");
                                                    Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                                    intent.putExtra("JsonRegardingId", JsonRegardingId);
                                                    intent.putExtra("JsonNotificationId", JsonNotificationId);
                                                    intent.putExtra("IntentPassing", IntentPassing);
                                                    startActivity(intent);
                                                    SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                    editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                                    Log.i("AccessToken", response.body().getAccessToken());
                                                    editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                                    editorfcm2.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                                    Log.i("NotificationCount", response.body().getNotificationCount());
                                                    editorfcm2.apply();

                                                } else if (JsonRegarding.equals("")) {
                                                    Intent intent = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                                    SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                    editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                                    Log.i("AccessToken", response.body().getAccessToken());
                                                    editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                                    editorfcm2.apply();
                                                    startActivity(intent);
                                                }

                                            }
                                        } else {
                                            SharedPreferences.Editor editorNav = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editorNav.putString("AccessToken", response.body().getAccessToken());
                                            Log.i("AccessToken", response.body().getAccessToken());
                                            editorNav.putString("DeviceId", displayFirebaseRegId());
                                            editorNav.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                            editorNav.apply();
                                            Log.i("NotificationCount", response.body().getNotificationCount());
                                            Intent intentOTP = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                            startActivity(intentOTP);
                                        }

                                        break;

                                    case "108":
                                        break;
                                    default:
                                        myPreference.ShowDialog(LoginActivity.this, "Oops", response.body().getMessage());
                                        break;

                                }


                            }

                            @Override
                            public void onFailure(Call<LoginPojo> call, Throwable t) {
                                progressBar.dismiss();

                                Log.i("logLoginScere",t.toString());
                                try {
                                    Toast.makeText(LoginActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                    } else {

                        myPreference.ShowDialog(LoginActivity.this, "Oops", "Missing Fields ");
                    }


                } else {
                    myPreference.ShowDialog(LoginActivity.this, "Oops", "There is not internet connection");
                }
            }
        });
        checkboxRememberPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkboxRememberPassword.isChecked()) {
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("CheckBoxTrue", "0");
                    editor.apply();
                } else {
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("CheckBoxTrue", "1");
                    editor.apply();
                }
            }
        });


//1.google sign in option for creating gso
        ImageViewGoogleLoginButton.setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//2.google apo client create
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.Notification) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        //remember password validation
//        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
//        MoblieNumber = prefs.getString("MoblieNumber","");
//        Password = prefs.getString("Password","");
//        Checkbox = prefs.getString("CheckBoxTrue","1");
//        if(Checkbox.equals("0")){
//            checkboxRememberPassword.setChecked(true);
//            EditTextMoblieNumber.setText(MoblieNumber);
//            EditTextPassword.setText(Password);
//        }else{
//            checkboxRememberPassword.setChecked(false);
//            EditTextMoblieNumber.setText("");
//            EditTextPassword.setText("");
//        }
//
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImageViewGoogleLoginButton:
                signin();
                break;
            case R.id.ImageViewFacebookButton:
                loginButton.performClick();
                break;
        }
    }

    private void signin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            GmailpersonName = acct.getDisplayName();
            GmailEmail = acct.getEmail();
            GmailId = acct.getId();
            updataUI(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset", "" + data);
//            data.getData().getPath()
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handlerResult(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    //gmailloginbuttonvisiable
    private void updataUI(boolean isLogin) {
        if (isLogin == true) {
            ImageViewGoogleLoginButton.setVisibility(View.GONE);
            ImageViewFacebookButton.setVisibility(View.GONE);

            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(LoginActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                jsonObject.addProperty("socialEmail", GmailEmail);
                jsonObject.addProperty("socialId", GmailId);
                jsonObject.addProperty("socialType", 2);
                API apiService = createServiceHeader(API.class);
                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                call.enqueue(new Callback<SoicalLoginPojo>() {
                    @Override
                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                        progressBar.dismiss();
                        Log.i("Code", "" + response.body().getCode());
                        switch (response.body().getCode()) {
                            case "200":

                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                Log.i("NotificationCount", response.body().getNotificationCount());
                                editor.apply();
                                Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                pushNotification.putExtra("NotificationCount", response.body().getNotificationCount());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                bundle = getIntent().getExtras();
                                if (bundle != null && bundle.containsKey("IntentPassing")) {
                                    IntentPassing = bundle.getString("IntentPassing");
                                    if (IntentPassing.equals("FcmIntentPassing")) {
                                        JsonRegarding = bundle.getString("JsonRegarding");
                                        if (JsonRegarding.equals("1")) {
                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                            intent.putExtra("IntentPassing", IntentPassing);
                                            startActivity(intent);
                                            SharedPreferences.Editor editorfcm1 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editorfcm1.putString("AccessToken", response.body().getAccessToken());
                                            Log.i("AccessToken", response.body().getAccessToken());
                                            editorfcm1.putString("DeviceId", displayFirebaseRegId());
                                            editorfcm1.apply();

                                        } else if (JsonRegarding.equals("2")) {
                                            JsonRegardingId = bundle.getString("JsonRegardingId");
                                            JsonNotificationId = bundle.getString("JsonNotificationId");
                                            Intent intent = new Intent(LoginActivity.this, NotificationFullViewActivity.class);
                                            intent.putExtra("JsonNotificationId", JsonNotificationId);
                                            intent.putExtra("JsonRegardingId", JsonRegardingId);
                                            intent.putExtra("IntentPassing", IntentPassing);
                                            startActivity(intent);
                                            SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                            Log.i("AccessToken", response.body().getAccessToken());
                                            editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                            editorfcm2.apply();

                                        } else if (JsonRegarding.equals("")) {
                                            Intent intent = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                            SharedPreferences.Editor editorfcm2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editorfcm2.putString("AccessToken", response.body().getAccessToken());
                                            Log.i("AccessToken", response.body().getAccessToken());
                                            editorfcm2.putString("DeviceId", displayFirebaseRegId());
                                            editorfcm2.apply();
                                            startActivity(intent);
                                        }

                                    }
                                } else {
                                    Log.i("accesstotken", "" + response.body().getAccessToken());
                                    SharedPreferences.Editor editorfcm3 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editorfcm3.putString("AccessToken", response.body().getAccessToken());
                                    Log.i("AccessToken", response.body().getAccessToken());
                                    editorfcm3.putString("DeviceId", displayFirebaseRegId());
                                    editorfcm3.apply();
                                    Intent intentUserProfile = new Intent(LoginActivity.this, UserNavigationActivity.class);
                                    startActivity(intentUserProfile);
                                }


                                break;
                            case "300":

                                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LoginActivity.this, R.style.RajCustomDialog);
                                final LayoutInflater inflater = (LayoutInflater) LoginActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                imageDialog.setView(dialogView);
                                TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                TextviewSmallTitle1.setText(response.body().getMessage());
                                TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                TextviewBigTitle.setText("Success");
                                Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                ButtonOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        Intent intentSignupPage = new Intent(LoginActivity.this, SignUpActivity.class);
                                        intentSignupPage.putExtra("EmailID", GmailEmail);
                                        intentSignupPage.putExtra("EmailIDUnique", GmailId);
                                        intentSignupPage.putExtra("SocialType", "2");
//                                        myPreference.ShowSuccessDialog(LoginActivity.this,"Success",response.body().getMessage());
                                        startActivity(intentSignupPage);
                                    }
                                });
                                dialog = imageDialog.create();
                                dialog.show();


                                break;

                            case "108":
                                Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                                startActivity(intent);
                                break;
                            default:
                                myPreference.ShowDialog(LoginActivity.this, "Oops", response.body().getMessage());
                                break;

                        }

                    }

                    @Override
                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        Toast.makeText(LoginActivity.this, " Something went wrong ", Toast.LENGTH_SHORT).show();

                    }
                });

            } else {
                myPreference.ShowDialog(LoginActivity.this, "Oops", "There is not internet connection");
            }

        } else {
            ImageViewGoogleLoginButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            LoginActivity.this.finishAffinity();
        }
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("DeviceId", "");
        if (!TextUtils.isEmpty(regId)) {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        } else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }


}
