package com.codegenstudios.purohit.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.codegenstudios.purohit.Fragment.ChangePasswordFragment;
import com.codegenstudios.purohit.Fragment.DashBoardFragment;
import com.codegenstudios.purohit.Fragment.MyBookingFragment;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.NotificationService.MyFirebaseMessagingService;
import com.codegenstudios.purohit.Pojo.NewChangedPasswordPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

public class UserNavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    boolean doubleBackToExitPressedOnce = false;
    GoogleApiClient mGoogleApiClient;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";
    Bundle bundle;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Integer NotificationCount;
    Toolbar toolbar;
    DrawerLayout drawer;

    MenuItem menuItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference(this);
        bundle = getIntent().getExtras();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext()) //Use app context to prevent leaks using activity
                //.enableAutoManage(this /* FragmentActivity */, connectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                    if(intent.getAction().equals(MyFirebaseMessagingService.NOTIFICATION_COUNT)){
                        SharedPreferences myPrefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);  //Activity1.class
                        NotificationCount = myPrefs.getInt("NotificationCount",0);
                        if(NotificationCount==0){
                            menuItem.setIcon(buildCounterDrawable(0));
                        }else{
                            menuItem.setIcon(buildCounterDrawable(NotificationCount));
                        }

                    }
            }
        };
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1){
            if (doubleBackToExitPressedOnce) {
                moveTaskToBack(true);
                    UserNavigationActivity.this.finish();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                   UserNavigationActivity.this.finishAffinity();
                }
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }

        else if (count > 1) {
            super.onBackPressed();
            UserNavigationActivity.this.finish();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                UserNavigationActivity.this.finishAffinity();
            }

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_navigation, menu);
        menuItem = menu.findItem(R.id.Notification);
        SharedPreferences myPrefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);  //Activity1.class
        NotificationCount = myPrefs.getInt("NotificationCount",0);
        if(NotificationCount==0){
            menuItem.setIcon(buildCounterDrawable(0));
        }else{
            menuItem.setIcon(buildCounterDrawable(NotificationCount));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.Notification) {

            Intent i=new Intent(UserNavigationActivity.this,NotificaitonListActivity.class);
            startActivity(i);
            return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.MyProfile) {
            UserProfileViewFragment fragment = new UserProfileViewFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        } else if (id == R.id.Dashboard) {
            DashBoardFragment fragment = new DashBoardFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        }
        else if (id == R.id.MyBooking) {
            MyBookingFragment fragment = new MyBookingFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();

        }  else if(id == R.id.ChangePassword){
            ChangePasswordFragment fragment = new ChangePasswordFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();

        }else if(id == R.id.Logout){
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("AccessToken", null);
            editor.putString("DeviceId", null);
            editor.putString("NotificationCount", null);
            LoginManager.getInstance().logOut();
            editor.putString("FacebookLogout","fblogout");
               if (mGoogleApiClient.isConnected()) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                        mGoogleApiClient.disconnect();
                        mGoogleApiClient.connect();
                    }
            editor.apply();
            Intent i=new Intent(UserNavigationActivity.this,LoginActivity.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Resume call","Resume call");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(MyFirebaseMessagingService.NOTIFICATION_COUNT));

        if(bundle.containsKey("IntentPassing")){
            if(bundle.getString("IntentPassing").equals("MybookingFragment")){

                MyBookingFragment fragment = new MyBookingFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame,fragment)
                        .addToBackStack(null)
                        .commit();
                fragmentManager.executePendingTransactions();
            }
        }else {


            DashBoardFragment fragment = new DashBoardFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        }


//        }
    }

    private Drawable buildCounterDrawable(int count) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.notification_count_icon, null);
//        view.setBackgroundResource(backgroundImageId);
        TextView textView = (TextView) view.findViewById(R.id.count);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.count);
            counterTextPanel.setVisibility(View.GONE);

            textView.setText("" + count);
        } else {
            textView.setText("" + count);
        }

        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }


    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }





}
