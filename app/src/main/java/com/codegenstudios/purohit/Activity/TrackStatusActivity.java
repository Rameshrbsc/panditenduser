package com.codegenstudios.purohit.Activity;

import butterknife.BindView;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import butterknife.ButterKnife;

public class TrackStatusActivity extends AppCompatActivity {

    @BindView(R.id.share)
    FloatingActionButton share;
    @BindView(R.id.card_view1)
    CardView card_view1;
    @BindView(R.id.card_view2)
    CardView card_view2;
    @BindView(R.id.card_view3)
    CardView card_view3;
    @BindView(R.id.card_view4)
    CardView card_view4;
    @BindView(R.id.card_view5)
    CardView card_view5;
    @BindView(R.id.card_view6)
    CardView card_view6;
    @BindView(R.id.purohit)
    TextView purohit;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.about)
    TextView about;
    int cardview1_status = 0, cardview2_status = 0, cardview3_status = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_status);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setTitle("Tracking Status");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Intent intent = getIntent();
        if (intent != null) {
            String status = intent.getStringExtra("status");
            if(status!=null)
            if (status.equals("confirm")) {
                card_view3.setVisibility(View.GONE);

            } else {
                card_view2.setVisibility(View.GONE);
                card_view3.setVisibility(View.GONE);

            }
        }
        card_view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cardview1_status == 0) {
                    cardview1_status = 1;
                    card_view4.setVisibility(View.VISIBLE);
                } else {

                    cardview1_status = 0;
                    card_view4.setVisibility(View.GONE);
                }
            }
        });
        card_view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cardview2_status == 0) {
                    cardview2_status = 1;
                    card_view5.setVisibility(View.VISIBLE);
                } else {
                    cardview2_status = 0;
                    card_view5.setVisibility(View.GONE);
                }
            }
        });
        card_view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cardview3_status == 0) {
                    cardview3_status = 1;
                    card_view6.setVisibility(View.VISIBLE);
                } else {
                    cardview3_status = 0;
                    card_view6.setVisibility(View.GONE);
                }
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                intent.putExtra(Intent.EXTRA_TEXT, "Name :" + purohit.getText().toString() + "\nPhone :" + phone.getText().toString() + "\nAbout :" + about.getText().toString());
                startActivity(intent);
            }
        });

    }

}
