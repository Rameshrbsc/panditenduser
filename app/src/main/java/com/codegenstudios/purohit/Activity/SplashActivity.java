package com.codegenstudios.purohit.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.UserSessionAuthPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import io.fabric.sdk.android.Fabric;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class SplashActivity extends AppCompatActivity {
    MyPreference myPreference;
    private ProgressDialog progressBar;
    AlertDialog dialog;
    Bundle bundle;
    String IntentPassing,JsonRegarding,JsonRegardingId,JsonNotificationId;
    String MY_PREFS_NAME = "MyPrefsFile";
    public static String  NOTIFICATION_COUNT = "NotificationCount";
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference(this);

        bundle = getIntent().getExtras();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "id");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "name");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        Trace myTrace = FirebasePerformance.getInstance().newTrace("test_trace");
        myTrace.start();
        myTrace.incrementMetric("item_cache_hit", 1);
        myTrace.stop();
        int badgeCount = 1;
        ShortcutBadger.applyCount(SplashActivity.this, badgeCount);
    }
    private class PrefetchData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (bundle != null && bundle.containsKey("IntentPassing")) {
                IntentPassing = bundle.getString("IntentPassing");
                if(IntentPassing.equals("FcmIntentPassing")){
                    JsonRegarding = bundle.getString("JsonRegarding");
                    JsonRegardingId = bundle.getString("JsonRegardingId");
                    JsonNotificationId = bundle.getString("JsonNotificationId");

                    if(JsonRegarding.equals("1")){
                        Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                        intent.putExtra("IntentPassing",IntentPassing);
                        intent.putExtra("JsonRegardingId",JsonRegardingId);
                        intent.putExtra("JsonRegarding",JsonRegarding);
                        intent.putExtra("JsonNotificationId",JsonNotificationId);
                        startActivity(intent);

                    }else
                    if(JsonRegarding.equals("2")) {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        intent.putExtra("IntentPassing", IntentPassing);
                        intent.putExtra("JsonRegardingId", JsonRegardingId);
                        intent.putExtra("JsonRegarding", JsonRegarding);
                        intent.putExtra("JsonNotificationId", JsonNotificationId);
                        startActivity(intent);

                    }

                }
            }else{
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

            return null;
        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (myPreference.isInternetOn()){

        if (myPreference.getDefaultRunTimeValue()[0] != null){

                progressBar = new ProgressDialog(SplashActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                API apiService = createServiceHeader(API.class);
                Call<UserSessionAuthPojo> call = apiService.USER_SESSION_AUTH_POJO_CALL(jsonObject);
                Log.e("Jsonrequest",jsonObject.toString());
                call.enqueue(new Callback<UserSessionAuthPojo>() {
                    @Override
                    public void onResponse(Call<UserSessionAuthPojo> call, Response<UserSessionAuthPojo> response) {
                        progressBar.dismiss();
                        Log.e("Jsonrequest",response.body().toString());
                        switch (response.body().getCode()) {

                            case "200":
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putInt("NotificationCount", Integer.parseInt(response.body().getNotificationCount()));
                                Log.i("splashNotificationCount",response.body().getNotificationCount());
                                editor.apply();

                                Intent pushNotification = new Intent(NOTIFICATION_COUNT);
                                pushNotification.putExtra("NotificationCount",response.body().getNotificationCount() );
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);

                                if (bundle != null && bundle.containsKey("IntentPassing")) {
                                    IntentPassing = bundle.getString("IntentPassing");
                                    if(IntentPassing.equals("FcmIntentPassing")){
                                        JsonRegarding = bundle.getString("JsonRegarding");
                                        JsonRegardingId = bundle.getString("JsonRegardingId");
                                        JsonNotificationId = bundle.getString("JsonNotificationId");
                                        if(JsonRegarding.equals("1")){
                                            Intent intent = new Intent(SplashActivity.this,NotificationFullViewActivity.class);
                                            intent.putExtra("IntentPassing",IntentPassing);
                                            intent.putExtra("JsonRegardingId",JsonRegardingId);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            intent.putExtra("JsonNotificationId",JsonNotificationId);
                                            startActivity(intent);

                                        }else  if(JsonRegarding.equals("2")){
                                            Intent intent = new Intent(SplashActivity.this,NotificationFullViewActivity.class);
                                            intent.putExtra("IntentPassing",IntentPassing);
                                            intent.putExtra("JsonRegardingId",JsonRegardingId);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            intent.putExtra("JsonNotificationId",JsonNotificationId);
                                            startActivity(intent);

                                        }else if(JsonRegarding.equals("")){
                                            Intent intent = new Intent(SplashActivity.this,UserNavigationActivity.class);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            startActivity(intent);
                                        }


                                    }
                                }
                                else{
                                    Intent intentHome = new Intent(SplashActivity.this, UserNavigationActivity.class);
                                    startActivity(intentHome);
                                }

                                break;

                            case "108":
                                if (bundle != null && bundle.containsKey("IntentPassing")) {
                                    IntentPassing = bundle.getString("IntentPassing");
                                    JsonRegarding = bundle.getString("JsonRegarding");
                                    JsonRegardingId = bundle.getString("JsonRegarding");
                                    JsonNotificationId = bundle.getString("JsonNotificationId");

                                    if(IntentPassing.equals("FcmIntentPassing")){
                                        if(JsonRegarding.equals("1")){
                                            Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                                            intent.putExtra("IntentPassing",IntentPassing);
                                            intent.putExtra("JsonRegardingId",JsonRegardingId);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            intent.putExtra("JsonNotificationId",JsonNotificationId);
                                            startActivity(intent);

                                        }else if(JsonRegarding.equals("2")){
                                            Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                                            intent.putExtra("IntentPassing",IntentPassing);
                                            intent.putExtra("JsonRegardingId",JsonRegardingId);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            intent.putExtra("JsonNotificationId",JsonNotificationId);
                                            startActivity(intent);

                                        }else if(JsonRegarding.equals("")){
                                            Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                                            intent.putExtra("JsonRegarding",JsonRegarding);
                                            startActivity(intent);
                                        }

                                    }
                                }else{
                                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                }

                                break;
                            default:
                                myPreference.ShowDialog(SplashActivity.this,"Oops",response.body().getMessage());
                                break;

                        }

                    }

                    @Override
                    public void onFailure(Call<UserSessionAuthPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            Toast.makeText(SplashActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });



        }else{


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new PrefetchData().execute();
                Log.i("LoginRequired","LoginRequired");

            }

        },4000);


        }

        }else{
           myPreference.ShowDialog(SplashActivity.this,"Oops","There is not internet connection");
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SplashActivity.this.finish();
    }
}
