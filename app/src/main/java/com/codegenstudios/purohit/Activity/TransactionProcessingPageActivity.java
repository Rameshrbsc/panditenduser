package com.codegenstudios.purohit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.BookDetails;
import com.codegenstudios.purohit.Pojo.TransactionSuccessSubmitPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class TransactionProcessingPageActivity extends AppCompatActivity implements PaymentResultListener {
    @BindView(R.id.ButtonPayNow)
    Button ButtonPayNow;
    @BindView(R.id.image)
    ImageView imageIcon;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    //    @BindView(R.id.SuccesImageView)
//    ImageView SuccesImageView;
//    @BindView(R.id.TextviewSmallTitle1)
//    TextView TextviewSmallTitle1;
//    @BindView(R.id.book_id)
//    TextView book_id;
//    @BindView(R.id.date_text)
//    TextView date_text;
    @BindView(R.id.pooja_charge)
    TextView pooja_charge;
    @BindView(R.id.pooja_name)
    TextView pooja_name;
    @BindView(R.id.cgst_charge)
    TextView cgst_charge;
    @BindView(R.id.sgst_charge)
    TextView sgst_charge;
    @BindView(R.id.service)
    TextView service_charge;
    @BindView(R.id.total)
    TextView total;

    @BindView(R.id.date)
    TextView txtDate;
    @BindView(R.id.time)
    TextView txtTime;

    String BookingId, BookingAmount, mobileNumber, emailId, ImageStr;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_processing_page);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setTitle("Payment Pending");
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        myPreference = new MyPreference(this);
        Checkout.preload(getApplicationContext());

        try {
            Intent intent = getIntent();
            BookingId = Objects.requireNonNull(intent.getExtras().get("BookingId")).toString();
            BookingAmount = Objects.requireNonNull(intent.getExtras().get("BookingAmount")).toString();
            Log.i("BookingId", "" + BookingId);

            Gson gson = new Gson();
            myPreference.getAllBookingDetails(getApplicationContext());

            String json = myPreference.getAllBookingDetails(getApplicationContext());
            BookDetails obj = gson.fromJson(json, BookDetails.class);

            String date = obj.getBookingDate();
            String strTime = obj.getBookingTime();
            emailId = obj.getBookingEmailID();
            mobileNumber = obj.getBookingMoblieNumber();
            ImageStr = myPreference.getImage(getApplicationContext());

            pooja_name.setText(obj.getBookingPoojaName());
            txtDate.setText(obj.getBookingDate());
            txtTime.setText(obj.getBookingTime());

            Glide.with(this).load(myPreference.getImage(getApplicationContext())).into(imageIcon);


            if (BookingId == null) {

                final Dialog dialog = new Dialog(TransactionProcessingPageActivity.this);
                dialog.setContentView(R.layout.custom_failor_dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                TextView TextviewBigTitle = (TextView) dialog.findViewById(R.id.TextviewBigTitle);
                TextviewBigTitle.setText("Transaction Failed");
                pooja_charge.setText("₹  " + BookingAmount);
                TextView TextviewSmallTitle1 = (TextView) dialog.findViewById(R.id.TextviewSmallTitle1);
//                TextviewSmallTitle1.setText(""+t.getMessage());
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent1 = new Intent(TransactionProcessingPageActivity.this, UserNavigationActivity.class);
                        startActivity(intent1);
                    }
                });
                dialog.show();
                TextviewSmallTitle1.setVisibility(View.GONE);
//                SuccesImageView.setVisibility(View.GONE);


            } else {
//                TextviewSmallTitle1.setText("Success");
                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                String formattedDate = df.format(c);
//                date_text.setText(formattedDate.toString());
//                book_id.setText(BookingId);
                pooja_charge.setText("₹" + BookingAmount);
                int BookAmount = Integer.parseInt(BookingAmount);
                int serviceCharge = 50;
                int sgst_charg = 5;
                int cgst_charge = 5;
                int tota = BookAmount + serviceCharge + sgst_charg + cgst_charge;

                total.setText("₹" + String.valueOf(tota));
               /* total.setText("₹  " + String.valueOf(Integer.parseInt(BookingAmount)
                        + Integer.parseInt(service_charge.getText().toString()) +
                        Integer.parseInt(sgst_charge.getText().toString()) +
                        Integer.parseInt(cgst_charge.getText().toString())));*/


//                TextviewSmallTitle1.setVisibility(View.VISIBLE);
//                SuccesImageView.setVisibility(View.VISIBLE);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ButtonPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(TransactionProcessingPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("BookingId", BookingId);
                    jsonObject.addProperty("TransactionId", "dhfjkhd");
                    jsonObject.addProperty("Status", "1");
                    API apiService = createServiceHeader(API.class);
                    Call<TransactionSuccessSubmitPojo> call = apiService.TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<TransactionSuccessSubmitPojo>() {
                        @Override
                        public void onResponse(Call<TransactionSuccessSubmitPojo> call, Response<TransactionSuccessSubmitPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    Intent intent1 = new Intent(TransactionProcessingPageActivity.this, UserNavigationActivity.class);
                                    startActivity(intent1);
                                    break;

                                case "108":
                                *//*Intent intent = new Intent(TransactionProcessingPageActivity.this, UserNavigationActivity.class);
                                startActivity(intent);*//*
                                    break;
                                default:
                                    myPreference.ShowDialog(TransactionProcessingPageActivity.this, "Oops", response.body().getMessage());
                                    break;

                            }


                        }

                        @Override
                        public void onFailure(Call<TransactionSuccessSubmitPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(TransactionProcessingPageActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    myPreference.ShowDialog(TransactionProcessingPageActivity.this, "Oops", "There is not internet connection");
                }*/
                String confirm=getResources().getString(R.string.yes);
                String cancel=getResources().getString(R.string.no);
                String sure=getResources().getString(R.string.are_you);

                final AlertDialog alertDialog = new AlertDialog.Builder(TransactionProcessingPageActivity.this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);

                alertDialog.setMessage(sure);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, confirm,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startPayment();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();


                /*Intent intent = new Intent(TransactionProcessingPageActivity.this,TransactionResultActivity.class);
                intent.putExtra("TransactionResult","Success");
                intent.putExtra("BookingId",BookingId);

                startActivity(intent);*/
            }
        });


    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Razorpay Corp");
            options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", ImageStr);
            options.put("currency", "INR");
            options.put("amount", "100");

            JSONObject preFill = new JSONObject();
            preFill.put("email", emailId);
            preFill.put("contact", mobileNumber);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            Toast.makeText(this, "Payment Successful: " + s, Toast.LENGTH_SHORT).show();


            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(TransactionProcessingPageActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", BookingId);
                jsonObject.addProperty("TransactionId", s);
                jsonObject.addProperty("Status", "1");
                API apiService = createServiceHeader(API.class);
                Call<TransactionSuccessSubmitPojo> call = apiService.TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(jsonObject);
                call.enqueue(new Callback<TransactionSuccessSubmitPojo>() {
                    @Override
                    public void onResponse(Call<TransactionSuccessSubmitPojo> call, Response<TransactionSuccessSubmitPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                Intent intent1 = new Intent(TransactionProcessingPageActivity.this, UserNavigationActivity.class);
                                startActivity(intent1);
                                break;

                            case "108":
                                /*Intent intent = new Intent(TransactionProcessingPageActivity.this, UserNavigationActivity.class);
                                startActivity(intent);*/
                                break;
                            default:
                                myPreference.ShowDialog(TransactionProcessingPageActivity.this, "Oops", response.body().getMessage());
                                break;

                        }


                    }

                    @Override
                    public void onFailure(Call<TransactionSuccessSubmitPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            Toast.makeText(TransactionProcessingPageActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                myPreference.ShowDialog(TransactionProcessingPageActivity.this, "Oops", "There is not internet connection");
            }

        } catch (Exception e) {
            Log.e("fdfdf", "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        try {
            Toast.makeText(this, "Payment failed: " + i + " " + s, Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            Log.e("dfff", "Exception in onPaymentError", e);
        }

    }
}
