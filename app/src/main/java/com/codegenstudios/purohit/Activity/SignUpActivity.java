package com.codegenstudios.purohit.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import com.codegenstudios.purohit.API.API;

import com.codegenstudios.purohit.Adapter.PlaceArrayAdapter;
import com.codegenstudios.purohit.Config.Config;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.Pojo.SoicalLoginPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.text_view)
    TextView text_view;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextEmailId)
    EditText EditTextEmailId;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextNatchathiram)
    EditText EditTextNatchathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.FrameLayout1)
    FrameLayout FrameLayout1;
    @BindView(R.id.TextViewBackLoginPage)
    TextView TextViewBackLoginPage;
    @BindView(R.id.ButtonProfilePic)
    Button ButtonProfilePic;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.closebtn)
    Button closebtn;
    @BindView(R.id.ButtonCreateAccount)
    Button ButtonCreateAccount;
    @BindView(R.id.RelativelayoutImageView)
    RelativeLayout RelativelayoutImageView;
    String userChoosenTask, encodedImage, FacebookID, Facebookemail, FacebookName, GmailpersonName, GmailEmail, GmailId, FilePath = null, TempUri, error;
    private Uri currentImageUri;
    Uri tempUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog;
    Bitmap thumbnail = null, bitmap;
    File dir = new File(Environment.getExternalStorageDirectory() + "/PurohitUserApp/.Images");
    File file;
    File camimage;
    @BindView(R.id.ImageViewGoogleLoginButton)
    ImageView ImageViewGoogleLoginButton;
    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile", encodedImagecheck;
    private ProgressDialog progressBar;
    String EmailIDsaved, EmailNameSaved, EmailIDToken, SocialType;
    LoginButton loginButton;
    @BindView(R.id.ImageViewFacebookButton)
    ImageView ImageViewFacebookButton;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    GoogleApiClient mGoogleApiClientPlace;
    private Target mTarget;
    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private int REQUEST_PERMISSION_SETTING = 111;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final String LOG_TAG = "SignUpActdivity";

    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private AutoCompleteTextView mAutocompleteTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
        // bind the view using butterknife
        ButterKnife.bind(this);
        ButtonProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        });


        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext()) //Use app context to prevent leaks using activity
                //.enableAutoManage(this /* FragmentActivity */, connectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();


        mGoogleApiClientPlace = new GoogleApiClient.Builder(SignUpActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();
        myPreference = new MyPreference(SignUpActivity.this);
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("ImageUriPath", null);
        editor.apply();

/*

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
*/

        /*AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(filter);*/
        /*autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                text_view.setText(place.getName());
            }

            @Override
            public void onError(Status status) {
                text_view.setText(status.toString());
            }
        });*/

        /*mAutocompleteTextView = (AutoCompleteTextView) findViewById(R.id
                .autoCompleteTextView);
        mAutocompleteTextView.setThreshold(3);

        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);*/

        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageViewProfilePic.setImageBitmap(null);
                closebtn.setVisibility(View.GONE);
                RelativelayoutImageView.setVisibility(View.GONE);
                camimage = null;
                currentImageUri = null;
                FilePath = null;
            }
        });
        final Intent intent = getIntent();
        try {

            EmailIDsaved = intent.getExtras().get("EmailID").toString();
            FacebookName = intent.getExtras().get("username").toString();
            if (EmailIDsaved != null) {
                ImageViewFacebookButton.setVisibility(View.GONE);
                ImageViewGoogleLoginButton.setVisibility(View.GONE);
            } else {
                ImageViewFacebookButton.setVisibility(View.VISIBLE);
                ImageViewGoogleLoginButton.setVisibility(View.VISIBLE);
            }
            EmailIDToken = intent.getExtras().get("EmailIDUnique").toString();
            SocialType = intent.getExtras().get("SocialType").toString();
            EditTextUserName.setText(FacebookName);
            EditTextEmailId.setText(EmailIDsaved);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.codegenstudios.purohit",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("com.example.fishe", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(SignUpActivity.this, LoginActivity.class);
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("AccessToken", null);
                editor.putString("DeviceId", null);
                LoginManager.getInstance().logOut();
                editor.putString("FacebookLogout", "fblogout");
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient.connect();
                }
                editor.apply();
                startActivity(intent1);
            }
        });


        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        ImageViewFacebookButton.setOnClickListener(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.i(SignUpActivity.this.getPackageName(), "On Success");
                Log.i(SignUpActivity.this.getPackageName(), loginResult.getAccessToken().getToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Facebookemail = object.getString("email");
                            FacebookName = object.getString("name");
                            FacebookID = object.getString("id");
                            Log.i("FacebookID", "" + FacebookID);

                            if (myPreference.isInternetOn()) {
                                progressBar = new ProgressDialog(SignUpActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.setCancelable(false);
                                progressBar.show();
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                                jsonObject.addProperty("socialEmail", Facebookemail);
                                jsonObject.addProperty("socialId", FacebookID);
                                jsonObject.addProperty("socialType", 1);
                                API apiService = createServiceHeader(API.class);
                                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                                call.enqueue(new Callback<SoicalLoginPojo>() {
                                    @Override
                                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                Log.i("", "" + response.body().getAccessToken());
                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getAccessToken());
                                                editor.putString("DeviceId", displayFirebaseRegId());
                                                editor.apply();
                                                Intent intentOTP = new Intent(SignUpActivity.this, UserNavigationActivity.class);
                                                startActivity(intentOTP);
                                                ImageViewFacebookButton.setVisibility(View.GONE);
                                                ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                break;
                                            case "300":
                                                EmailIDsaved = Facebookemail;
                                                EmailIDToken = FacebookID;
                                                SocialType = "1";
                                                myPreference.ShowSuccessDialog(SignUpActivity.this, "Success", response.body().getMessage());
                                                ImageViewFacebookButton.setVisibility(View.GONE);
                                                ImageViewGoogleLoginButton.setVisibility(View.GONE);

                                                EditTextUserName.setText(FacebookName);
                                                EditTextEmailId.setText(EmailIDsaved);
                                                break;

                                            case "108":
                                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                break;
                                            default:
                                                myPreference.ShowDialog(SignUpActivity.this, "Oops", response.body().getMessage());
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        Toast.makeText(SignUpActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    }
                                });


                            } else {
                                myPreference.ShowDialog(SignUpActivity.this, "Oops", "There is not internet connection");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.i(SignUpActivity.this.getPackageName(), "On Cancel");

            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                FacebookException facebookException = exception;
                Log.i(SignUpActivity.this.getPackageName(), "On Error");
                Log.i(SignUpActivity.this.getPackageName(), facebookException.getMessage());
                Log.i(SignUpActivity.this.getPackageName(), facebookException.getLocalizedMessage());
            }
        });


        EditTextAadhar1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar1.getText().toString().length() == 4) {
                    EditTextAadhar2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditTextAadhar2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar2.getText().toString().length() == 4) {
                    EditTextAadhar3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar1.requestFocus();
                }
            }
        });
        EditTextAadhar3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar2.requestFocus();
                }

            }
        });
        //this is changing the keyboard has done or tick icon ready to submit
        EditTextAadhar3.setImeOptions(EditorInfo.IME_ACTION_DONE);


        ButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(final View v) {
                final String AadhaNumber = EditTextAadhar1.getText().toString() + EditTextAadhar2.getText().toString() + EditTextAadhar3.getText().toString();
                Log.i("AadhaNumber", "" + AadhaNumber);

                if (isValidPassword(EditTextPassword.getText().toString().trim())) {
                    if (Check(EditTextUserName) && Check(EditTextLanguage) && Check(EditTextPassword)&& Check(EditTextSection)) {

                        if (isMobValid(EditTextMoblieNumber.getText().toString()) && isValidEmail(EditTextEmailId.getText().toString())) {

                            Intent intentConfirm = new Intent(SignUpActivity.this, SignupConfirmViewActivity.class);
                            //SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            intentConfirm.putExtra("UserName", EditTextUserName.getText().toString());
                            intentConfirm.putExtra("UserEmailID", EditTextEmailId.getText().toString());
                            intentConfirm.putExtra("UserMoblieNumber", EditTextMoblieNumber.getText().toString());
                            intentConfirm.putExtra("UserPassword", EditTextPassword.getText().toString());
                            intentConfirm.putExtra("UserNatchtathiram", EditTextNatchathiram.getText().toString());
                            intentConfirm.putExtra("UserGothram", EditTextGothram.getText().toString());
                            intentConfirm.putExtra("UserSection", EditTextSection.getText().toString());
                            intentConfirm.putExtra("UserLanguage", EditTextLanguage.getText().toString());
                            intentConfirm.putExtra("UserAadhar", AadhaNumber);
                            if (FilePath != null) {
                                intentConfirm.putExtra("ImageUriPath", FilePath);
                            } else {
                                intentConfirm.putExtra("ImageUriPath", (Bundle) null);
                            }
                            intentConfirm.putExtra("EmailID", EmailIDsaved);
                            intentConfirm.putExtra("EmailIDUnique", EmailIDToken);
                            intentConfirm.putExtra("SocialType", SocialType);
                            intentConfirm.putExtra("IntentPassing", "SignupPage");
                            startActivity(intentConfirm);

                        } else {
                            myPreference.ShowDialog(SignUpActivity.this, "Oops", "Email or moblie enter correctly ");
                        }
                    } else {
                        myPreference.ShowDialog(SignUpActivity.this, "Oops", "Missing Fields ");
                    }
                } else {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Password lenght must have alleast 8 character, one special character, one uppercase character, one lowercase character and number !!");
                }


            }

        });

//1.google sign in option for creating gso
        ImageViewGoogleLoginButton.setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//2.google apo client create
        /*googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
*/
    }

    private void selectImage() {
        final String[] value = getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    boolean result= Utility.checkPermission(SignUpActivity.this);
                    userChoosenTask = "Take Photo";

                    alertDialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    dir.mkdirs();

                    if (dir.isDirectory()) {
                        Log.i("FileCreated", "FileCreated");

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        camimage = new File(dir, "PurhoitAPP" + timeStamp + ".jpg");
                        currentImageUri = Uri.fromFile(camimage);//file
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    } else {
                        Log.i("FileNotCreated", "FileNotCreated");
                    }


                } else if (position == 1) {

                    userChoosenTask = "Choose from Library";
//                    boolean result= Utility.checkPermission(SignUpActivity.this);


                    alertDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                    Log.i("onsave", "going to gallery ");

                } else if (position == 2) {
                    alertDialog.dismiss();
                    userChoosenTask = "Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset", "" + data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handlerResult(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                if (currentImageUri != null) {
                    try {
                        FilePath = currentImageUri.getPath();
                        ;
                        CamLoadImage(BitmapFactory.decodeStream(getContentResolver().openInputStream(currentImageUri)), currentImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(SignUpActivity.this.getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave", "data got from gallery ");

                    FilePath = getRealPathFromURI(SignUpActivity.this, currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if (imageFileFilter.accept(camimage)) {
                        Log.i("GalleryImageLoaded", "GalleryImageLoaded");
                        LoadImage(currentImageUri);
                    } else {
                        Toast.makeText(this, "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (checkPermissions()) {
                    selectImage();
                }
            }
        }
    }


    private void CamLoadImage(Bitmap thumbnail, Uri currentImageUri) {//camera
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
        /*Picasso.with(SignUpActivity.this)
                .load(new File(FilePath))
                .into(ImageViewProfilePic);*/
        Picasso.with(this).load(new File(FilePath)).transform(new SignUpActivity.SquareTransform()).into(ImageViewProfilePic);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    private void LoadImage(Uri thumbnail) {//gallery
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
        Picasso.with(SignUpActivity.this)
                .load(thumbnail)
                .into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        Log.i("onsave", " viewwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }


    //Some device image get rotated
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    String convertstring(EditText txtEdit) {
        if (txtEdit.getText().toString().isEmpty()) {
            return "";
        } else {
            return txtEdit.getText().toString();
        }
    }

    //moblie number validations
    public static boolean isMobValid(String s) {
        Pattern p = Pattern.compile("[0-9]{10}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    //email vaild
    public static boolean isValidEmail(String email) {
        String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    //password validation
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImageViewGoogleLoginButton:
                signin();
                break;
            case R.id.ImageViewFacebookButton:
                loginButton.performClick();
                break;
        }

    }

    private void signin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            GmailpersonName = acct.getDisplayName();
            GmailEmail = acct.getEmail();
            GmailId = acct.getId();
            Log.e(TAG, "Name: " + GmailpersonName + ", email: " + GmailEmail);
            updataUI(true);
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();

    }

    private void updataUI(boolean isLogin) {

        if (isLogin == true) {
            ImageViewGoogleLoginButton.setVisibility(View.GONE);
            ImageViewFacebookButton.setVisibility(View.GONE);
            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(SignUpActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.setCancelable(false);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                jsonObject.addProperty("socialEmail", GmailEmail);
                jsonObject.addProperty("socialId", GmailId);
                jsonObject.addProperty("socialType", 2);
                API apiService = createServiceHeader(API.class);
                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                call.enqueue(new Callback<SoicalLoginPojo>() {
                    @Override
                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                        progressBar.dismiss();
                        Log.i("Code", "" + response.body().getCode());
                        switch (response.body().getCode()) {
                            case "200":
                                Log.i("accesstotken", "" + response.body().getAccessToken());
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("AccessToken", response.body().getAccessToken());
                                editor.putString("DeviceId", displayFirebaseRegId());
                                editor.apply();
                                Intent intentUserProfile = new Intent(SignUpActivity.this, UserNavigationActivity.class);
                                startActivity(intentUserProfile);

                                break;
                            case "300":
                                EmailIDsaved = GmailEmail;
                                EmailIDToken = GmailId;
                                SocialType = "2";
                                break;

                            case "108":
                                Toast.makeText(SignUpActivity.this, "Login Reqired", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                startActivity(intent);
                                break;
                            default:
                                myPreference.ShowDialog(SignUpActivity.this, "Oops", response.body().getMessage());
                                break;


                        }

                    }

                    @Override
                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        Toast.makeText(SignUpActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                });

            } else {
                final Dialog dialog = new Dialog(SignUpActivity.this);
                dialog.setContentView(R.layout.custom_failor_dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                TextView TextviewSmallTitle1 = (TextView) dialog.findViewById(R.id.TextviewSmallTitle1);
                TextviewSmallTitle1.setText("There is no internet connection");
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        } else {
            ImageViewGoogleLoginButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClientPlace);


    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");

    }


    public static class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static class SquareTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawPaint(paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "square";
        }
    }


    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (checkPermissions()) {
                selectImage();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private String displayFirebaseRegId() {
        String regId;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("DeviceId", "");


        if (!TextUtils.isEmpty(regId)) {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        } else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("AccessToken", null);
        editor.putString("DeviceId", null);
        LoginManager.getInstance().logOut();
        editor.putString("FacebookLogout", "fblogout");
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
        editor.apply();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                //.... write file into storage ...
//                System.out.println("SDK > BuildVersion TRUE");
//            } else {
//                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 666);  // Comment 26
//                System.out.println("go to requestPermissions");
//            }
//        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//
//            case 666: // Allowed was selected so Permission granted
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    Snackbar s = Snackbar.make(findViewById(android.R.id.content),"Permission Granted",Snackbar.LENGTH_LONG);
//                    View snackbarView = s.getView();
//                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
//                    textView.setTextColor(Color.RED);
//                    textView.setTextSize(18);
//                    textView.setMaxLines(6);
//                    s.show();
//
//                    // do your work here
//
//                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
//                    // User selected the Never Ask Again Option Change settings in app settings manually
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//                    alertDialogBuilder.setTitle("Change Permissions in Settings");
//                    alertDialogBuilder
//                            .setMessage("" +
//                                    "\nClick SETTINGS to Manually Set\n"+"Permissions to use Database Storage")
//                            .setCancelable(false)
//                            .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
//                                    intent.setData(uri);
//                                    startActivityForResult(intent, 1000);     // Comment 3.
//                                }
//                            });
//
//                     alertDialogrun = alertDialogBuilder.create();
//                    alertDialogrun.show();
//
//                } else {
//                    // User selected Deny Dialog to EXIT App ==> OR <== RETRY to have a second chance to Allow Permissions
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
//
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//                        alertDialogBuilder.setTitle("Second Chance");
//                        alertDialogBuilder
//                                .setMessage("Click RETRY to Set Permissions to Allow\n\n"+"Click EXIT to the Close App")
//                                .setCancelable(false)
//                                .setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        //ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Integer.parseInt(WRITE_EXTERNAL_STORAGE));
//                                        Intent i = new Intent(SignUpActivity.this,SignUpActivity.class);
//                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(i);
//                                    }
//                                })
//                                .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        finish();
//                                        dialog.cancel();
//                                    }
//                                });
//                        alertDialogrun = alertDialogBuilder.create();
//                        alertDialogrun.show();
//                    }
//                }
//                break;
//        }};


    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClientPlace, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("SignUPActivity", "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
            text_view.setText(place.getAddress());
            /*mNameTextView.setText(Html.fromHtml(place.getName() + ""));
            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }*/
        }
    };
}
