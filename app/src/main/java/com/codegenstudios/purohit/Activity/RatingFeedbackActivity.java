package com.codegenstudios.purohit.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Pojo.RatingFeedbackPojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

public class RatingFeedbackActivity extends AppCompatActivity {
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.RatingBar)
    RatingBar RatingBar;
    @BindView(R.id.EditTextFeedback)
    EditText EditTextFeedback;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String BookingId,rating,Feedback;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Rating");
        setSupportActionBar(toolbar);
       ButterKnife.bind(this);
        myPreference = new MyPreference(this);
        bundle = getIntent().getExtras();
         BookingId = bundle.getString("BookingId");
        rating = bundle.getString("Rating");
        Feedback = bundle.getString("FeedBack");
         if(!rating.equals("") && !Feedback.equals("")){
//             String number =   rating;
             //    float d= (float) ((number*5) /100);
//             if(! number.equals("")){
                 RatingBar.setRating(Float.parseFloat(rating));
                 EditTextFeedback.setText(Feedback);
//             }
         }


        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(RatingFeedbackActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("BookingID", BookingId);
                    jsonObject.addProperty("Rating", String.valueOf(RatingBar.getRating()));
                    jsonObject.addProperty("Feedback", EditTextFeedback.getText().toString());

                    API apiService = createServiceHeader(API.class);
                    Call<RatingFeedbackPojo> call = apiService.RATING_FEEDBACK_POJO_CALL(jsonObject);
                    Log.e("json",jsonObject.toString());
                    call.enqueue(new Callback<RatingFeedbackPojo>() {
                        @Override
                        public void onResponse(Call<RatingFeedbackPojo> call, Response<RatingFeedbackPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    onBackPressed();

                                    break;
                                case "108":
                                    Intent intent = new Intent(RatingFeedbackActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                default:
                                    myPreference.ShowDialog(RatingFeedbackActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<RatingFeedbackPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(RatingFeedbackActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }else {
                    myPreference.ShowDialog(RatingFeedbackActivity.this,"Oops","There is not internet connection");
                }


            }
        });





    }


}
