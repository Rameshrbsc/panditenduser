package com.codegenstudios.purohit.SupportClass;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.codegenstudios.purohit.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    EditText txtdate,time,FromDate,ToDate;
    String selectvalue;
    String date;
    String afterdate;
    DatePickerDialog datePickerDialog;
    Calendar c;

    public Dialog onCreateDialog(Bundle savedInstanceState){
        c= Calendar.getInstance();
        int year =c.get(Calendar.YEAR);
        int month= c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = (datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme,this,year,month,day));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return  datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        int tempmonth = month+1;
//        date = day+"/"+("0"+tempmonth)+"/"+year;
        date = day+"-"+("0"+tempmonth)+"-"+year;

//        FromDate.setText(date);
//        ToDate.setText(date);
        txtdate.setText(date);
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,day);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        afterdate = dateFormat.format(c.getTime());
        txtdate.setText(afterdate);
        FromDate.setText(afterdate);
        ToDate.setText(afterdate);
    }
    public DateDialog(View view){
        txtdate=(EditText) view;
        FromDate=(EditText)view;
        ToDate =(EditText)view;
        this.selectvalue=selectvalue;
        time =(EditText)view;
    }
}
