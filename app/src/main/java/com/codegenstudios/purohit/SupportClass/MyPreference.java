package com.codegenstudios.purohit.SupportClass;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.codegenstudios.purohit.Activity.SignUpActivity;
import com.codegenstudios.purohit.Activity.SplashActivity;
import com.codegenstudios.purohit.R;

import static android.content.Context.MODE_PRIVATE;

public class MyPreference {

    private Context context;
    private AlertDialog dialog;

private  String maritalStatus="maritalstatus";
private  String dateofbirth="dateofbirth";
private  String familymember="familymember";
private  String placeofbirth="placeofbirth";
private  String timeofbirth="timeofbirth";
private  String vedhas="vedhas";
    private String MY_PREFS_NAME = "MyPrefsFile", AccessToken = null, DeviceId = null, MY_LOCATION = "LocationAccessToken";
    SharedPreferences mPrefs;
    SharedPreferences.Editor prefsEditor ;
    public MyPreference(Context context) {
        this.context = context;
      mPrefs=  context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        prefsEditor= mPrefs.edit();
    }


    public String[] getDefaultRunTimeValue() {
        String[] Value = new String[17];
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("AccessToken", null);
        if (restoredText != null) {
            Value[0] = prefs.getString("AccessToken", "no data");//"No AccessToken Defined" is the default value.
            Value[1] = prefs.getString("DeviceId", "no data");//"No DeviceId Defined" is the default value.
            Value[2] = prefs.getString("AccessType", "Blank Value");
            Value[3] = prefs.getString("ProfilePic", "");
            Value[4] = prefs.getString("MoblieNumber", "");
            Value[5] = prefs.getString("Password", "");
            Value[6] = prefs.getString("CheckBoxTrue", "0");
            Value[7] = prefs.getString("FacebookLogout", "fblogout");


        }
        return Value;
    }

    public String getLocationAccessToken() {
        String Value = null;
        SharedPreferences prefs = context.getSharedPreferences(MY_LOCATION, MODE_PRIVATE);
        String restoredText = prefs.getString("AccessToken", null);
        if (restoredText != null) {
            Value = prefs.getString("AccessToken", "No AccessToken Defined");//"No AccessToken Defined" is the default value.
        }
        return Value;
    }

    public final boolean isInternetOn() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                // Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                //Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }

    public void setAllBookingDetails(Context mContext,
                                     String saveAlldetails) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("save", saveAlldetails);
        prefsEditor.commit();
    }

    public String getAllBookingDetails(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String inactiveAccounts = mPrefs.getString("save", "");
        return inactiveAccounts;
    }

    public void setImage(Context mContext,
                         String image) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("image", image);
        prefsEditor.commit();
    }

    public String getImage(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString("image", "");
        return image;
    }

    public void setmaritalstatus(Context mContext,
                         String maritalstatus) {
        prefsEditor.putString(maritalStatus, maritalstatus);
        prefsEditor.commit();
    }

    public String getmaritalstatus(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(maritalStatus, "");
        return image;
    }

    public void setdateofbirth(Context mContext,
                                 String dob) {
        prefsEditor.putString(dateofbirth, dob);
        prefsEditor.commit();
    }

    public String getdateofbirth(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(dateofbirth, "");
        return image;
    }
    //timeofbirth
    public void setTimeOfBirth(Context mContext,
                               String tob) {
        prefsEditor.putString(timeofbirth, tob);
        prefsEditor.commit();
    }

    public String getTimeOfBirth(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(timeofbirth, "");
        return image;
    }
//end timeofbirth

    //POB
    public void setPlaceOfBirth(Context mContext,
                               String pob) {
        prefsEditor.putString(placeofbirth, pob);
        prefsEditor.commit();
    }

    public String getPlaceOfBirth(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(placeofbirth, "");
        return image;
    }
//end POB

//familymember
public void setFamilyMember(Context mContext,
                            String family) {
    prefsEditor.putString(familymember, family);
    prefsEditor.commit();
}

    public String getFamilyMember(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(familymember, "");
        return image;
    }
//end familymember

    //vedhas
    public void setVedhas(Context mContext,
                                String Vedhas) {
        prefsEditor.putString(vedhas, Vedhas);
        prefsEditor.commit();
    }

    public String getVedhas(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String image = mPrefs.getString(vedhas, "");
        return image;
    }
    //End vedhas













    public boolean checkEditTextEmpty(EditText editText) {
        return editText.getText().toString().trim().length() > 0;
    }

    public boolean isValidEmail(EditText editText) {
        return Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches();
    }

    public void ShowDialog(final Context context, String Title, String message) {
        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);

        TextviewSmallTitle1.setText(message);
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(Title);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog = imageDialog.create();
        dialog.show();
    }

    public void ShowGif(final Context context) {
        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
        imageDialog.setView(dialogView);
        dialog = imageDialog.create();
        dialog.show();
    }


    public void ShowSuccessDialog(Context context, String Title, String message) {

        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
        TextviewSmallTitle1.setText(message);
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(Title);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = imageDialog.create();
        dialog.show();

    }

//    final Dialog dialognew = new Dialog(SignUpActivity.this);
//                                                dialognew.setContentView(R.layout.custom_dialog_success);
//    Button dialogButtonnew = (Button) dialognew.findViewById(R.id.ButtonOk);
//    TextView TextviewBigTitlenew = (TextView)dialognew.findViewById(R.id.TextviewBigTitle);
//                                                TextviewBigTitlenew.setText("Success");
//    TextView TextviewSmallTitle1new = (TextView)dialognew.findViewById(R.id.TextviewSmallTitle1);
//                                                TextviewSmallTitle1new.setText(""+response.message());
//                                                dialogButtonnew.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            dialognew.dismiss();
//        }
//    });
//                                                dialognew.show();
}
