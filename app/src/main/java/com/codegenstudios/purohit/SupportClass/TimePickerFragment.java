package com.codegenstudios.purohit.SupportClass;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import com.codegenstudios.purohit.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

@SuppressLint("ValidFragment")
public  class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener  {
    EditText EditTextTime;
    int hour_x,minute_x;
    String am_pm;

    public TimePickerFragment(View view) {
        EditTextTime = (EditText) view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker

        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, false);
    }
/*
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        hour_x = hourOfDay % 12;
        if (hour_x == 0)
            hour_x = 12;

//        hour_x = hourOfDay;
//        minute_x = minute;
//        EditTextTime.setText(hour_x+":"+minute_x);


        String hour = String.valueOf(((hourOfDay > 12) ? hourOfDay % 12 : hourOfDay));
        String min = String.valueOf((minute < 10 ? ("0" + minute) : minute));
        am_pm = ((hourOfDay >= 12) ? "PM" : "AM");
        EditTextTime.setText(hour+":"+min+" "+am_pm);
        hour_x = Integer.parseInt(hour);
        minute_x= Integer.parseInt(min);
    }
*/
public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    // Do something with the time chosen by the user
    int a= hourOfDay;
    int b= minute;
    String startTime= String.valueOf(a)+":"+String.valueOf(b)+":00";
    StringTokenizer tk = new StringTokenizer(startTime);
    String time = tk.nextToken();

    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
    Date dt;

    try {
        dt = sdf.parse(time);
//                System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
        EditTextTime.setText(sdfs.format(dt));

    } catch (ParseException e) {
        e.printStackTrace();
    }


}

}
