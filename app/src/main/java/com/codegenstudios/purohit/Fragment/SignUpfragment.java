package com.codegenstudios.purohit.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.SignUpActivity;
import com.codegenstudios.purohit.Activity.SignupConfirmViewActivity;
import com.codegenstudios.purohit.Adapter.PlaceArrayAdapter;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class SignUpfragment extends Fragment implements View.OnClickListener {


    @BindView(R.id.text_view)
    TextView text_view;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextEmailId)
    EditText EditTextEmailId;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextNatchathiram)
    EditText EditTextNatchathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.FrameLayout1)
    FrameLayout FrameLayout1;
    @BindView(R.id.TextViewBackLoginPage)
    TextView TextViewBackLoginPage;

    @BindView(R.id.ButtonProfilePic)
    Button ButtonProfilePic;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.closebtn)
    Button closebtn;
    @BindView(R.id.ButtonCreateAccount)
    Button ButtonCreateAccount;
    @BindView(R.id.RelativelayoutImageView)
    RelativeLayout RelativelayoutImageView;

    @BindView(R.id.EditsubSection)
    EditText EditsubSection;
    String userChoosenTask, encodedImage, FacebookID, Facebookemail, FacebookName, GmailpersonName, GmailEmail, GmailId, FilePath = null, TempUri, error;
    private Uri currentImageUri;
    Uri tempUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog;
    Bitmap thumbnail = null, bitmap;
    File dir = new File(Environment.getExternalStorageDirectory() + "/PurohitUserApp/.Images");
    File file;
    File camimage;
    @BindView(R.id.ImageViewGoogleLoginButton)
    ImageView ImageViewGoogleLoginButton;
    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile", encodedImagecheck;
    private ProgressDialog progressBar;
    String EmailIDsaved, EmailNameSaved, EmailIDToken, SocialType;
    LoginButton loginButton;
    @BindView(R.id.ImageViewFacebookButton)
    ImageView ImageViewFacebookButton;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    GoogleApiClient mGoogleApiClientPlace;
    private Target mTarget;
    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private int REQUEST_PERMISSION_SETTING = 111;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final String LOG_TAG = "SignUpActdivity";

    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private AutoCompleteTextView mAutocompleteTextView;
    int position;
    private TextView textView;
    View view;

    public static Fragment getInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        SignUpfragment tabFragment = new SignUpfragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myPreference = new MyPreference(getActivity());
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("ImageUriPath", null);
        editor.apply();

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        if (position == 0) {
            view = inflater.inflate(R.layout.activity_sign_up, container, false);
            ButterKnife.bind(this, view);
            ButtonProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectImage();


                }
            });
            closebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageViewProfilePic.setImageBitmap(null);
                    closebtn.setVisibility(View.GONE);
                    RelativelayoutImageView.setVisibility(View.GONE);
                    camimage = null;
                    currentImageUri = null;
                    FilePath = null;
                }
            });


            EditTextAadhar1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (EditTextAadhar1.getText().toString().length() == 4) {
                        EditTextAadhar2.requestFocus();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            try {
                PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                        "com.codegenstudios.purohit",
                        PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.d("com.example.fishe", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {

            } catch (NoSuchAlgorithmException e) {

            }


            TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("AccessToken", null);
                    editor.putString("DeviceId", null);
                    startActivity(intent1);
                }
            });

            EditTextAadhar2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (EditTextAadhar2.getText().toString().length() == 4) {
                        EditTextAadhar3.requestFocus();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() == 0) {
                        EditTextAadhar1.requestFocus();
                    }
                }
            });
            EditTextAadhar3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() == 0) {
                        EditTextAadhar2.requestFocus();
                    }

                }
            });
            //this is changing the keyboard has done or tick icon ready to submit
            EditTextAadhar3.setImeOptions(EditorInfo.IME_ACTION_DONE);


            ButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String AadhaNumber = EditTextAadhar1.getText().toString() + EditTextAadhar2.getText().toString() + EditTextAadhar3.getText().toString();
                    Log.i("AadhaNumber", "" + AadhaNumber);


                    if (!Check(EditTextUserName)) {
                        EditTextUserName.setError("Enter User Name");
                        EditTextUserName.setFocusable(true);

                    } else if (!Check(EditTextMoblieNumber)) {

                        EditTextMoblieNumber.setError("Enter Valid 10 digit  Mobile Number");
                        EditTextMoblieNumber.setFocusable(true);

                    } else if (!isMobValid(EditTextMoblieNumber.getText().toString())) {

                        EditTextMoblieNumber.setError("Enter Valid 10 digit  Mobile Number");
                        EditTextMoblieNumber.setFocusable(true);

                    } else if (!Check(EditTextEmailId)) {
                        EditTextEmailId.setError("Enter  EmailID");
                        EditTextEmailId.setFocusable(true);

                    } else if (!isValidEmail(EditTextEmailId.getText().toString())) {
                        EditTextEmailId.setError("Enter Valid EmailID");
                        EditTextEmailId.setFocusable(true);

                    } else if (!Check(EditTextPassword)) {
                        EditTextPassword.setError("Enter Password");
                        EditTextPassword.setFocusable(true);
                    } else if (!isValidPassword(EditTextPassword.getText().toString().trim())) {
                        EditTextPassword.setError("Password lenght must have alleast 8 character, one special character, one uppercase character, one lowercase character and number !!");
                        EditTextPassword.setFocusable(true);
                    } else if (!Check(EditTextNatchathiram)) {
                        EditTextNatchathiram.setError("Enter Natchathiram");

                    } else if (!Check(EditTextGothram)) {
                        EditTextGothram.setError("Enter Gothram");

                    } else if (!Check(EditTextSection)) {
                        EditTextSection.setError("Enter Your caste");
                    } else if (!Check(EditsubSection)) {
                        EditsubSection.setError("Enter Sub-caste");

                    } else if (!Check(EditTextLanguage)) {
                        EditTextLanguage.setError("Enter Password");

                    } else {
                        Intent intentConfirm = new Intent(getActivity(), SignupConfirmViewActivity.class);
                        //SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        intentConfirm.putExtra("UserName", EditTextUserName.getText().toString());
                        intentConfirm.putExtra("UserEmailID", EditTextEmailId.getText().toString());
                        intentConfirm.putExtra("UserMoblieNumber", EditTextMoblieNumber.getText().toString());
                        intentConfirm.putExtra("UserPassword", EditTextPassword.getText().toString());
                        intentConfirm.putExtra("UserNatchtathiram", EditTextNatchathiram.getText().toString());
                        intentConfirm.putExtra("UserGothram", EditTextGothram.getText().toString());
                        intentConfirm.putExtra("UserSection", EditTextSection.getText().toString());
                        intentConfirm.putExtra("UserLanguage", EditTextLanguage.getText().toString());
                        intentConfirm.putExtra("UserAadhar", AadhaNumber);
                        if (FilePath != null) {
                            intentConfirm.putExtra("ImageUriPath", FilePath);
                        } else {
                            intentConfirm.putExtra("ImageUriPath", (Bundle) null);
                        }
                        intentConfirm.putExtra("EmailID", EmailIDsaved);
                        intentConfirm.putExtra("EmailIDUnique", EmailIDToken);
                        intentConfirm.putExtra("SocialType", SocialType);
                        intentConfirm.putExtra("IntentPassing", "SignupPage");
                        startActivity(intentConfirm);
                    }


                }
            });


        } /*else if (position == 1) {
            // ButterKnife.bind(this, view);
            //view = inflater.inflate(R.layout.activity_splash, container, false);
        } else {
            // ButterKnife.bind(this, view);
            view = inflater.inflate(R.layout.activity_persionl_detail, container, false);
            *//*TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*//*
        }*/

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    String convertstring(EditText txtEdit) {
        if (txtEdit.getText().toString().isEmpty()) {
            return "";
        } else {
            return txtEdit.getText().toString();
        }
    }

    //moblie number validations
    public static boolean isMobValid(String s) {
        Pattern p = Pattern.compile("[0-9]{10}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidEmail(String email) {
        String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void CamLoadImage(Bitmap thumbnail, Uri currentImageUri) {//camera
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
        /*Picasso.with(SignUpActivity.this)
                .load(new File(FilePath))
                .into(ImageViewProfilePic);*/
        Picasso.with(getContext()).load(new File(FilePath)).transform(new SignUpActivity.SquareTransform()).into(ImageViewProfilePic);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset", "" + data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                //handlerResult(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                if (currentImageUri != null) {
                    try {
                        FilePath = currentImageUri.getPath();
                        ;
                        CamLoadImage(BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(currentImageUri)), currentImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave", "data got from gallery ");

                    FilePath = getRealPathFromURI(getActivity(), currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if (imageFileFilter.accept(camimage)) {
                        Log.i("GalleryImageLoaded", "GalleryImageLoaded");
                        LoadImage(currentImageUri);
                    } else {
                        Toast.makeText(getActivity(), "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (checkPermissions()) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final String[] value = getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    boolean result= Utility.checkPermission(SignUpActivity.this);
                    userChoosenTask = "Take Photo";

                    alertDialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    dir.mkdirs();

                    if (dir.isDirectory()) {
                        Log.i("FileCreated", "FileCreated");

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        camimage = new File(dir, "PurhoitAPP" + timeStamp + ".jpg");
                        currentImageUri = Uri.fromFile(camimage);//file
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    } else {
                        Log.i("FileNotCreated", "FileNotCreated");
                    }


                } else if (position == 1) {

                    userChoosenTask = "Choose from Library";
//                    boolean result= Utility.checkPermission(SignUpActivity.this);


                    alertDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                    Log.i("onsave", "going to gallery ");

                } else if (position == 2) {
                    alertDialog.dismiss();
                    userChoosenTask = "Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void LoadImage(Uri thumbnail) {//gallery
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
        Picasso.with(getActivity())
                .load(thumbnail)
                .into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        Log.i("onsave", " viewwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (checkPermissions()) {
                selectImage();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

}





