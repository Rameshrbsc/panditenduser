package com.codegenstudios.purohit.Fragment;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.codegenstudios.purohit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBookingFragment extends Fragment implements TabLayout.OnTabSelectedListener{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Bundle bundle ;

    public MyBookingFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_booking2, container, false);
        getActivity().setTitle("Mybookings");
        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Confirmed"));
        tabLayout.addTab(tabLayout.newTab().setText("Pending "));
        tabLayout.addTab(tabLayout.newTab().setText("Canceled "));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        int white= Color.parseColor("#FFFFFF");
        int themecolor= Color.parseColor("#F0F0F0");
        tabLayout.setTabTextColors(themecolor,white );
        tabLayout.setSelectedTabIndicatorHeight(4);
        tabLayout.setSelectedTabIndicatorColor(white);
        viewPager=(ViewPager)view.findViewById(R.id.viewpager);
        Pager adapter= new Pager(getActivity().getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setOnTabSelectedListener((TabLayout.OnTabSelectedListener) this);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Confirmed");
        tabLayout.getTabAt(1).setText("Pending");
        tabLayout.getTabAt(2).setText("Cancelled");
//
//        if(getArguments() != null){
//            String JsonTitle = getArguments().getString("JsonTitle");
//            if(JsonTitle.equals("Accepted")){
//                TabLayout.Tab tab = tabLayout.getTabAt(0);
//                tab.select();
//            }else if(JsonTitle.equals("Pending")){
//                TabLayout.Tab tab = tabLayout.getTabAt(1);
//                tab.select();
//
//            }
//        }
        return view;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override

    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class Pager extends FragmentStatePagerAdapter {


        int tabCount;

        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount=tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new BookingConfirmedFragment();
                case 1:
                    return new BookingPendingFragment();
                case 2:
                    return new BookingCancelledFragment();
                default:
                    return null;
            }
        }
        @Override
        public int getCount() {
            return tabCount;
        }
    }


}
