package com.codegenstudios.purohit.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.SignUpActivity;
import com.codegenstudios.purohit.Activity.SignupConfirmViewActivity;
import com.codegenstudios.purohit.Adapter.PlaceArrayAdapter;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class MapFragment extends Fragment implements View.OnClickListener {



    int position;
    private TextView textView;
    View view;

    public static Fragment getInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        MapFragment tabFragment = new MapFragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


            view = inflater.inflate(R.layout.content_location_searching_fragment, container, false);
            //ButterKnife.bind(this, view);

         /*else if (position == 1) {
            // ButterKnife.bind(this, view);
            //view = inflater.inflate(R.layout.activity_splash, container, false);
        } else {
            // ButterKnife.bind(this, view);
            view = inflater.inflate(R.layout.activity_persionl_detail, container, false);
            *//*TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*//*
        }*/

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    String convertstring(EditText txtEdit) {
        if (txtEdit.getText().toString().isEmpty()) {
            return "";
        } else {
            return txtEdit.getText().toString();
        }
    }

    //moblie number validations




    @Override
    public void onClick(View v) {

    }



}





