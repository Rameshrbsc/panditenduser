package com.codegenstudios.purohit.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Adapter.BookedCardAdapter;
import com.codegenstudios.purohit.Pojo.AllBookingListPojo;
import com.codegenstudios.purohit.Pojo.AllBookingListResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingConfirmedFragment extends Fragment {
    @BindView(R.id.BookingConfirmedListView)
    ListView BookingConfirmedListView;
    @BindView(R.id.GIFLinearLayout)
    LinearLayout GIFLinearLayout;
    MyPreference myPreference;
    private ProgressDialog progressBar;
    private List<AllBookingListResponsePojo> allBookingListPojos;
    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    public BookingConfirmedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_confirmed, container, false);

        myPreference = new MyPreference(getActivity());
        ButterKnife.bind(this, view);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(getActivity());
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);

                    API apiService = createServiceHeader(API.class);
                    Call<AllBookingListPojo> call = apiService.ALL_BOOKING_LIST_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<AllBookingListPojo>() {
                        @Override
                        public void onResponse(Call<AllBookingListPojo> call, Response<AllBookingListPojo> response) {

                            progressBar.dismiss();

                            switch (response.body().getCode()) {
                                case "200":
                                    allBookingListPojos = new ArrayList<>();
                                    for (int i = 0; i < response.body().getResponse().length; i++) {

                                        if (response.body().getResponse()[i].getStatus().equals("1") || response.body().getResponse()[i].getStatus().equals("4")) {
                                            Collections.addAll(allBookingListPojos, response.body().getResponse()[i]);
                                            if (allBookingListPojos.size() > 0) {
                                                BookedCardAdapter bookedCardAdapter = new BookedCardAdapter(getActivity(), allBookingListPojos,"confirm");
                                                BookingConfirmedListView.setAdapter(bookedCardAdapter);
                                                bookedCardAdapter.notifyDataSetChanged();
                                                pullToRefresh.setRefreshing(false);
                                                pullToRefresh.setColorSchemeColors(Color.BLUE, Color.YELLOW, Color.BLUE);
                                                BookingConfirmedListView.setVisibility(View.VISIBLE);
                                                GIFLinearLayout.setVisibility(View.GONE);
                                            } else {
                                                BookingConfirmedListView.setVisibility(View.GONE);
                                                GIFLinearLayout.setVisibility(View.VISIBLE);
                                                pullToRefresh.setRefreshing(false);
                                                pullToRefresh.setColorSchemeColors(Color.BLUE, Color.YELLOW, Color.BLUE);
                                            }
                                        }
                                    }

                                    break;
                                case "108":
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                    break;

                            }
                        }


                        @Override
                        public void onFailure(Call<AllBookingListPojo> call, Throwable t) {

                            progressBar.dismiss();
                            try {
                                myPreference.ShowDialog(getActivity(), "Oops", t.getMessage());

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else {
                    myPreference.ShowDialog(getActivity(), "Oops", "There is not internet connection");
                }
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);

            API apiService = createServiceHeader(API.class);
            Call<AllBookingListPojo> call = apiService.ALL_BOOKING_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<AllBookingListPojo>() {
                @Override
                public void onResponse(Call<AllBookingListPojo> call, Response<AllBookingListPojo> response) {

                    progressBar.dismiss();

                    switch (response.body().getCode()) {
                        case "200":
                            allBookingListPojos = new ArrayList<>();


                            for (int i = 0; i < response.body().getResponse().length; i++) {

                                if (response.body().getResponse()[i].getStatus().equals("1") || response.body().getResponse()[i].getStatus().equals("4")) {
//                                    collection.add(response.body().getResponse()[i]);
                                    Collections.addAll(allBookingListPojos, response.body().getResponse()[i]);
                                }
                            }
                            if (allBookingListPojos.size() > 0) {
//                            allBookingListPojos.addAll(collection);
                                BookedCardAdapter bookedCardAdapter = new BookedCardAdapter(getActivity(), allBookingListPojos,"confirm");
                                BookingConfirmedListView.setAdapter(bookedCardAdapter);
                                bookedCardAdapter.notifyDataSetChanged();
                                BookingConfirmedListView.setVisibility(View.VISIBLE);
                                GIFLinearLayout.setVisibility(View.GONE);
                            } else {
                                BookingConfirmedListView.setVisibility(View.GONE);
                                GIFLinearLayout.setVisibility(View.VISIBLE);

                            }

//

                            break;

                        case "108":
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());
                            break;

                    }
                }


                @Override
                public void onFailure(Call<AllBookingListPojo> call, Throwable t) {

                    progressBar.dismiss();
                    try {
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            myPreference.ShowDialog(getActivity(), "Oops", "There is not internet connection");
        }

    }
}
