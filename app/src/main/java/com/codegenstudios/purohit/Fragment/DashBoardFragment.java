package com.codegenstudios.purohit.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Adapter.DashboardAdapter;
import com.codegenstudios.purohit.Adapter.DashboardSubGridViewAdapter;
import com.codegenstudios.purohit.Pojo.DashboardPojo;
import com.codegenstudios.purohit.Pojo.DashboardResponsePojo;
import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFragment extends Fragment {
//    @BindView(R.id.RecyclerViewGirdView)
//    RecyclerView RecyclerViewGirdView;

    //BindListView
    @BindView(R.id.ListViewGirdView)
    ListView ListViewGirdView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    MyPreference myPreference;
    private ProgressDialog progressBar;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;
    public SliderLayout mDemoSlider;

    private List<DashboardResponsePojo> dashboardResponsePojosList;

    DashboardSubGridViewAdapter dashboardCatogoeryListAdapter;
    DashboardAdapter dashboardAdapter;


    public DashBoardFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board, container, false);
        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);
        // bind the view using butterknife
        ButterKnife.bind(this, view);
        getActivity().setTitle("Dashboard");
        myPreference = new MyPreference(getActivity());

        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("God is universal", R.drawable.saraswathi);
        file_maps.put("Happy Ganesh Chaturthi ", R.drawable.one);
        file_maps.put("When Shiva beats his Damru", R.drawable.siva);
        file_maps.put("When Shiva beats his Damru", R.drawable.icon3);


        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getContext());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);


            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* MediaPlayer mp;
                //String filename = "android.resource://" + this.getPackageName() + "/raw/test0";


                mp=MediaPlayer.create(getActivity(), getActivity().getResources().getIdentifier("bell","raw",getActivity().getPackageName()));
                mp.start();*/


            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(getActivity());
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    Log.i("getRamesh", jsonObject.toString());
                    API apiService = createServiceHeader(API.class);
                    Call<DashboardPojo> call = apiService.DASHBOARD_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<DashboardPojo>() {
                        @Override
                        public void onResponse(Call<DashboardPojo> call, Response<DashboardPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    dashboardResponsePojosList = new ArrayList<>();
                                    Collections.addAll(dashboardResponsePojosList, response.body().getResponse());

                                    dashboardCatogoeryListAdapter = new DashboardSubGridViewAdapter(getContext(), dashboardResponsePojosList);

                                    dashboardAdapter=new DashboardAdapter(getContext(),dashboardResponsePojosList);
                                    GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1);
                                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setAdapter(dashboardAdapter);


                                    ListViewGirdView.setAdapter(dashboardCatogoeryListAdapter);
                                    dashboardCatogoeryListAdapter.notifyDataSetChanged();
                                    pullToRefresh.setRefreshing(false);
                                    break;

                                case "108":
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(getActivity(),"Oops",response.body().getMessage());
                                    break;
                            }
                        }
                        @Override
                        public void onFailure(Call<DashboardPojo> call, Throwable t) {
                            progressBar.dismiss();
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    myPreference.ShowDialog(getActivity(),"Oops","There is not internet connection");
                }



            }
        });
        return  view;
    }


    @Override
    public void onResume() {
        super.onResume();


        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            API apiService = createServiceHeader(API.class);
            Call<DashboardPojo> call = apiService.DASHBOARD_POJO_CALL(jsonObject);
            call.enqueue(new Callback<DashboardPojo>() {
                @Override
                public void onResponse(Call<DashboardPojo> call, Response<DashboardPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            Log.i("", "" + response.body().getMessage());

                            Log.i("", "" + response.body().getResponse());

                            dashboardResponsePojosList = new ArrayList<>();

                            Collections.addAll(dashboardResponsePojosList, response.body().getResponse());
                            dashboardCatogoeryListAdapter = new DashboardSubGridViewAdapter(getContext(), dashboardResponsePojosList);
                            ListViewGirdView.setAdapter(dashboardCatogoeryListAdapter);
                            dashboardCatogoeryListAdapter.notifyDataSetChanged();

                            dashboardAdapter=new DashboardAdapter(getContext(),dashboardResponsePojosList);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setAdapter(dashboardAdapter);

                            break;
                        case "300":

                            break;

                        case "108":
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(),"Oops",response.body().getMessage());
                            break;
                    }

                }

                @Override
                public void onFailure(Call<DashboardPojo> call, Throwable t) {
                    progressBar.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            myPreference.ShowDialog(getActivity(),"Oops","There is not internet connection");
        }
    }

    @Override
    public void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    private void dismissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }



}
