package com.codegenstudios.purohit.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.UserNavigationActivity;
import com.codegenstudios.purohit.Adapter.BookedCardAdapter;
import com.codegenstudios.purohit.Pojo.AllBookingListPojo;
import com.codegenstudios.purohit.Pojo.NewChangedPasswordPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Activity.SignUpActivity.isValidPassword;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.EditTextUserNewPassword)
    EditText EditTextUserNewPassword;
    @BindView(R.id.EditTextConfirmNewPassword)
    EditText EditTextConfirmNewPassword;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    Unbinder unbinder;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        getActivity().setTitle("ChangePassword");
        unbinder = ButterKnife.bind(this, view);
        myPreference = new MyPreference(getActivity());
        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (myPreference.isInternetOn()) {
                    if (isValidPassword(EditTextUserNewPassword.getText().toString().trim())) {
                        if (EditTextUserNewPassword.getText().toString().equals(EditTextConfirmNewPassword.getText().toString())) {
                            ButtonSubmit.setVisibility(View.GONE);
                            progressBar = new ProgressDialog(getActivity());
                            progressBar.setCancelable(true);
                            progressBar.setMessage("Processing ...");
                            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressBar.setProgress(0);
                            progressBar.setMax(100);
                            progressBar.show();

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                            jsonObject.addProperty("NewPassword", EditTextConfirmNewPassword.getText().toString());
                            API apiService = createServiceHeader(API.class);
                            Call<NewChangedPasswordPojo> call = apiService.NEW_CHANGED_PASSWORD_POJO_CALL(jsonObject);
                            call.enqueue(new Callback<NewChangedPasswordPojo>() {
                                @Override
                                public void onResponse(Call<NewChangedPasswordPojo> call, Response<NewChangedPasswordPojo> response) {
                                    ButtonSubmit.setVisibility(View.VISIBLE);
                                    progressBar.dismiss();
                                    switch (response.body().getCode()) {
                                        case "200":
//                                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getActivity(), UserNavigationActivity.class);
                                            startActivity(intent);
                                            break;
                                        case "108":
                                            Intent intentemg = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intentemg);
                                            break;

                                        default:
                                            myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());

                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Call<NewChangedPasswordPojo> call, Throwable t) {
                                    progressBar.dismiss();
                                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            });
                        } else {
                            myPreference.ShowDialog(getActivity(), "Oops", "password does not match!!");
                        }
                    } else {
                        myPreference.ShowDialog(getActivity(), "Oops", "Password length must have atleast 8 character, one special character, one uppercase character, one lowercase character and number !!");
                    }
                } else {
                    myPreference.ShowDialog(getActivity(), "Oops", "There is no internet connection");
                }
            }

        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {


                    DashBoardFragment fragment = new DashBoardFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, fragment)
                            .addToBackStack(null)
                            .commit();
                    fragmentManager.executePendingTransactions();



               /*     MainHomeFragment mainHomeFragment = new SupplierHomeFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction =
                            getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, mainHomeFragment);
                    fragmentTransaction.commit();*/

                    return true;
                }
                return false;
            }
        });

    }
}
