package com.codegenstudios.purohit.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.BookingCardListActivity;
import com.codegenstudios.purohit.Activity.BookingFormActivity;
import com.codegenstudios.purohit.Activity.FullScreenImageViewActivity;
import com.codegenstudios.purohit.Activity.LocationSearchingActivity;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.MapSeacrchingActivity;
import com.codegenstudios.purohit.Activity.UserProfileEditActivity;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileViewFragment extends Fragment {
    MyPreference myPreference;
    private ProgressDialog progressBar;
    Unbinder unbinder;
    UserProfileViewResponsePojo userProfileViewResponsePojo;
    String ImageURL, TempAccessToken;
    @BindView(R.id.TextViewUserName)
    TextView TextViewUserName;
    @BindView(R.id.TextViewUserEmailID)
    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewUserNatchtathiram)
    TextView TextViewUserNatchtathiram;
    @BindView(R.id.TextViewUserGothram)
    TextView TextViewUserGothram;
    @BindView(R.id.TextViewUserMoblieNumber)
    TextView TextViewUserMoblieNumber;
    @BindView(R.id.TextViewUserSection)
    TextView TextViewUserSection;
    @BindView(R.id.TextViewUserAadharNumber)
    TextView TextViewUserAadharNumber;
    @BindView(R.id.TextViewUserLanguage)
    TextView TextViewUserLanguage;
    @BindView(R.id.ImageViewProfilePic)
    android.widget.ImageView ImageViewProfilePic;
    String MY_PREFS_NAME = "MyPrefsFile", ImageFullView;
    @BindView(R.id.ButtonEdit)
    Button ButtonEdit;
    private GoogleApiClient mGoogleApiClient;


    public UserProfileViewFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_profile_view, container, false);
        unbinder = ButterKnife.bind(this, v);
        myPreference = new MyPreference(getActivity());
//        setHasOptionsMenu(true);
        getActivity().setTitle("ProfileView");
        try {
            Intent intent = Objects.requireNonNull(getActivity()).getIntent();
            TempAccessToken = intent.getExtras().get("TempAccessToken").toString();
            if (TempAccessToken != null) {
                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("AccessToken", TempAccessToken);
                editor.apply();
            } else {
                Log.i("AcceesTokenProfile", "" + myPreference.getDefaultRunTimeValue()[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(getActivity(), FullScreenImageViewActivity.class);
                    intent.putExtra("Path", ImageFullView);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UserProfileEditActivity.class);
                intent.putExtra("UserName", TextViewUserName.getText().toString());
                intent.putExtra("UserEmailID", TextViewUserEmailID.getText().toString());
                intent.putExtra("UserNatchtathiram", TextViewUserNatchtathiram.getText().toString());
                intent.putExtra("UserGothram", TextViewUserGothram.getText().toString());
                intent.putExtra("UserMoblieNumber", TextViewUserMoblieNumber.getText().toString());
                intent.putExtra("UserSection", TextViewUserSection.getText().toString());
                intent.putExtra("UserAadhar", TextViewUserAadharNumber.getText().toString());
                intent.putExtra("UserLanguage", TextViewUserLanguage.getText().toString());
                intent.putExtra("UserPicURI", ImageURL);
                startActivity(intent);
            }
        });

        return v;
    }

    @Override
    public void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardFragment fragment = new DashBoardFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, fragment)
                            .addToBackStack(null)
                            .commit();
                    fragmentManager.executePendingTransactions();



               /*     MainHomeFragment mainHomeFragment = new SupplierHomeFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction =
                            getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, mainHomeFragment);
                    fragmentTransaction.commit();*/

                    return true;
                }
                return false;
            }
        });
        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Validating Your Account...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            API apiService = createServiceHeader(API.class);
            Call<UserProfileViewPojo> call = apiService.USER_PROFILE_VIEW_POJO_CALL(jsonObject);
            Log.e("JsonProfile", jsonObject.toString());
            call.enqueue(new Callback<UserProfileViewPojo>() {
                @Override
                public void onResponse(Call<UserProfileViewPojo> call, Response<UserProfileViewPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            userProfileViewResponsePojo = response.body().getResponse();
                            TextViewUserName.setText(userProfileViewResponsePojo.getUserName());
                            TextViewUserMoblieNumber.setText(userProfileViewResponsePojo.getUserPhone());
                            TextViewUserEmailID.setText(userProfileViewResponsePojo.getUserEmail());
                            TextViewUserSection.setText(userProfileViewResponsePojo.getUserSection());
                            TextViewUserNatchtathiram.setText(userProfileViewResponsePojo.getUserNatchatram());
                            TextViewUserGothram.setText(userProfileViewResponsePojo.getUserGothram());
                            TextViewUserAadharNumber.setText(userProfileViewResponsePojo.getUserAadharNumber());
                            //TODO : Need to add language text after getting it from API
                            TextViewUserLanguage.setText(userProfileViewResponsePojo.getLanguage());
                            Log.i("Image", "" + userProfileViewResponsePojo.getUserProfilePic());
                            ImageURL = userProfileViewResponsePojo.getUserProfileThumb();
                            ImageFullView = userProfileViewResponsePojo.getUserProfilePic();
                            Picasso.with(getActivity()).load(ImageURL).transform(new CircleTransform()).into(ImageViewProfilePic);
                            break;
                        case "108":
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        default:
                            myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());

                    }

                }

                @Override
                public void onFailure(Call<UserProfileViewPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            myPreference.ShowDialog(getActivity(), "Oops", "There is not internet connection");

        }

    }

    @Override
    public void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    private void dismissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }


}
