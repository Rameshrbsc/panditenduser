package com.codegenstudios.purohit.Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.DateDialog;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.EditTextDate)
    EditText EditTextDate;

    int position;
    private TextView textView;
    View view;
    String dates = "";
    MyPreference myPreference;

    public static Fragment getInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        PersonalFragment tabFragment = new PersonalFragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.personal_details, container, false);
        myPreference=new MyPreference(getActivity());
        ButterKnife.bind(this, view);

         /*else if (position == 1) {
            // ButterKnife.bind(this, view);
            //view = inflater.inflate(R.layout.activity_splash, container, false);
        } else {
            // ButterKnife.bind(this, view);
            view = inflater.inflate(R.layout.activity_persionl_detail, container, false);
            *//*TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*//*
        }*/


        EditTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");*/

               /* DateDialog dialog = new DateDialog(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "DatePicker");*/

            }
        });
       /* if (dates == null) {
            EditTextDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
        }*/
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @SuppressLint("ValidFragment")
    public  class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }
        public void populateSetDate(int year, int month, int day) {
            EditTextDate.setText(month+"/"+day+"/"+year);

        }


    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    String convertstring(EditText txtEdit) {
        if (txtEdit.getText().toString().isEmpty()) {
            return "";
        } else {
            return txtEdit.getText().toString();
        }
    }

    //moblie number validations


    @Override
    public void onClick(View v) {

    }


}





