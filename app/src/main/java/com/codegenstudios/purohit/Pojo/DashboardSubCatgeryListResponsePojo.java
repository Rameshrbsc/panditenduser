package com.codegenstudios.purohit.Pojo;

import java.io.Serializable;

public class DashboardSubCatgeryListResponsePojo implements Serializable {
    private String SubCatogeryPic;

    private String SubCatogeryId;

    private String SubCatogeryPicthumbnail;

    private String SubCatogeryName;

    public String getSubCatogeryPic ()
    {
        return SubCatogeryPic;
    }

    public void setSubCatogeryPic (String SubCatogeryPic)
    {
        this.SubCatogeryPic = SubCatogeryPic;
    }

    public String getSubCatogeryId ()
    {
        return SubCatogeryId;
    }

    public void setSubCatogeryId (String SubCatogeryId)
    {
        this.SubCatogeryId = SubCatogeryId;
    }

    public String getSubCatogeryPicthumbnail ()
    {
        return SubCatogeryPicthumbnail;
    }

    public void setSubCatogeryPicthumbnail (String SubCatogeryPicthumbnail)
    {
        this.SubCatogeryPicthumbnail = SubCatogeryPicthumbnail;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SubCatogeryPic = "+SubCatogeryPic+", SubCatogeryId = "+SubCatogeryId+", SubCatogeryPicthumbnail = "+SubCatogeryPicthumbnail+", SubCatogeryName = "+SubCatogeryName+"]";
    }
}
