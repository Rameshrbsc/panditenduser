package com.codegenstudios.purohit.Pojo;

public class NotificationSingleBookingIdPojo {
    private String message;

    private NotificationSingleBookingIdResponsePojo[] BookingDetails;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public NotificationSingleBookingIdResponsePojo[] getBookingDetails ()
    {
        return BookingDetails;
    }

    public void setBookingDetails (NotificationSingleBookingIdResponsePojo[] BookingDetails)
    {
        this.BookingDetails = BookingDetails;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", BookingDetails = "+BookingDetails+", code = "+code+"]";
    }
}
