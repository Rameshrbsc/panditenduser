package com.codegenstudios.purohit.Pojo;

public class SubCatogeryViewAllPojo {

    private String message;

    private SubCatogeryViewAllResponsePojo[] Response;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public SubCatogeryViewAllResponsePojo[] getResponse ()
    {
        return Response;
    }

    public void setResponse (SubCatogeryViewAllResponsePojo[] Response)
    {
        this.Response = Response;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", Response = "+Response+", code = "+code+"]";
    }
}
