package com.codegenstudios.purohit.Pojo;

public class ResendOTPPojo {

    private String message;

    private String code;

    private String otp;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", code = "+code+", otp = "+otp+"]";
    }
}
