package com.codegenstudios.purohit.Pojo;

public class UserProfileViewPojo {

    private UserProfileViewResponsePojo response;

    private String message;

    private String code;

    public UserProfileViewResponsePojo getResponse ()
    {
        return response;
    }

    public void setResponse (UserProfileViewResponsePojo response)
    {
        this.response = response;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [response = "+response+", message = "+message+", code = "+code+"]";
    }
}
