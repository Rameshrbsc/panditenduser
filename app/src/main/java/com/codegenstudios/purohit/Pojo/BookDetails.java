package com.codegenstudios.purohit.Pojo;

public class BookDetails {
    String BookingName;
    String BookingEmailID;
    String BookingMoblieNumber;
    String BookingAmount;
    String BookingLanguage;
    String BookingAddress;
    String Bookinglat;
    String Bookinglong;
    String BookingPoojaId;
    String BookingPoojaName;
    String BookingCatogeryId;
    String BookingCatogeryName;
    String BookingSubCatogeryId;
    String BookingSubCatogeryName;
    String BookingDate;
    String BookingTime;

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(String bookingName) {
        BookingName = bookingName;
    }

    public String getBookingEmailID() {
        return BookingEmailID;
    }

    public void setBookingEmailID(String bookingEmailID) {
        BookingEmailID = bookingEmailID;
    }

    public String getBookingMoblieNumber() {
        return BookingMoblieNumber;
    }

    public void setBookingMoblieNumber(String bookingMoblieNumber) {
        BookingMoblieNumber = bookingMoblieNumber;
    }

    public String getBookingAmount() {
        return BookingAmount;
    }

    public void setBookingAmount(String bookingAmount) {
        BookingAmount = bookingAmount;
    }

    public String getBookingLanguage() {
        return BookingLanguage;
    }

    public void setBookingLanguage(String bookingLanguage) {
        BookingLanguage = bookingLanguage;
    }

    public String getBookingAddress() {
        return BookingAddress;
    }

    public void setBookingAddress(String bookingAddress) {
        BookingAddress = bookingAddress;
    }

    public String getBookinglat() {
        return Bookinglat;
    }

    public void setBookinglat(String bookinglat) {
        Bookinglat = bookinglat;
    }

    public String getBookinglong() {
        return Bookinglong;
    }

    public void setBookinglong(String bookinglong) {
        Bookinglong = bookinglong;
    }

    public String getBookingPoojaId() {
        return BookingPoojaId;
    }

    public void setBookingPoojaId(String bookingPoojaId) {
        BookingPoojaId = bookingPoojaId;
    }

    public String getBookingPoojaName() {
        return BookingPoojaName;
    }

    public void setBookingPoojaName(String bookingPoojaName) {
        BookingPoojaName = bookingPoojaName;
    }

    public String getBookingCatogeryId() {
        return BookingCatogeryId;
    }

    public void setBookingCatogeryId(String bookingCatogeryId) {
        BookingCatogeryId = bookingCatogeryId;
    }

    public String getBookingCatogeryName() {
        return BookingCatogeryName;
    }

    public void setBookingCatogeryName(String bookingCatogeryName) {
        BookingCatogeryName = bookingCatogeryName;
    }

    public String getBookingSubCatogeryId() {
        return BookingSubCatogeryId;
    }

    public void setBookingSubCatogeryId(String bookingSubCatogeryId) {
        BookingSubCatogeryId = bookingSubCatogeryId;
    }

    public String getBookingSubCatogeryName() {
        return BookingSubCatogeryName;
    }

    public void setBookingSubCatogeryName(String bookingSubCatogeryName) {
        BookingSubCatogeryName = bookingSubCatogeryName;
    }

    public String getBookingDate() {
        return BookingDate;
    }

    public void setBookingDate(String bookingDate) {
        BookingDate = bookingDate;
    }

    public String getBookingTime() {
        return BookingTime;
    }

    public void setBookingTime(String bookingTime) {
        BookingTime = bookingTime;
    }
}
