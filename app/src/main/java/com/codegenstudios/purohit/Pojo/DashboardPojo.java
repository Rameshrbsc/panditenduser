package com.codegenstudios.purohit.Pojo;

import java.io.Serializable;

public class DashboardPojo implements Serializable {

    private String message;

    private DashboardResponsePojo[] Response;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public DashboardResponsePojo[] getResponse ()
    {
        return Response;
    }

    public void setResponse (DashboardResponsePojo[] Response)
    {
        this.Response = Response;
    }

    public String getCode ()
    {
        return code;
    }


    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", Response = "+Response+", code = "+code+"]";
    }
}
