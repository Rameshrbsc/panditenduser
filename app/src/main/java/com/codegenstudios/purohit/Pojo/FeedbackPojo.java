package com.codegenstudios.purohit.Pojo;

/**
 * Created by One Booking System on 15/4/2018.
 */

public class FeedbackPojo {

    String key;
    String date;
    String name;
    String content;
    String profilepic;
    String fivestars;
    String ratings;



    public FeedbackPojo(String fivestars) {
        this.fivestars = fivestars;

    }
    public FeedbackPojo(String date, String name, String content, String profilepic, String key, String ratings) {


        this.key = key;
        this.date = date;
        this.name = name;
        this.content = content;
        this.profilepic = profilepic;
        this.ratings = ratings;

    }

    public String getKey() {
        return key;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }
    public String getProfilepic() {
        return profilepic;
    }
    public String getRatings() {
        return ratings;
    }


}

