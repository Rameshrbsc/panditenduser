package com.codegenstudios.purohit.Pojo;

import java.io.Serializable;

public class DashboardResponsePojo implements Serializable {

    private String CatogeryName;

    private DashboardSubCatgeryListResponsePojo[] SubCatgeryListResponse;

    private String CatogeryId;

    public String getCatogeryName() {
        return CatogeryName;
    }

    public void setCatogeryName(String CatogeryName) {
        this.CatogeryName = CatogeryName;
    }

    public DashboardSubCatgeryListResponsePojo[] getSubCatgeryListResponse() {
        return SubCatgeryListResponse;
    }

    public void setSubCatgeryListResponse(DashboardSubCatgeryListResponsePojo[] SubCatgeryListResponse) {
        this.SubCatgeryListResponse = SubCatgeryListResponse;
    }

    public String getCatogeryId() {
        return CatogeryId;
    }

    public void setCatogeryId(String CatogeryId) {
        this.CatogeryId = CatogeryId;
    }

    @Override
    public String toString() {
        return "ClassPojo [CatogeryName = " + CatogeryName + ", SubCatgeryListResponse = " + SubCatgeryListResponse + ", CatogeryId = " + CatogeryId + "]";
    }

}
