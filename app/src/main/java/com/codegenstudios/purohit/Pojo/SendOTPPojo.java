package com.codegenstudios.purohit.Pojo;

public class SendOTPPojo {
    private String message;

    private String code;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    private String NotificationCount;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", code = "+code+"]";
    }
}
