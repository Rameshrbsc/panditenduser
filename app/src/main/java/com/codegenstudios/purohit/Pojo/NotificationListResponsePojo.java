package com.codegenstudios.purohit.Pojo;

public class NotificationListResponsePojo {

    private String SenderType;

    private String RegardingId;

    private String body;

    private String NotificationID;

    private String title;

    private String ReadNotification;

    private String RegardingType;

    private String Image;

    private String SenderID;

    private String Regarding;

    private String Timestamp;

    public String getSenderType ()
    {
        return SenderType;
    }

    public void setSenderType (String SenderType)
    {
        this.SenderType = SenderType;
    }

    public String getRegardingId ()
    {
        return RegardingId;
    }

    public void setRegardingId (String RegardingId)
    {
        this.RegardingId = RegardingId;
    }

    public String getBody ()
    {
        return body;
    }

    public void setBody (String body)
    {
        this.body = body;
    }

    public String getNotificationID ()
    {
        return NotificationID;
    }

    public void setNotificationID (String NotificationID)
    {
        this.NotificationID = NotificationID;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getReadNotification ()
    {
        return ReadNotification;
    }

    public void setReadNotification (String ReadNotification)
    {
        this.ReadNotification = ReadNotification;
    }

    public String getRegardingType ()
    {
        return RegardingType;
    }

    public void setRegardingType (String RegardingType)
    {
        this.RegardingType = RegardingType;
    }

    public String getImage ()
    {
        return Image;
    }

    public void setImage (String Image)
    {
        this.Image = Image;
    }

    public String getSenderID ()
    {
        return SenderID;
    }

    public void setSenderID (String SenderID)
    {
        this.SenderID = SenderID;
    }

    public String getRegarding ()
    {
        return Regarding;
    }

    public void setRegarding (String Regarding)
    {
        this.Regarding = Regarding;
    }

    public String getTimestamp ()
    {
        return Timestamp;
    }

    public void setTimestamp (String Timestamp)
    {
        this.Timestamp = Timestamp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SenderType = "+SenderType+", RegardingId = "+RegardingId+", body = "+body+", NotificationID = "+NotificationID+", title = "+title+", ReadNotification = "+ReadNotification+", RegardingType = "+RegardingType+", Image = "+Image+", SenderID = "+SenderID+", Regarding = "+Regarding+", Timestamp = "+Timestamp+"]";
    }
}
