package com.codegenstudios.purohit.Pojo;

public class NotificationListPojo {
    private String message;

    private String TotalPage;

    private NotificationListResponsePojo[] Response;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getTotalPage ()
    {
        return TotalPage;
    }

    public void setTotalPage (String TotalPage)
    {
        this.TotalPage = TotalPage;
    }

    public NotificationListResponsePojo[] getResponse ()
    {
        return Response;
    }

    public void setResponse (NotificationListResponsePojo[] Response)
    {
        this.Response = Response;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", TotalPage = "+TotalPage+", Response = "+Response+", code = "+code+"]";
    }
}
