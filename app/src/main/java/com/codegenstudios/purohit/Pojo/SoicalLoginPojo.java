package com.codegenstudios.purohit.Pojo;

public class SoicalLoginPojo {

    private String message;

    private String id;

    private String DeviceId;

    private String accessToken;

    private String name;

    private String code;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    private String NotificationCount ;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDeviceId ()
    {
        return DeviceId;
    }

    public void setDeviceId (String DeviceId)
    {
        this.DeviceId = DeviceId;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", id = "+id+", DeviceId = "+DeviceId+", accessToken = "+accessToken+", name = "+name+", code = "+code+"]";
    }

}
