package com.codegenstudios.purohit.Pojo;

public class PoojaListResponsePojo {

    private String PoojaPicthumbnail;

    private String Amount;

    private String PoojaId;

    private String PoojaDescription;

    private String PoojaName;

    public String getPoojaPicthumbnail ()
    {
        return PoojaPicthumbnail;
    }

    public void setPoojaPicthumbnail (String PoojaPicthumbnail)
    {
        this.PoojaPicthumbnail = PoojaPicthumbnail;
    }

    public String getAmount ()
    {
        return Amount;
    }

    public void setAmount (String Amount)
    {
        this.Amount = Amount;
    }

    public String getPoojaId ()
    {
        return PoojaId;
    }

    public void setPoojaId (String PoojaId)
    {
        this.PoojaId = PoojaId;
    }

    public String getPoojaDescription ()
    {
        return PoojaDescription;
    }

    public void setPoojaDescription (String PoojaDescription)
    {
        this.PoojaDescription = PoojaDescription;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PoojaPicthumbnail = "+PoojaPicthumbnail+", Amount = "+Amount+", PoojaId = "+PoojaId+", PoojaDescription = "+PoojaDescription+", PoojaName = "+PoojaName+"]";
    }
}
