package com.codegenstudios.purohit.Pojo;

import android.net.Uri;

public class SubCatogeryViewAllResponsePojo {

    private String SubCatogeryId;

    private String SubCatogeryPicthumbnail;

    private String SubCatogeryName;

    public String getSubCatogeryId ()
    {
        return SubCatogeryId;
    }

    public void setSubCatogeryId (String SubCatogeryId)
    {
        this.SubCatogeryId = SubCatogeryId;
    }

    public String getSubCatogeryPicthumbnail ()
    {
        return SubCatogeryPicthumbnail;
    }

    public void setSubCatogeryPicthumbnail (String SubCatogeryPicthumbnail)
    {
        this.SubCatogeryPicthumbnail = SubCatogeryPicthumbnail;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SubCatogeryId = "+SubCatogeryId+", SubCatogeryPicthumbnail = "+SubCatogeryPicthumbnail+", SubCatogeryName = "+SubCatogeryName+"]";
    }
}
