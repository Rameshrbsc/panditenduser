package com.codegenstudios.purohit.Pojo;

public class NotificationSingleBookingIdResponsePojo {
    private String UserPhoneNumber;

    private String UserImage;

    private String BookingDate;

    private String booking_user_id;

    private String UserName;

    private String PurohitPhoneNumber;

    private String SubmitDate;

    private String Time;

    private String LocationName;

    private String SubCatogeryName;

    private String Latitude;

    private String Feedback;

    private String TrackMe;

    private String UserEmailID;

    private String PurohitName;

    private String Amount;

    private String BookingId;

    private String CatogeryName;

    private String Status;

    private String booking_purohit_id;

    private String Rating;

    private String PurohitImage;

    private String Longitude;

    private String Language;

    private String PoojaName;

    private String PoojaImage;

    public String getUserPhoneNumber ()
    {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber (String UserPhoneNumber)
    {
        this.UserPhoneNumber = UserPhoneNumber;
    }

    public String getUserImage ()
    {
        return UserImage;
    }

    public void setUserImage (String UserImage)
    {
        this.UserImage = UserImage;
    }

    public String getBookingDate ()
    {
        return BookingDate;
    }

    public void setBookingDate (String BookingDate)
    {
        this.BookingDate = BookingDate;
    }

    public String getBooking_user_id ()
    {
        return booking_user_id;
    }

    public void setBooking_user_id (String booking_user_id)
    {
        this.booking_user_id = booking_user_id;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getPurohitPhoneNumber ()
    {
        return PurohitPhoneNumber;
    }

    public void setPurohitPhoneNumber (String PurohitPhoneNumber)
    {
        this.PurohitPhoneNumber = PurohitPhoneNumber;
    }

    public String getSubmitDate ()
    {
        return SubmitDate;
    }

    public void setSubmitDate (String SubmitDate)
    {
        this.SubmitDate = SubmitDate;
    }

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getLocationName ()
    {
        return LocationName;
    }

    public void setLocationName (String LocationName)
    {
        this.LocationName = LocationName;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getFeedback ()
    {
        return Feedback;
    }

    public void setFeedback (String Feedback)
    {
        this.Feedback = Feedback;
    }

    public String getTrackMe ()
    {
        return TrackMe;
    }

    public void setTrackMe (String TrackMe)
    {
        this.TrackMe = TrackMe;
    }

    public String getUserEmailID ()
    {
        return UserEmailID;
    }

    public void setUserEmailID (String UserEmailID)
    {
        this.UserEmailID = UserEmailID;
    }

    public String getPurohitName ()
    {
        return PurohitName;
    }

    public void setPurohitName (String PurohitName)
    {
        this.PurohitName = PurohitName;
    }

    public String getAmount ()
    {
        return Amount;
    }

    public void setAmount (String Amount)
    {
        this.Amount = Amount;
    }

    public String getBookingId ()
    {
        return BookingId;
    }

    public void setBookingId (String BookingId)
    {
        this.BookingId = BookingId;
    }

    public String getCatogeryName ()
    {
        return CatogeryName;
    }

    public void setCatogeryName (String CatogeryName)
    {
        this.CatogeryName = CatogeryName;
    }

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getBooking_purohit_id ()
    {
        return booking_purohit_id;
    }

    public void setBooking_purohit_id (String booking_purohit_id)
    {
        this.booking_purohit_id = booking_purohit_id;
    }

    public String getRating ()
    {
        return Rating;
    }

    public void setRating (String Rating)
    {
        this.Rating = Rating;
    }

    public String getPurohitImage ()
    {
        return PurohitImage;
    }

    public void setPurohitImage (String PurohitImage)
    {
        this.PurohitImage = PurohitImage;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getLanguage ()
    {
        return Language;
    }

    public void setLanguage (String Language)
    {
        this.Language = Language;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    public String getPoojaImage ()
    {
        return PoojaImage;
    }

    public void setPoojaImage (String PoojaImage)
    {
        this.PoojaImage = PoojaImage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserPhoneNumber = "+UserPhoneNumber+", UserImage = "+UserImage+", BookingDate = "+BookingDate+", booking_user_id = "+booking_user_id+", UserName = "+UserName+", PurohitPhoneNumber = "+PurohitPhoneNumber+", SubmitDate = "+SubmitDate+", Time = "+Time+", LocationName = "+LocationName+", SubCatogeryName = "+SubCatogeryName+", Latitude = "+Latitude+", Feedback = "+Feedback+", TrackMe = "+TrackMe+", UserEmailID = "+UserEmailID+", PurohitName = "+PurohitName+", Amount = "+Amount+", BookingId = "+BookingId+", CatogeryName = "+CatogeryName+", Status = "+Status+", booking_purohit_id = "+booking_purohit_id+", Rating = "+Rating+", PurohitImage = "+PurohitImage+", Longitude = "+Longitude+", Language = "+Language+", PoojaName = "+PoojaName+", PoojaImage = "+PoojaImage+"]";
    }
}
