package com.codegenstudios.purohit.Pojo;

public class SignUpPojo {
    private String message;

    private String AccessToken;

    private String code;

    private String otp;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    private String NotificationCount;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getAccessToken ()
    {
        return AccessToken;
    }

    public void setAccessToken (String AccessToken)
    {
        this.AccessToken = AccessToken;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", AccessToken = "+AccessToken+", code = "+code+", otp = "+otp+"]";
    }
}
