package com.codegenstudios.purohit.Pojo;

public class BookingFormSubmitPojo {

    private String message;

    private String reason;
    private String bookingId;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getBookingId ()
    {
        return bookingId;
    }

    public void setBookingId (String bookingId)
    {
        this.bookingId = bookingId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", reason = "+reason+",bookingId = "+bookingId+", code = "+code+"]";
    }
}
