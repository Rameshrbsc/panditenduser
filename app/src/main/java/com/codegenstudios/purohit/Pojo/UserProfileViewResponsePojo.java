package com.codegenstudios.purohit.Pojo;

public class UserProfileViewResponsePojo {
    private String UserProfilePic;

    private String UserAccessToken;

    private String UserNatchatram;

    private String UserImageName;

    private String UserName;

    private String UserEmail;

    private String UserProfileThumb;

    private String UserAadharNumber;

    private String UserGothram;

    private String UserSection;

    private String UserPhone;
    private String Language;

    public String getUserProfilePic ()
    {
        return UserProfilePic;
    }

    public void setUserProfilePic (String UserProfilePic)
    {
        this.UserProfilePic = UserProfilePic;
    }

    public String getUserAccessToken ()
    {
        return UserAccessToken;
    }

    public void setUserAccessToken (String UserAccessToken)
    {
        this.UserAccessToken = UserAccessToken;
    }

    public String getUserNatchatram ()
    {
        return UserNatchatram;
    }

    public void setUserNatchatram (String UserNatchatram)
    {
        this.UserNatchatram = UserNatchatram;
    }

    public String getUserImageName ()
    {
        return UserImageName;
    }

    public void setUserImageName (String UserImageName)
    {
        this.UserImageName = UserImageName;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getUserEmail ()
    {
        return UserEmail;
    }

    public void setUserEmail (String UserEmail)
    {
        this.UserEmail = UserEmail;
    }

    public String getUserProfileThumb ()
    {
        return UserProfileThumb;
    }

    public void setUserProfileThumb (String UserProfileThumb)
    {
        this.UserProfileThumb = UserProfileThumb;
    }

    public String getUserAadharNumber ()
    {
        return UserAadharNumber;
    }

    public void setUserAadharNumber (String UserAadharNumber)
    {
        this.UserAadharNumber = UserAadharNumber;
    }

    public String getUserGothram ()
    {
        return UserGothram;
    }

    public void setUserGothram (String UserGothram)
    {
        this.UserGothram = UserGothram;
    }

    public String getUserSection ()
    {
        return UserSection;
    }

    public void setUserSection (String UserSection)
    {
        this.UserSection = UserSection;
    }

    public String getUserPhone ()
    {
        return UserPhone;
    }

    public void setUserPhone (String UserPhone)
    {
        this.UserPhone = UserPhone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserProfilePic = "+UserProfilePic+", UserAccessToken = "+UserAccessToken+", UserNatchatram = "+UserNatchatram+", UserImageName = "+UserImageName+", UserName = "+UserName+", UserEmail = "+UserEmail+", UserProfileThumb = "+UserProfileThumb+", UserAadharNumber = "+UserAadharNumber+", UserGothram = "+UserGothram+", UserSection = "+UserSection+", UserPhone = "+UserPhone+"]";
    }

    public String                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }
}
