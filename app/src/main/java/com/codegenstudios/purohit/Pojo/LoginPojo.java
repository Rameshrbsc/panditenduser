package com.codegenstudios.purohit.Pojo;

public class LoginPojo {

    private String message;

    private String id;

    private String accessToken;

    private String name;

    private String code;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    private String NotificationCount;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", id = "+id+", accessToken = "+accessToken+", name = "+name+", code = "+code+"]";
    }
}
