package com.codegenstudios.purohit.Pojo;

public class ForgetPasswordPojo {
    private String message;

    private String AccessToken;

    private String NotificationCount;

    private String code;

    private String otp;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getAccessToken ()
    {
        return AccessToken;
    }

    public void setAccessToken (String AccessToken)
    {
        this.AccessToken = AccessToken;
    }

    public String getNotificationCount ()
    {
        return NotificationCount;
    }

    public void setNotificationCount (String NotificationCount)
    {
        this.NotificationCount = NotificationCount;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", AccessToken = "+AccessToken+", NotificationCount = "+NotificationCount+", code = "+code+", otp = "+otp+"]";
    }
}
