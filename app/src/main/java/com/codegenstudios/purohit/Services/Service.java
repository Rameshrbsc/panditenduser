package com.codegenstudios.purohit.Services;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service {
    //liveip http://104.196.156.62/purohitapi/  http://192.168.1.10/projects/purohitapi/

    //new live ip http://35.237.76.77/

    //private static final String BASE_URL = "http://purohitapi.sribarati.com/";
   // private static final String BASE_URL = "http://35.200.134.213/purohitapi/";
    //private static final String BASE_URL = "http://192.168.0.130/purohitapi/";
    private static final String BASE_URL = "http://35.200.134.213/purohitapi/";

    private static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/";

    private static final OkHttpClient httpClient = new OkHttpClient();

    private static final Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()));

    private static final Retrofit.Builder GoogleBuilder =
            new Retrofit.Builder()
                    .baseUrl(GOOGLE_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static  <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = GoogleBuilder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }


    public static  <S> S createServiceHeader(Class<S> serviceClass) {

        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();

                                // Request customization: add request headers
                                Request.Builder requestBuilder = original.newBuilder()
                                        .addHeader("UserName","SriBarati")
                                        .addHeader("Password","ItTeam")
                                        .addHeader("Content-Type","application/json");
//                                        .method(original.method(), original.body());

                                Request request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        })
                .build();

        Retrofit retrofit = builder.client(okClient).build();
        return retrofit.create(serviceClass);
    }

}