package com.codegenstudios.purohit.NotificationService;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.MapTrackingPurohitActivity;
import com.codegenstudios.purohit.Activity.SplashActivity;
import com.codegenstudios.purohit.Activity.SupportTrackingReceiver;
import com.codegenstudios.purohit.Fragment.BookingConfirmedFragment;
import com.codegenstudios.purohit.Fragment.MyBookingFragment;
import com.codegenstudios.purohit.R;

import org.json.JSONException;
import org.json.JSONObject;

import me.leolin.shortcutbadger.ShortcutBadger;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String MY_PREFS_NAME = "MyPrefsFile";
    public static String  NOTIFICATION_COUNT = "NotificationCount";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getMessageId());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }

    }


    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            JSONObject data = json.getJSONObject("message");

            String title = data.get("title").toString();
            String message = data.get("body").toString();
            String Regarding = data.get("Regarding").toString();
            String RegardingId = data.get("RegardingId").toString();
            String NotificationId = data.get("NotificationId").toString();
            String SenderID = data.get("SenderID").toString();
            String NotificationCount = data.get("NotificationCount").toString();

            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt("NotificationCount", Integer.parseInt(NotificationCount));
            Log.i("NotificationCount",NotificationCount);
            editor.apply();

            Intent pushNotification = new Intent(NOTIFICATION_COUNT);
            pushNotification.putExtra("NotificationCount",NotificationCount );
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(pushNotification);


            String Image = data.get("Image").toString();
            String timestamp = data.get("Timestamp").toString();
            sendNotification(title, message, timestamp, Image,Regarding,RegardingId,NotificationId);


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void sendNotification(String title, String message, String timestamp, String image, String Regarding, String RegardingId,String NotificationId ) {

        if(Regarding.equals("1")){
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.putExtra("IntentPassing","FcmIntentPassing");
            intent.putExtra("JsonRegarding",Regarding);
            intent.putExtra("JsonRegardingId",RegardingId);
            intent.putExtra("JsonNotificationId",NotificationId);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationMessage(getApplicationContext(),title,message,timestamp,image,intent,NotificationId);
        }else if(Regarding.equals("2")){
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.putExtra("IntentPassing","FcmIntentPassing");
            intent.putExtra("JsonRegarding",Regarding);
            intent.putExtra("JsonRegardingId",RegardingId);
            intent.putExtra("JsonNotificationId",NotificationId);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationMessage(getApplicationContext(),title,message,timestamp,image,intent,NotificationId);
        }else if(Regarding.equals("")){
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
//            intent.putExtra("JsonNotificationId",NotificationId);
            intent.putExtra("JsonRegarding",Regarding);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationMessage(getApplicationContext(),title,message,timestamp,image,intent,NotificationId);
        }


    }
}

