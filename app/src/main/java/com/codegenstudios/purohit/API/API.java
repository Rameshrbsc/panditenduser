package com.codegenstudios.purohit.API;

import com.google.gson.JsonObject;
import com.codegenstudios.purohit.Pojo.AllBookingListPojo;
import com.codegenstudios.purohit.Pojo.BookingFormSubmitPojo;
import com.codegenstudios.purohit.Pojo.DashboardPojo;
import com.codegenstudios.purohit.Pojo.ForgetPasswordPojo;
import com.codegenstudios.purohit.Pojo.LoginPojo;
import com.codegenstudios.purohit.Pojo.NewChangedPasswordPojo;
import com.codegenstudios.purohit.Pojo.NotificaitonReadSubmitPojo;
import com.codegenstudios.purohit.Pojo.NotificationListPojo;
import com.codegenstudios.purohit.Pojo.NotificationSingleBookingIdPojo;
import com.codegenstudios.purohit.Pojo.PoojaListPojo;
import com.codegenstudios.purohit.Pojo.RatingFeedbackPojo;
import com.codegenstudios.purohit.Pojo.ResendOTPPojo;
import com.codegenstudios.purohit.Pojo.SendOTPPojo;
import com.codegenstudios.purohit.Pojo.SignUpPojo;
import com.codegenstudios.purohit.Pojo.SoicalLoginPojo;
import com.codegenstudios.purohit.Pojo.SubCatogeryViewAllPojo;
import com.codegenstudios.purohit.Pojo.TrackingPurohitLatLongPojo;
import com.codegenstudios.purohit.Pojo.TransactionSuccessSubmitPojo;
import com.codegenstudios.purohit.Pojo.UserProfileUpdatePojo;
import com.codegenstudios.purohit.Pojo.UserProfileViewPojo;
import com.codegenstudios.purohit.Pojo.UserSessionAuthPojo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface API {
    //userSignup
    @POST("User/Register")
    Call<SignUpPojo> SIGN_UP_POJO_CALL(@Body JsonObject requestParameter);
    //userAuth
    //userSignup
    @POST("User/userSessionAuth")
    Call<UserSessionAuthPojo> USER_SESSION_AUTH_POJO_CALL(@Body JsonObject requestParameter);
    //Login
    @POST("User/login")
    Call<LoginPojo> LOGIN_POJO_CALL(@Body JsonObject requestParameter);
    //SocialLogin
    @POST("User/socialLogin")
    Call<SoicalLoginPojo> SOICAL_LOGIN_POJO_CALL(@Body JsonObject requestParameter);
//ResendOTP
    @POST("User/resentOtp")
    Call<ResendOTPPojo> RESEND_OTP_POJO_CALL(@Body JsonObject requestParameter);
    //SendOTP
    @POST("User/validateOtp")
    Call<SendOTPPojo> SEND_OTP_POJO_CALL(@Body JsonObject requestParameter);
    @POST("User/profileView")
    Call<UserProfileViewPojo> USER_PROFILE_VIEW_POJO_CALL(@Body JsonObject requestParameter);
    @POST("User/profileUpdate")
    Call<UserProfileUpdatePojo> USER_PROFILE_UPDATE_POJO_CALL(@Body JsonObject requestParameter);
    //ForgetPassword
    @POST("User/forgotPassword")
    Call<ForgetPasswordPojo> FORGET_PASSWORD_POJO_CALL(@Body JsonObject requestParameter);
    //newpassword
    @POST("User/changePassword")
    Call<NewChangedPasswordPojo> NEW_CHANGED_PASSWORD_POJO_CALL(@Body JsonObject requestParameter);
    //Dashboard
    @POST("Booking/getCatSubCatList")
    Call<DashboardPojo> DASHBOARD_POJO_CALL(@Body JsonObject requestParameter);
    //ViewAll
    @POST("Booking/getSubCategories")
    Call<SubCatogeryViewAllPojo> SUB_CATOGERY_VIEW_ALL_POJO_CALL(@Body JsonObject requestParameter);
    //BookingPoojaList
    @POST("Booking/getPoojaList")
    Call<PoojaListPojo> POOJA_LIST_POJO_CALL(@Body JsonObject requestParameter);
    //BookingFormSubmit
    @POST("Booking/poojaBooking")
    Call<BookingFormSubmitPojo> BOOKING_FORM_SUBMIT_POJO_CALL(@Body JsonObject requestParameter);
    //BookingFormSubmitTransactionsuccess
    @POST("Booking/updateBookingStatus")
    Call<TransactionSuccessSubmitPojo> TRANSACTION_SUCCESS_SUBMIT_POJO_CALL(@Body JsonObject requestParameter);
    //BookingFormSubmitTransactionsuccess
    @POST("Booking/getBookingDetails")
    Call<AllBookingListPojo> ALL_BOOKING_LIST_POJO_CALL(@Body JsonObject requestParameter);
    //RatingFeedback
    @POST("User/updateRating")
    Call<RatingFeedbackPojo> RATING_FEEDBACK_POJO_CALL(@Body JsonObject requestParameter);
    //NotificationList
    @POST("Notification/List")
    Call<NotificationListPojo> NOTIFICATION_LIST_POJO_CALL(@Body JsonObject requestParameter);
    //SingleBookingId
    @POST("Booking/getBookingDetailsWithId")
    Call<NotificationSingleBookingIdPojo> NOTIFICATION_SINGLE_BOOKING_ID_POJO_CALL(@Body JsonObject requestParameter);
    //Readnotificaitonstatuschanges
    @POST("Notification/ReadNotification")
    Call<NotificaitonReadSubmitPojo> NOTIFICAITON_READ_SUBMIT_POJO_CALL(@Body JsonObject requestParameter);
    //TrackingPurohit
    @POST("Track/TrackPurohit")
    Call<TrackingPurohitLatLongPojo> TRACKING_PUROHIT_LAT_LONG_POJO_CALL(@Body JsonObject requestParameter);


}
