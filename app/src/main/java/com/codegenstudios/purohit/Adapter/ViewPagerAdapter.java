package com.codegenstudios.purohit.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.codegenstudios.purohit.Fragment.BookingConfirmedFragment;
import com.codegenstudios.purohit.Fragment.MapFragment;
import com.codegenstudios.purohit.Fragment.MyBookingFragment;
import com.codegenstudios.purohit.Fragment.PersonalFragment;
import com.codegenstudios.purohit.Fragment.SignUpfragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String title[] = {"Basic Info", "Locality", "Personal Info"};

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if (position == 0) {
            fragment = SignUpfragment.getInstance(position);
        } else if(position==1) {
            fragment = MapFragment.getInstance(position);
        }else {
            fragment = PersonalFragment.getInstance(position);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
