package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.Activity.BookingCardListActivity;
import com.codegenstudios.purohit.Activity.DashboardViewallActivity;
import com.codegenstudios.purohit.Pojo.SubCatogeryViewAllResponsePojo;
import com.codegenstudios.purohit.R;

import java.util.List;

import retrofit2.http.Headers;

public class DashboardViewAllAdapter extends BaseAdapter {
    private Context context;
    ViewHolder holder;

    List<SubCatogeryViewAllResponsePojo> subCatogeryViewAllResponsePojos;
    String CatogeryId, CatogeryName;


    public DashboardViewAllAdapter(DashboardViewallActivity dashboardViewallActivity, List<SubCatogeryViewAllResponsePojo> subCatogeryViewAllResponsePojos, String CatogeryId, String CatogeryName) {
        this.context = dashboardViewallActivity;
        this.subCatogeryViewAllResponsePojos = subCatogeryViewAllResponsePojos;
        this.CatogeryName = CatogeryName;
        this.CatogeryId = CatogeryId;

    }

    private class ViewHolder {
        TextView TextViewSubCatogery;
        ImageView image;
        LinearLayout SubCatLayoutClick;
    }

    @Override
    public int getCount() {
        return subCatogeryViewAllResponsePojos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
//        if (view == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.custom_dashboard_cardview, parent, false);
        holder = new ViewHolder();
        holder.TextViewSubCatogery = (TextView) view.findViewById(R.id.TextViewSubCatogery);
        holder.image = (ImageView) view.findViewById(R.id.image);
        holder.SubCatLayoutClick = (LinearLayout) view.findViewById(R.id.SubCatLayoutClick);

//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }

        holder.TextViewSubCatogery.setText(subCatogeryViewAllResponsePojos.get(position).getSubCatogeryName());
        Picasso.with(context)
                .load(subCatogeryViewAllResponsePojos.get(position).getSubCatogeryPicthumbnail())
                .into(holder.image);
        holder.SubCatLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BookingCardListActivity.class);
                intent.putExtra("IntentPassing", "ViewAll");
                intent.putExtra("SubCatogeryId", subCatogeryViewAllResponsePojos.get(position).getSubCatogeryId());
                intent.putExtra("SubCatogeryName", subCatogeryViewAllResponsePojos.get(position).getSubCatogeryName());
                intent.putExtra("CatogeryName", CatogeryName);
                intent.putExtra("CatogeryId", CatogeryId);
                context.startActivity(intent);
            }
        });
        return view;
    }


}
