package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.Activity.BookingCardListActivity;
import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
import com.codegenstudios.purohit.R;

public class DashboardSubCardViewAdapter extends RecyclerView.Adapter<DashboardSubCardViewAdapter.ViewHolder> {

    Context context;
    String CatName, CatID;
    DashboardSubCatgeryListResponsePojo[] dashboardSubCatgeryListResponsePojos;


    DashboardSubCardViewAdapter(Context context, String CatName, String CatID, DashboardSubCatgeryListResponsePojo[] dashboardSubCatgeryListResponsePojos) {
        this.context = context;
        this.CatName = CatName;
        this.CatID = CatID;
        this.dashboardSubCatgeryListResponsePojos = dashboardSubCatgeryListResponsePojos;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewSubCatogery;
        ImageView image;
        LinearLayout linear;

        ViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            TextViewSubCatogery = (TextView) v.findViewById(R.id.TextViewSubCatogery);
            linear = (LinearLayout) v.findViewById(R.id.linear);

        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_dashboard_cardview, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        int size = dashboardSubCatgeryListResponsePojos.length;

        if (position < 6) {
            holder.TextViewSubCatogery.setText(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
            Picasso.with(context)
                    .load(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryPicthumbnail())
                    .into(holder.image);
        } else {

           // holder.linear.setVisibility(View.GONE);
            holder.TextViewSubCatogery.setVisibility(View.GONE);
            holder.image.setVisibility(View.GONE);

        }

        /* else{

            holder.TextViewSubCatogery.setText(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
            Picasso.with(context)
                    .load(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryPicthumbnail())
                    .into(holder.image);

        }*/

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BookingCardListActivity.class);
                intent.putExtra("IntentPassing", "DashboardDetails");
                intent.putExtra("CatogeryName", CatName);
                intent.putExtra("CatogeryId", CatID);
                intent.putExtra("Activity", "dash");
                intent.putExtra("SubCatogeryName", dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
                intent.putExtra("SubCatogeryId", dashboardSubCatgeryListResponsePojos[position].getSubCatogeryId());

                context.startActivity(intent);
            }
        });
        /*holder.TextViewSubCatogery.setText(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
        Picasso.with(context)
                        .load(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryPicthumbnail())
                        .into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BookingCardListActivity.class);
                    intent.putExtra("IntentPassing","DashboardDetails");
                        intent.putExtra("CatogeryName",CatName);
                        intent.putExtra("CatogeryId",CatID);
                        intent.putExtra("SubCatogeryName",dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
                        intent.putExtra("SubCatogeryId",dashboardSubCatgeryListResponsePojos[position].getSubCatogeryId());

                    context.startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return dashboardSubCatgeryListResponsePojos.length;
    }
}


//package com.codegenstudios.purohit.Adapter;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.CardView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.squareup.picasso.Picasso;
//import com.codegenstudios.purohit.Activity.BookingCardListActivity;
//import com.codegenstudios.purohit.Activity.DashboardViewallActivity;
//import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
//import com.codegenstudios.purohit.R;
//
//import java.util.List;
//
//    public class DashboardSubCardViewAdapter extends BaseAdapter {
//
//        Context context;
//        ViewHolder holder;
//        String CatName,CatID;
//        DashboardSubCatgeryListResponsePojo[] dashboardSubCatgeryListResponsePojos;
//
//
//
//
//        DashboardSubCardViewAdapter(Context context, String CatName, String CatID, DashboardSubCatgeryListResponsePojo[] dashboardSubCatgeryListResponsePojos) {
//            this.context = context;
//            this.CatName = CatName;
//            this.CatID = CatID;
//            this.dashboardSubCatgeryListResponsePojos = dashboardSubCatgeryListResponsePojos;
//        }
//
//
//        @Override
//        public int getCount() {
//
//            return dashboardSubCatgeryListResponsePojos.length;    }
//
//        @Override
//        public Object getItem(int position) {
//            return 0;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return 0;
//        }
//
//        @Override
//        public View getView(final int position, View view, ViewGroup parent) {
//            if (view == null) {
//                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//                view = inflater.inflate(R.layout.custom_dashboard_cardview, parent, false);
//                holder = new ViewHolder();
//                holder.TextViewSubCatogery = (TextView) view.findViewById(R.id.TextViewSubCatogery);
////                holder.IconCard = (CardView)view.findViewById(R.id.IconCard) ;
//                holder.image = (ImageView) view.findViewById(R.id.image);
//                holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
//
//                view.setTag(holder);
//                holder.TextViewSubCatogery.setText(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
//                Picasso.with(context)
//                        .load(dashboardSubCatgeryListResponsePojos[position].getSubCatogeryPicthumbnail())
//                        .into(holder.image);
//                holder.image.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                    Intent intent = new Intent(context, BookingCardListActivity.class);
//                    intent.putExtra("IntentPassing","DashboardDetails");
//                        intent.putExtra("CatogeryName",CatName);
//                        intent.putExtra("CatogeryId",CatID);
//                        intent.putExtra("SubCatogeryName",dashboardSubCatgeryListResponsePojos[position].getSubCatogeryName());
//                        intent.putExtra("SubCatogeryId",dashboardSubCatgeryListResponsePojos[position].getSubCatogeryId());
//
//                    context.startActivity(intent);
//
//                    }
//                });
//
//            }
//            return view;
//        }
//
//
//        private class ViewHolder {
//            TextView TextViewSubCatogery;
//            ImageView image;
//        }
//    }
//
