package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.NotificaitonListActivity;
import com.codegenstudios.purohit.Activity.NotificationFullViewActivity;
import com.codegenstudios.purohit.NotificationService.MyFirebaseMessagingService;
import com.codegenstudios.purohit.Pojo.NotificaitonReadSubmitPojo;
import com.codegenstudios.purohit.Pojo.NotificationListResponsePojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.util.List;

import javax.sql.DataSource;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.codegenstudios.purohit.Services.Service.createServiceHeader;


public class NotificationReadListAdapter extends RecyclerView.Adapter<NotificationReadListAdapter.MyViewHolder> {
    private Context context;
    private  List<NotificationListResponsePojo> notificationListResponsePojos;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";
    RefreshData refreshData;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView TextViewTitle, TextViewBodyMessage, TextViewTime,titlemessage;
        ImageView ImageIcon;
        CardView CardFullView;

        public MyViewHolder(View view) {
            super(view);
            TextViewTitle = (TextView) view.findViewById(R.id.TextViewTitle);
            TextViewBodyMessage  = (TextView)view.findViewById(R.id.TextViewBodyMessage);
            TextViewTime  = (TextView)view.findViewById(R.id.TextViewTime);
            ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);
            CardFullView =(CardView)view.findViewById(R.id.CardFullView);
            titlemessage = (TextView)view.findViewById(R.id.titlemessage);

        }
    }


    public NotificationReadListAdapter(NotificaitonListActivity notificaitonListActivity, List<NotificationListResponsePojo> notificationListResponsePojos,RefreshData refreshData) {
        this.context= notificaitonListActivity;
        this.notificationListResponsePojos = notificationListResponsePojos;
        myPreference = new MyPreference(context);
        this.refreshData = refreshData;
    }

    public  interface  RefreshData{
        void refreshData();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_card_notifications_read_list, parent, false);
         return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            if(notificationListResponsePojos.get(position).getReadNotification().equals("0")){
                holder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
                holder.TextViewTitle.setTextColor(Color.BLACK);
                holder.TextViewTitle.setTypeface(null, Typeface.BOLD);
                holder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
                holder.TextViewBodyMessage.setTextColor(Color.BLACK);
                holder.TextViewBodyMessage.setTypeface(null, Typeface.BOLD);
                holder.titlemessage.setTextColor(Color.BLACK);
                holder.titlemessage.setTypeface(null,Typeface.BOLD);

                holder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
            }else if(notificationListResponsePojos.get(position).getReadNotification().equals("1")){

                holder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
                holder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
                holder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
            }
            holder.CardFullView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(notificationListResponsePojos.get(position).getRegardingId().equals("")){//tap-on-onetouch
//                        Toast.makeText(context, "not value", Toast.LENGTH_SHORT).show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                        jsonObject.addProperty("SenderType","2");
                        jsonObject.addProperty("ReadNotification","1");
                        JsonArray array = new JsonArray();
                        array.add(notificationListResponsePojos.get(position).getNotificationID());
                        jsonObject.add("NotificaitonIdArray", array);
                        API apiService = createServiceHeader(API.class);
                        Call<NotificaitonReadSubmitPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<NotificaitonReadSubmitPojo>() {
                            @Override
                            public void onResponse(Call<NotificaitonReadSubmitPojo> call, Response<NotificaitonReadSubmitPojo> response) {
                                switch (response.body().getCode()) {
                                    case "200":

                                        SharedPreferences myPrefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);  //Activity1.class
                                        Integer NotificationCount = myPrefs.getInt("NotificationCount",0);

                                        if(NotificationCount > 0){
                                            SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editor.putInt("NotificationCount", NotificationCount - 1);
                                            editor.apply();
                                            Intent pushNotification = new Intent(MyFirebaseMessagingService.NOTIFICATION_COUNT);
                                            pushNotification.putExtra("NotificationCount",NotificationCount );
                                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(pushNotification);
                                            refreshData.refreshData();
                                            Log.e("Refeshcall","cal");
                                        }

                                        break;
                                    case "108":
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        context. startActivity(intent);
                                    default:
                                        myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<NotificaitonReadSubmitPojo> call, Throwable t) {
                                try {
                                    Toast.makeText(context, " Something went wrong ", Toast.LENGTH_SHORT).show();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });



                    }else{
                        /*Intent intent = new Intent(context, NotificationFullViewActivity.class);
                        intent.putExtra("NotificaitonBookingId",notificationListResponsePojos.get(position).getRegardingId());
                        intent.putExtra("NotificaitonId",notificationListResponsePojos.get(position).getNotificationID());
                        intent.putExtra("IntentPassing","NotificaitonReadListAdapter");
                        context.startActivity(intent);*/
                    }

                }
            });

        holder.setIsRecyclable(false);//stop the draw in recyclerview must put






    }

    @Override
    public int getItemCount() {
        return notificationListResponsePojos.size();
    }
    public  void adddate(List<NotificationListResponsePojo> pojos){
        for(NotificationListResponsePojo  pojos1 : pojos){
            notificationListResponsePojos.add(pojos1);
            notifyDataSetChanged();

        }
    }

}
















//public class NotificationReadListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    private Context context;
//    ILoadMore iLoadMore;
//    private final int VIEW_TYPE =0 ,VIEW_TYPE_LOADING =1;
//    private  List<NotificationListResponsePojo> notificationListResponsePojos;
//    int visiableThreadhold =5;
//    int lastVisiableItem,totalItemCount;
//    boolean isLoading;
//
//
//    public NotificationReadListAdapter(NotificaitonListActivity notificaitonListActivity, List<NotificationListResponsePojo> notificationListResponsePojos, RecyclerView recyclerView) {
//        this.context = notificaitonListActivity;
//        this.notificationListResponsePojos = notificationListResponsePojos;
//
//        final LinearLayoutManager linearLayoutManager  = (LinearLayoutManager)recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisiableItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
//                if(!isLoading && totalItemCount <= (lastVisiableItem +visiableThreadhold)){
//                    if(iLoadMore != null){
//                        iLoadMore.onLoadMore();
//                        isLoading = true;
//                    }
//
//                }
//            }
//        });
//    }
//    @Override
//    public int getItemViewType(int position) {
//        return notificationListResponsePojos.get(position) ==null ? VIEW_TYPE_LOADING : VIEW_TYPE;
//    }
//
//    public  void  setiLoadMore(ILoadMore iLoadMore){
//        this.iLoadMore = iLoadMore;
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        TextView TextViewTitle, TextViewBodyMessage, TextViewTime;
//        ImageView ImageIcon;
//        CardView CardFullView;
//
//        public MyViewHolder(View view) {
//            super(view);
//            TextViewTitle = (TextView) view.findViewById(R.id.TextViewTitle);
//            TextViewBodyMessage  = (TextView)view.findViewById(R.id.TextViewBodyMessage);
//            TextViewTime  = (TextView)view.findViewById(R.id.TextViewTime);
//            ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);
//            CardFullView =(CardView)view.findViewById(R.id.CardFullView);
//
//        }
//    }
//
//
//    public class LoadMore extends RecyclerView.ViewHolder {
//      ProgressBar progressBar;
//
//        public LoadMore(View view) {
//            super(view);
//            progressBar = (ProgressBar) view.findViewById(R.id.ProgressBar);
//
//        }
//    }
//
//
//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//
//        if(viewType == VIEW_TYPE){
//            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_card_notifications_read_list, parent, false);
//         return new MyViewHolder(itemView);
//        }else if(viewType == VIEW_TYPE_LOADING){
//            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_layout, parent, false);
//            return new LoadMore(itemView);
//
//        }
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
//            if(holder instanceof MyViewHolder){
//                MyViewHolder myViewHolder = (MyViewHolder)holder;
//                if(notificationListResponsePojos.get(position).getReadNotification().equals("0")){
//                    myViewHolder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
//                    myViewHolder.TextViewTitle.setTextColor(Color.BLACK);
//                    myViewHolder.TextViewTitle.setTypeface(null, Typeface.BOLD);
//                    myViewHolder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
//                    myViewHolder.TextViewBodyMessage.setTextColor(Color.BLACK);
//                    myViewHolder.TextViewBodyMessage.setTypeface(null, Typeface.BOLD);
//                    myViewHolder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
//                    myViewHolder.TextViewTime.setTextColor(Color.BLACK);
//                    myViewHolder.TextViewTime.setTypeface(null, Typeface.BOLD);
//            }else if(notificationListResponsePojos.get(position).getReadNotification().equals("1")){
//
//                    myViewHolder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
//                    myViewHolder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
//                    myViewHolder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
//            }
//                myViewHolder.CardFullView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(context, NotificationFullViewActivity.class);
//                    intent.putExtra("BookingId",notificationListResponsePojos.get(position).getRegardingId());
//                    context.startActivity(intent);
//                }
//            });
//
//            }
//            else if(holder instanceof LoadMore){
//                LoadMore loadMore = (LoadMore)holder;
//                loadMore.progressBar.setIndeterminate(true);
//            }
//    }
//
//    @Override
//    public int getItemCount() {
//        return notificationListResponsePojos.size();
//    }
//
//
//    public interface ILoadMore{
//        void onLoadMore();
//    }
//
//
//    public void setLoading(Boolean loading){
//        isLoading = false;
//    }
//}










//
//public class NotificationReadListAdapter extends BaseAdapter {
//    private Context context;
//    private ViewHolder holder;
//    private  List<NotificationListResponsePojo> notificationListResponsePojos;
//
//    public NotificationReadListAdapter(NotificaitonListActivity notificaitonListActivity, List<NotificationListResponsePojo> notificationListResponsePojos) {
//        this.context= notificaitonListActivity;
//        this.notificationListResponsePojos = notificationListResponsePojos;
//    }
//
//    @Override
//    public int getCount() {
//        return notificationListResponsePojos.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return notificationListResponsePojos.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//    private class ViewHolder {
//        TextView TextViewTitle, TextViewBodyMessage, TextViewTime;
//        ImageView ImageIcon;
//        CardView CardFullView;
//    }
//
//
//    @Override
//    public View getView(final int position, View view, ViewGroup viewGroup) {
//        if (view == null) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//            view = inflater.inflate(R.layout.custom_card_notifications_read_list, viewGroup, false);
//            holder = new ViewHolder();
//
//            holder.TextViewTitle  = (TextView)view.findViewById(R.id.TextViewTitle);
//            holder.TextViewBodyMessage  = (TextView)view.findViewById(R.id.TextViewBodyMessage);
//            holder.TextViewTime  = (TextView)view.findViewById(R.id.TextViewTime);
//            holder.ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);
//            holder.CardFullView =(CardView)view.findViewById(R.id.CardFullView);
//
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//        if(notificationListResponsePojos.get(position).getReadNotification().equals("0")){
//                holder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
//                holder.TextViewTitle.setTextColor(Color.BLACK);
//                holder.TextViewTitle.setTypeface(null, Typeface.BOLD);
//                holder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
//                holder.TextViewBodyMessage.setTextColor(Color.BLACK);
//                holder.TextViewBodyMessage.setTypeface(null, Typeface.BOLD);
//                holder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
//                holder.TextViewTime.setTextColor(Color.BLACK);
//                holder.TextViewTime.setTypeface(null, Typeface.BOLD);
//            }else if(notificationListResponsePojos.get(position).getReadNotification().equals("1")){
//
//                holder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
//                holder.TextViewBodyMessage.setText(notificationListResponsePojos.get(position).getBody());
//                holder.TextViewTime.setText(notificationListResponsePojos.get(position).getTimestamp());
//            }
//            holder.CardFullView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(context, NotificationFullViewActivity.class);
//                    intent.putExtra("BookingId",notificationListResponsePojos.get(position).getRegardingId());
//                    context.startActivity(intent);
//                }
//            });
//
//
//        return view;
//    }
//}









