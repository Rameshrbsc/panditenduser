package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.Activity.BookingCardListActivity;
import com.codegenstudios.purohit.Activity.ViewMoreActivty;
import com.codegenstudios.purohit.Pojo.DashboardResponsePojo;
import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
import com.codegenstudios.purohit.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    Context context;
    private DashboardSubGridViewAdapter.ViewHolder holder;
    private DashboardSubCardViewAdapter mAdapter;
    List<DashboardResponsePojo> dashboardResponsePojosList;
    DashboardResponsePojo dashboardResponsePojo;
    List<DashboardResponsePojo> all_thumbs = new ArrayList<DashboardResponsePojo>();


    public DashboardAdapter(Context context, List<DashboardResponsePojo> dashboardResponsePojosList) {
        this.context = context;
        this.dashboardResponsePojosList = dashboardResponsePojosList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;
        public TextView TextViewTitle, TextViewViewAll, more;

        ViewHolder(View v) {
            super(v);
            TextViewTitle = (TextView) v.findViewById(R.id.TextViewTitle);
            recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
            more = (TextView) v.findViewById(R.id.more);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_dashboard_inside_gridview, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        dashboardResponsePojo = new DashboardResponsePojo();
        holder.TextViewTitle.setText(dashboardResponsePojosList.get(position).getCatogeryName());
        mAdapter = new DashboardSubCardViewAdapter(context, dashboardResponsePojosList.get(position).getCatogeryName(),
                dashboardResponsePojosList.get(position).getCatogeryId(),
                dashboardResponsePojosList.get(position).getSubCatgeryListResponse());
        GridLayoutManager layoutManager = new GridLayoutManager(context, 1);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        GridLayoutManager manager = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(mAdapter);
        all_thumbs = dashboardResponsePojosList;
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(context, ViewMoreActivty.class);
                bundle.putSerializable("Valuue", (Serializable) all_thumbs);
                bundle.putSerializable("value", dashboardResponsePojo);
                bundle.putSerializable("Name", dashboardResponsePojosList.get(position).getCatogeryName());
                bundle.putString("code", dashboardResponsePojosList.get(position).getCatogeryId());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dashboardResponsePojosList.size();
    }
}