package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.Pojo.FeedbackPojo;
import com.codegenstudios.purohit.R;

import java.util.List;

/**
 * Created by One Booking System on 15/4/2018.
 */

public class Feedback_adapter extends RecyclerView.Adapter<Feedback_adapter.FeedbackHolder> {

    FeedbackPojo feedbackModel;
    private Context context;
    private List<FeedbackPojo> feedbackModelList;

    public Feedback_adapter(Context context, List<FeedbackPojo> feedbackModelList) {
        this.feedbackModelList = feedbackModelList;
        this.context = context;

    }

    @Override
    public Feedback_adapter.FeedbackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feedback_design, parent, false);
        Feedback_adapter.FeedbackHolder viewHolder = new Feedback_adapter.FeedbackHolder(v);

        return viewHolder;
    }
    @Override
    public void onBindViewHolder(Feedback_adapter.FeedbackHolder holder, int position) {
        feedbackModel = feedbackModelList.get(position);

        holder.tv_Name.setText(feedbackModel.getName());
        holder.key.setText(feedbackModel.getKey());
        holder.tv_date.setText(feedbackModel.getDate());
        holder.tv_content.setText(feedbackModel.getContent());
        holder.ratings.setText(feedbackModel.getRatings());
        Picasso.with(context)
                .load("https://www.onebookingsystem.com/API//FebsUsers/"+feedbackModel.getProfilepic())
                .error(R.drawable.panditlogo)
                .into(holder.profilepic);
    }

    @Override
    public int getItemCount() {
        return feedbackModelList.size();
    }

    public class FeedbackHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tv_Name,tv_content,tv_date,key,ratings;
        public ImageView profilepic;

        String status;

        public FeedbackHolder(View itemView) {
            super(itemView);
            tv_content =(TextView) itemView.findViewById(R.id.textView14);
            tv_date =(TextView) itemView.findViewById(R.id.textView18);
            tv_Name = (TextView) itemView.findViewById(R.id.textView13);
            key = (TextView) itemView.findViewById(R.id.key);
            ratings = (TextView) itemView.findViewById(R.id.textView5);
            profilepic = (ImageView) itemView.findViewById(R.id.imageView10);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            if ((v == itemView)){


            }

        }
    }
}
