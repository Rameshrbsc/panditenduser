package com.codegenstudios.purohit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.codegenstudios.purohit.Activity.DashboardViewallActivity;
import com.codegenstudios.purohit.Activity.ViewMoreActivty;
import com.codegenstudios.purohit.Pojo.DashboardResponsePojo;
import com.codegenstudios.purohit.Pojo.DashboardSubCatgeryListResponsePojo;
import com.codegenstudios.purohit.R;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DashboardSubGridViewAdapter extends BaseAdapter {

    Context context;
    private ViewHolder holder;
    private DashboardSubCardViewAdapter mAdapter;
    List<DashboardResponsePojo> dashboardResponsePojosList;
    DashboardResponsePojo dashboardResponsePojo;
    List<DashboardResponsePojo> all_thumbs = new ArrayList<DashboardResponsePojo>();


    public DashboardSubGridViewAdapter(Context context, List<DashboardResponsePojo> dashboardResponsePojosList) {
        this.context = context;
        this.dashboardResponsePojosList = dashboardResponsePojosList;
    }

    public class ViewHolder {
        public GridView GridViewLayout1;
        public RecyclerView recyclerView;
        public TextView TextViewTitle, TextViewViewAll, more;

    }

    @Override
    public int getCount() {
        return dashboardResponsePojosList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        dashboardResponsePojo = new DashboardResponsePojo();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.custom_dashboard_inside_gridview, parent, false);
        holder = new ViewHolder();
        holder.TextViewTitle = (TextView) convertView.findViewById(R.id.TextViewTitle);
        holder.recyclerView = (RecyclerView) convertView.findViewById(R.id.recyclerView);
        holder.more = (TextView) convertView.findViewById(R.id.more);



        holder.TextViewTitle.setText(dashboardResponsePojosList.get(position).getCatogeryName());
        mAdapter = new DashboardSubCardViewAdapter(context, dashboardResponsePojosList.get(position).getCatogeryName(),
                dashboardResponsePojosList.get(position).getCatogeryId(),
                dashboardResponsePojosList.get(position).getSubCatgeryListResponse());
        GridLayoutManager layoutManager = new GridLayoutManager(context,1);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        GridLayoutManager manager = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(mAdapter);

       /* holder.recyclerView.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                holder.recyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }
        });*/
        //holder.recyclerView.scrollToPosition(1);
      // holder.recyclerView.setNestedScrollingEnabled(false);
        all_thumbs = dashboardResponsePojosList;

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(context, ViewMoreActivty.class);
                bundle.putSerializable("Valuue", (Serializable) all_thumbs);
                bundle.putSerializable("value", dashboardResponsePojo);
                bundle.putSerializable("Name", dashboardResponsePojosList.get(position).getCatogeryName());
                bundle.putString("code",dashboardResponsePojosList.get(position).getCatogeryId());
                intent.putExtras(bundle);


                context.startActivity(intent);
            }
        });


        return convertView;
    }

    private void setDynamicHeight(GridView gridView) {
        ListAdapter gridViewAdapter = gridView.getAdapter();
        if (gridViewAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = gridViewAdapter.getCount();
        Log.e("items", String.valueOf(items));
        int rows = 0;

        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();
        Log.e("totalHeight", String.valueOf(totalHeight));

        float x = 1;
        if (items > 5) {
            x = items / 5;
            rows = (int) (x + 1);
            totalHeight *= rows;
            Log.e("totalHeight", String.valueOf(totalHeight));
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

}
