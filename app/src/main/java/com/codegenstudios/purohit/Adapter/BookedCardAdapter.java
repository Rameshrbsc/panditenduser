package com.codegenstudios.purohit.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.API.API;
import com.codegenstudios.purohit.Activity.BookingFormActivity;
import com.codegenstudios.purohit.Activity.LoginActivity;
import com.codegenstudios.purohit.Activity.MapTrackingPurohitActivity;
import com.codegenstudios.purohit.Activity.NotificationFullViewActivity;
import com.codegenstudios.purohit.Activity.RatingFeedbackActivity;
import com.codegenstudios.purohit.Pojo.AllBookingListPojo;
import com.codegenstudios.purohit.Pojo.AllBookingListResponsePojo;
import com.codegenstudios.purohit.Pojo.RatingFeedbackPojo;
import com.codegenstudios.purohit.R;
import com.codegenstudios.purohit.SupportClass.MyPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codegenstudios.purohit.Services.Service.createServiceHeader;
import static java.lang.String.valueOf;

public class BookedCardAdapter extends BaseAdapter {
    private Context context;
    ViewHolder holder;
    List<AllBookingListResponsePojo> allBookingListResponsePojos;
    String status;
    private ProgressDialog progressBar;
    MyPreference myPreference;

    public BookedCardAdapter(FragmentActivity context, List<AllBookingListResponsePojo> allBookingListResponsePojos, String status) {
        this.context = context;
        this.allBookingListResponsePojos = allBookingListResponsePojos;
        this.status = status;

    }


    private class ViewHolder {
        TextView TextViewCustomerName, TextViewAmount, TextViewPoojaName, TextViewStatus, TextViewSubmitDate, TextViewBookingDate, TextViewBookingDate1, TextViewFeedback, TextViewTracking, cancel;
        LinearLayout LinearLayoutRating, LinearLayoutTracking;
        RatingBar RatingBar;
        ImageView ImageViewEditRating, ImageIcon;
        CardView CardFullView;
        ImageView live_tracking;
    }

    @Override
    public int getCount() {
        return allBookingListResponsePojos.size();
    }

    @Override
    public Object getItem(int position) {
        return allBookingListResponsePojos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        Date datepick = null, datepick1 = null;
        myPreference=new MyPreference(context);


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.custom_bookedlist_card_latest, viewGroup, false);
            holder = new ViewHolder();
            holder.CardFullView = (CardView) view.findViewById(R.id.CardFullView);
            holder.TextViewCustomerName = (TextView) view.findViewById(R.id.TextViewCustomerName);
            holder.TextViewAmount = (TextView) view.findViewById(R.id.TextViewAmount);
            holder.TextViewPoojaName = (TextView) view.findViewById(R.id.TextViewPoojaName);
            holder.TextViewStatus = (TextView) view.findViewById(R.id.TextViewStatus);
            holder.TextViewSubmitDate = (TextView) view.findViewById(R.id.TextViewSubmitDate);
            holder.TextViewBookingDate = (TextView) view.findViewById(R.id.TextViewBookingDate);
            holder.TextViewBookingDate1 = (TextView) view.findViewById(R.id.TextViewBookingDate1);
            holder.TextViewFeedback = (TextView) view.findViewById(R.id.TextViewFeedback);
            holder.TextViewTracking = (TextView) view.findViewById(R.id.TextViewTracking);
            holder.cancel = (TextView) view.findViewById(R.id.cancel);
            holder.live_tracking = (ImageView) view.findViewById(R.id.live_tracking);


            holder.RatingBar = (RatingBar) view.findViewById(R.id.RatingBar);
            holder.LinearLayoutRating = (LinearLayout) view.findViewById(R.id.LinearLayoutRating);
            holder.LinearLayoutTracking = (LinearLayout) view.findViewById(R.id.LinearLayoutTracking);
            holder.ImageViewEditRating = (ImageView) view.findViewById(R.id.ImageViewEditRating);


            holder.ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);

            view.setTag(holder);
        } else {

            holder = (ViewHolder) view.getTag();
        }


        holder.TextViewCustomerName.setText(allBookingListResponsePojos.get(position).getUserName());
        holder.TextViewAmount.setText("Rs." + allBookingListResponsePojos.get(position).getAmount() + "/-");
        holder.TextViewPoojaName.setText(allBookingListResponsePojos.get(position).getPoojaName());

        final SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");

        try {
            datepick = input.parse(allBookingListResponsePojos.get(position).getSubmitDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.TextViewSubmitDate.setText(output.format(datepick));
        try {
            datepick1 = input.parse(allBookingListResponsePojos.get(position).getBookingDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.TextViewBookingDate.setText(output.format(datepick1));
       /* Picasso.with(context)
                .load(allBookingListResponsePojos.get(position).getPoojaImage())
                .into(holder.ImageIcon);*/


        Glide.with(holder.ImageIcon).load(allBookingListResponsePojos.get(position).getPoojaImage())
                .apply(new RequestOptions()
                        .fitCenter()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .override(Target.SIZE_ORIGINAL))
                .into(holder.ImageIcon);
        holder.TextViewBookingDate1.setText(allBookingListResponsePojos.get(position).getBookingDate());
        holder.CardFullView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, NotificationFullViewActivity.class);//to change this
                intent.putExtra("BookingId", allBookingListResponsePojos.get(position).getBookingId());
                intent.putExtra("IntentPassing", "BookedCardAdapter");
                intent.putExtra("status", status);
                context.startActivity(intent);
            }
        });
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_success_dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                dialogButton.setText("Now");
                Button ButtonNo = (Button) dialog.findViewById(R.id.ButtonNo);
                ButtonNo.setText("Later");
                ImageView ImageSuccessIcon = (ImageView) dialog.findViewById(R.id.ImageSuccessIcon);
                ImageSuccessIcon.setImageResource(R.mipmap.ic_launcher);
                ImageSuccessIcon.setVisibility(View.VISIBLE);
                TextView TextviewBigTitle = (TextView) dialog.findViewById(R.id.TextviewBigTitle);
                TextviewBigTitle.setVisibility(View.GONE);
                TextView TextviewSmallTitle1 = (TextView) dialog.findViewById(R.id.TextviewSmallTitle1);
                String first = "If you want to cancel ! Please call to 044-448655151";

                // String next = "<font color='#F08400'> " + "+91 94441 54945" + " </font>";
                TextviewSmallTitle1.setText(Html.fromHtml(first));
                TextviewSmallTitle1.setTextSize(20);
                dialog.dismiss();
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:044-448655151"));
                        context.startActivity(intent);
                        //dialog.dismiss();
                        /*Intent intent = new Intent(context, BookingFormActivity.class);
                        intent.putExtra("IntentPassing","MyBookingCardActivity");
                        intent.putExtra("PoojaID", PoojaID[position]);
                        intent.putExtra("PoojaName", PoojaName[position]);
                        intent.putExtra("Amount", Amount[position]);
                        intent.putExtra("CatId", catId);
                        intent.putExtra("CatName", catName);
                        intent.putExtra("SubId", subId);
                        intent.putExtra("SubName", subName);
                        intent.putExtra("IntentPassingType","MyBookingCard");
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);*/
                    }
                });
                ButtonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        });

//        0 - Pending   1 - Confirmed   2 - Rejected     3 - Cancelled    4 - Completed
        if (allBookingListResponsePojos.get(position).getStatus().equals("0")) {               //pending
            holder.TextViewStatus.setText("Pending");
            holder.TextViewStatus.setTextColor((Color.parseColor("#E65100")));
        } else if (allBookingListResponsePojos.get(position).getStatus().equals("1")) {          //confirmed
            holder.TextViewStatus.setText("Confirmed");
            holder.TextViewStatus.setTextColor((Color.parseColor("#558B2F")));
            if (allBookingListResponsePojos.get(position).getTrackMe().equals("1")) {//online
                holder.LinearLayoutTracking.setVisibility(View.VISIBLE);
                holder.TextViewTracking.setVisibility(View.VISIBLE);







                holder.LinearLayoutTracking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, NotificationFullViewActivity.class);
                        intent.putExtra("IntentPassing", "MapTracking");
                        intent.putExtra("BookingId", allBookingListResponsePojos.get(position).getBookingId());
                        intent.putExtra("UserLat", allBookingListResponsePojos.get(position).getLatitude());
                        intent.putExtra("UserLong", allBookingListResponsePojos.get(position).getLongitude());
                        intent.putExtra("status", status);
                        context.startActivity(intent);
                    }
                });

            } else {
                holder.LinearLayoutTracking.setVisibility(View.VISIBLE);
                holder.TextViewTracking.setVisibility(View.GONE);
                holder.live_tracking.setVisibility(View.VISIBLE);
            }
        } else if (allBookingListResponsePojos.get(position).getStatus().equals("4")) {         //completed
            holder.TextViewStatus.setText("Completed");
            holder.TextViewStatus.setTextColor((Color.parseColor("#558B2F")));
            holder.LinearLayoutRating.setVisibility(View.GONE);
            String number = allBookingListResponsePojos.get(position).getRating();
            //    float d= (float) ((number*5) /100);
            if (!number.equals("")) {
                holder.RatingBar.setRating(Float.parseFloat(number));
                holder.TextViewFeedback.setText(allBookingListResponsePojos.get(position).getFeedback());
            }
            holder.ImageViewEditRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RatingFeedbackActivity.class);
                    intent.putExtra("BookingId", allBookingListResponsePojos.get(position).getBookingId());
                    intent.putExtra("Rating", allBookingListResponsePojos.get(position).getRating());
                    intent.putExtra("FeedBack", allBookingListResponsePojos.get(position).getFeedback());

                    context.startActivity(intent);
                }
            });
        } else if (allBookingListResponsePojos.get(position).getStatus().equals("3")) {             //canceled
            holder.TextViewStatus.setText("Cancelled");
            holder.TextViewStatus.setTextColor(Color.parseColor("#D84315"));
            holder.cancel.setVisibility(View.GONE);
        } else if (allBookingListResponsePojos.get(position).getStatus().equals("2")) {              //rejected
            holder.TextViewStatus.setText("Rejected");
            holder.TextViewStatus.setTextColor(Color.parseColor("#D84315"));
        }

        holder.RatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {


                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(context);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("BookingID", allBookingListResponsePojos.get(position).getBookingId());
                    jsonObject.addProperty("Rating", String.valueOf(holder.RatingBar.getRating()));
                    jsonObject.addProperty("Feedback", allBookingListResponsePojos.get(position).getFeedback());

                    API apiService = createServiceHeader(API.class);
                    Call<RatingFeedbackPojo> call = apiService.RATING_FEEDBACK_POJO_CALL(jsonObject);
                    Log.e("json",jsonObject.toString());
                    call.enqueue(new Callback<RatingFeedbackPojo>() {
                        @Override
                        public void onResponse(Call<RatingFeedbackPojo> call, Response<RatingFeedbackPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":


                                    break;
                                case "108":

                                default:
                                    myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<RatingFeedbackPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }else {
                    myPreference.ShowDialog(context,"Oops","There is not internet connection");
                }


            }
        });
        return view;

    }
}
