package com.codegenstudios.purohit.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;
import com.codegenstudios.purohit.Activity.BookingCardListActivity;
import com.codegenstudios.purohit.Activity.BookingFormActivity;
import com.codegenstudios.purohit.Fragment.UserProfileViewFragment;
import com.codegenstudios.purohit.R;

public class PoojaListCardAdapter extends BaseAdapter {

    private Context context;
    ViewHolder holder;
    private String[] PoojaName, PoojaID, PoojaDescription, Amount, PoojaPicthumbnail;
    String catName, catId, subName, subId;

    public PoojaListCardAdapter(FragmentActivity activity, String[] PoojaName, String[] PoojaID, String[] PoojaDescription, String[] Amount, String[] PoojaPicthumbnail, String catName, String subName, String catId, String subId) {
        this.context = activity;
        this.Amount = Amount;
        this.PoojaName = PoojaName;
        this.PoojaID = PoojaID;
        this.PoojaDescription = PoojaDescription;
        this.PoojaPicthumbnail = PoojaPicthumbnail;
        this.catId = catId;
        this.catName = catName;
        this.subId = subId;
        this.subName = subName;

    }


    @Override
    public int getCount() {
        return Amount.length;
    }

    @Override
    public Object getItem(int position) {
        return Amount[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView TextViewPoojaName, TextViewDescription, TextViewPoojaCost;
        Button ButtonBook;
        ImageView ImageIcon;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.custom_card_mybooking, viewGroup, false);
            holder = new ViewHolder();
            holder.ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);
            holder.TextViewPoojaName = (TextView) view.findViewById(R.id.TextViewPoojaName);
            holder.TextViewDescription = (TextView) view.findViewById(R.id.TextViewDescription);
            holder.TextViewPoojaCost = (TextView) view.findViewById(R.id.TextViewPoojaCost);
            holder.ButtonBook = (Button) view.findViewById(R.id.ButtonBook);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        holder.TextViewPoojaName.setText(PoojaName[position]);
        holder.TextViewPoojaCost.setText("Rs." + Amount[position] + "/-");
        holder.TextViewDescription.setText(PoojaDescription[position]);
        /*Glide
                .with(context)
                .load(PoojaPicthumbnail[position])
                .apply(new RequestOptions().override(600, 200))

                .into(holder.ImageIcon);*/

       // Picasso.with(context).load(PoojaPicthumbnail[position]).transform(new UserProfileViewFragment.CircleTransform()).into(  holder.ImageIcon);

        Glide.with(holder.ImageIcon).load(PoojaPicthumbnail[position])
                .apply(new RequestOptions()
                        .fitCenter()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .override(Target.SIZE_ORIGINAL))
                .into(holder.ImageIcon);

        holder.ButtonBook.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, BookingFormActivity.class);
                intent.putExtra("IntentPassing", "MyBookingCardActivity");
                intent.putExtra("PoojaID", PoojaID[position]);
                intent.putExtra("PoojaName", PoojaName[position]);
                intent.putExtra("Amount", Amount[position]);
                intent.putExtra("imageIcon", PoojaPicthumbnail[position]);
                intent.putExtra("CatId", catId);
                intent.putExtra("CatName", catName);
                intent.putExtra("SubId", subId);
                intent.putExtra("SubName", subName);
                intent.putExtra("IntentPassingType", "MyBookingCard");
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);

                /*final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_success_dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                Button ButtonNo = (Button) dialog.findViewById(R.id.ButtonNo);
                ImageView ImageSuccessIcon = (ImageView) dialog.findViewById(R.id.ImageSuccessIcon);
                Picasso.with(context).load(PoojaPicthumbnail[position]).transform(new UserProfileViewFragment.CircleTransform()).into(  ImageSuccessIcon);
                ImageSuccessIcon.setVisibility(View.VISIBLE);
                TextView TextviewBigTitle = (TextView) dialog.findViewById(R.id.TextviewBigTitle);
                TextviewBigTitle.setVisibility(View.GONE);
                TextView TextviewSmallTitle1 = (TextView) dialog.findViewById(R.id.TextviewSmallTitle1);
                String first = "Do you want to book this ? ";
                String next = "<font color='#F08400'> " + PoojaName[position] + " </font>";
                TextviewSmallTitle1.setText(Html.fromHtml(first+"\n" + next));
                TextviewSmallTitle1.setTextSize(16);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(context, BookingFormActivity.class);
                        intent.putExtra("IntentPassing", "MyBookingCardActivity");
                        intent.putExtra("PoojaID", PoojaID[position]);
                        intent.putExtra("PoojaName", PoojaName[position]);
                        intent.putExtra("Amount", Amount[position]);
                        intent.putExtra("imageIcon", PoojaPicthumbnail[position]);
                        intent.putExtra("CatId", catId);
                        intent.putExtra("CatName", catName);
                        intent.putExtra("SubId", subId);
                        intent.putExtra("SubName", subName);
                        intent.putExtra("IntentPassingType", "MyBookingCard");
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    }
                });
                ButtonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                */

            }
        });

        return view;
    }
}


